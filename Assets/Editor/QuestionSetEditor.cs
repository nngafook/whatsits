﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.IO;

[CustomEditor(typeof(QuestionSet))]
[CanEditMultipleObjects]
public class QuestionSetEditor : Editor {

    private QuestionSet qs;
    private int questionIndexToRemove = -1;
    private string importPath = "";
    private string newQuestionSetName = "";
    private string exportToPath = "";
    private QuestionCategory importCategory = QuestionCategory.Uncategorized;

    public override void OnInspectorGUI() {
        base.OnInspectorGUI();

        qs = (target as QuestionSet);

        if (GUILayout.Button("Generate Missing IDs")) {
            List<Question> questions = qs.Questions;
            for (int i = 0; i < questions.Count; i++) {
                if (string.IsNullOrEmpty(questions[i].uid)) {
                    questions[i].GenerateUID();
                }
            }

            if (string.IsNullOrEmpty(qs.data.id)) {
                Debug.Log("Missing Question Set ID Generated".Bold());
                qs.data.GenerateID();
            }
        }

        if (GUILayout.Button("Export")) {

            QuestionSetData setData = new QuestionSetData();
            setData.Copy(qs.data);
            //setData.name = qs.data.name;
            //setData.id = qs.data.id;
            //setData.cost = qs.data.cost;
            //setData.gameProgressionIndex = qs.data.gameProgressionIndex;
            //setData.category = qs.data.category;
            //setData.questions = new List<Question>(qs.Questions);

            Utility.QuestionSetToJson(setData, qs.data.name, System.IO.Path.Combine(Application.dataPath, "QuestionSetJSONFiles/Exported/"));
            AssetDatabase.Refresh();
        }

        if (GUILayout.Button("Fix Empty Subcategories")) {
            for (int i = 0; i < qs.Questions.Count; i++) {
                if (qs.Questions[i].subCategories.Count == 0) {
                    Debug.Log("Subcategory inserted");
                    qs.Questions[i].subCategories.Add(QuestionCategory.General);
                }
            }
        }

        if (GUILayout.Button("Validate Answer Categories")) {
            for (int i = 0; i < qs.Questions.Count; i++) {
                if (qs.Questions[i].answerCategories.Count == 0) {
                    Debug.LogError(qs.Questions[i].uid + " has no answer categories, index: " + i);
                }
            }
        }

        if (GUILayout.Button("Validate Label Answers")) {
            for (int i = 0; i < qs.Questions.Count; i++) {
                if (qs.Questions[i].questionType == QuestionType.Label) {
                    // Make sure no empty label options exist
                    for (int j = 0; j < qs.Questions[i].buttonLabelOptions.Count; j++) {
                        if (string.IsNullOrEmpty(qs.Questions[i].buttonLabelOptions[j])) {
                            Debug.Log("Empty label option found in question index: " + i + ", and label option index: " + j);
                            for (int k = j + 1; k < qs.Questions[i].buttonLabelOptions.Count; k++) {
                                qs.Questions[i].buttonLabelOptions[k - 1] = qs.Questions[i].buttonLabelOptions[k];
                            }
                            qs.Questions[i].buttonLabelOptions.RemoveAt(qs.Questions[i].buttonLabelOptions.Count - 1);
                            j = 0;
                        }
                    }

                    // Make sure that the correct label answer is in the label options
                    if (!string.IsNullOrEmpty(qs.Questions[i].correctLabelAnswer)) {
                        bool found = false;
                        for (int j = 0; j < qs.Questions[i].buttonLabelOptions.Count; j++) {
                            if (qs.Questions[i].buttonLabelOptions[j] == qs.Questions[i].correctLabelAnswer) {
                                found = true;
                                break;
                            }
                        }
                        if (!found) {
                            qs.Questions[i].buttonLabelOptions.Add(qs.Questions[i].correctLabelAnswer);
                            Debug.Log(("Inserted correct label answer into button label options of question index: " + i + ". The answer was: " + qs.Questions[i].correctLabelAnswer).Colored(CustomColor.GetColor(ColorName.SKY_BLUE)));
                        }
                    }

                }
            }
        }

        GUILayout.Space(10);
        questionIndexToRemove = EditorGUILayout.IntField(questionIndexToRemove);
        GUI.color = CustomColor.GetColor(ColorName.RACY_RED);
        if (GUILayout.Button("Remove Index")) {
            if (qs.Questions.IsIndexValid(questionIndexToRemove)) {
                qs.Questions.RemoveAt(questionIndexToRemove);
            }
        }
        GUI.color = Color.white;

        GUILayout.Space(10);
        importPath = EditorGUILayout.TextField(importPath);

        if (GUILayout.Button("Import")) {
            if (!string.IsNullOrEmpty(importPath)) {
                QuestionSetData setData = JsonUtility.FromJson<QuestionSetData>(System.IO.File.ReadAllText(importPath));
                if (setData != null) {
                    qs.data = setData;
                    importPath = "";
                }
            }
        }

        if (GUILayout.Button("Add Questions")) {
            if (!string.IsNullOrEmpty(importPath)) {
                QuestionSetData setData = JsonUtility.FromJson<QuestionSetData>(System.IO.File.ReadAllText(importPath));
                if (setData != null) {
                    for (int i = 0; i < setData.questions.Count; i++) {
                        if (!qs.HasQuestionID(setData.questions[i].uid)) {
                            qs.Questions.Add(setData.questions[i]);
                        }
                        else {
                            Debug.Log("Question Set already contains UID: " + setData.questions[i].uid);
                        }
                    }
                    EditorUtility.SetDirty(this);
                    AssetDatabase.SaveAssets();
                }
            }
        }

        GUILayout.Space(10);
        GUILayout.Label("Category Import");
        importCategory = (QuestionCategory)EditorGUILayout.EnumPopup(importCategory);

        GUI.color = Color.green;
        if (GUILayout.Button("Add Specific Category Questions")) {
            if (!string.IsNullOrEmpty(importPath)) {
                QuestionSetData setData = JsonUtility.FromJson<QuestionSetData>(System.IO.File.ReadAllText(importPath));
                if (setData != null) {
                    for (int i = 0; i < setData.questions.Count; i++) {
                        if (setData.questions[i].questionCategory == importCategory) {
                            if (!qs.HasQuestionID(setData.questions[i].uid)) {
                                qs.Questions.Add(setData.questions[i]);
                            }
                            else {
                                Debug.Log("Question Set already contains UID: " + setData.questions[i].uid);
                            }
                        }
                    }
                    importPath = "";
                }
            }
        }

        GUILayout.Space(10);
        GUI.color = Color.white;
        newQuestionSetName = EditorGUILayout.TextField("New Question Set Name", newQuestionSetName);
        //exportToPath = EditorGUILayout.TextField("Export To Path", exportToPath);

        GUI.color = Color.red;
        if (GUILayout.Button("Export Category Questions To New")) {
            if (!string.IsNullOrEmpty(newQuestionSetName)) {

                exportToPath = "Assets/Resources/QuestionSets/" + importCategory.ToString() + "/";
                Directory.CreateDirectory(exportToPath);
                if (Directory.Exists(exportToPath)) {
                    //it's a directory
                    QuestionSetData setData = qs.data;
                    if (setData != null) {
                        bool exported = false;

                        QuestionSet newQuestionSet = QuestionSet.CreateInstance<QuestionSet>();
                        newQuestionSet.data.category = importCategory;
                        newQuestionSet.data.GenerateID();
                        AssetDatabase.CreateAsset(newQuestionSet, exportToPath + newQuestionSetName + ".asset");

                        for (int i = 0; i < setData.questions.Count; i++) {
                            if (setData.questions[i].questionCategory == importCategory) {
                                newQuestionSet.Questions.Add(setData.questions[i]);
                                exported = true;
                            }
                        }

                        if (exported) {
                            Debug.Log("Exported " + newQuestionSetName + " to " + exportToPath);
                        }

                        importPath = "";
                        newQuestionSetName = "";
                        exportToPath = "";
                        AssetDatabase.SaveAssets();
                        AssetDatabase.Refresh();
                    }
                }
                else{
                    Debug.LogError("Export To Path is not a directory");
                }
            }
        }

        GUI.color = Color.white;

    }

}
