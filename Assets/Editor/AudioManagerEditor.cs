﻿using UnityEngine;
using UnityEditor;
using System.Collections;

[CustomEditor(typeof(AudioManager))]
public class AudioManagerEditor : Editor {

    public override void OnInspectorGUI() {
        if (Application.isPlaying) {
            AudioManager manager = (target as AudioManager);
            string label = (manager.MusicEnabled) ? "I'M SICK OF THIS MUSIC!" : "I NEED MOAR MUSIC!";
            GUI.color = (manager.MusicEnabled) ? Color.red : Color.green;
            if (GUILayout.Button(label, GUILayout.Height(40))) {
                manager.SetMusicEnabled(!manager.MusicEnabled);
            }

            EditorGUILayout.Space();

            label = (manager.SFXEnabled) ? "I'M SICK OF THESE NOISES!" : "WHERE ARE MY NOISES!?";
            GUI.color = (manager.SFXEnabled) ? Color.red : Color.green;
            if (GUILayout.Button(label, GUILayout.Height(40))) {
                manager.SetSFXEnabled(!manager.SFXEnabled);
            }
            EditorGUILayout.Space();
            GUI.color = Color.white;
        }
        DrawDefaultInspector();
    }

}
