﻿using UnityEngine;
using System.Collections;
using System;

public class EnumSelectWindow : ScriptableObject {

    private Action<int> OkCallback;
    private Action CancelCallback;

    private Vector2 scrollPosition;

    private Enum enumToShow;

    private int selectedValue = -1;

    public Rect windowRect;
    public string windowTitle = "Enum Selection";
    public Vector2 minSize = new Vector2(420, 500);

    public void Init(Enum e, int selected) {
        enumToShow = e;
        selectedValue = selected;
    }

    public void SetCallbacks(Action<int> okCallback, Action cancelCallback) {
        OkCallback = okCallback;
        CancelCallback = cancelCallback;
    }

    private void DrawEnumButtons() {
        GUILayout.BeginVertical();
        int totalColumns = 3;
        int i = 0;

        string[] values = Enum.GetNames(enumToShow.GetType());

        while (i < values.Length) {
            GUILayout.BeginHorizontal();
            for (int j = 0; j < totalColumns; j++) {
                if (i < values.Length) {
                    GUI.color = (selectedValue == i) ? CustomColor.GetColor(ColorName.SUBMIT_GREEN) : Color.white;
                    if (GUILayout.Button(Utility.CamelCaseToReadable(values[i].ToString()), GUILayout.Width(120), GUILayout.Height(120))) {
                        selectedValue = i;
                        CancelCallback = null;
                        if (OkCallback != null) {
                            OkCallback(selectedValue);
                            OkCallback = null;
                        }
                        break;
                    }
                    GUI.color = Color.white;
                    i++;
                }
            }
            GUILayout.EndHorizontal();
        }

        GUILayout.EndVertical();
    }

    public void DrawWindow() {
        GUILayout.BeginVertical();
        GUILayout.Space(10);

        scrollPosition = GUILayout.BeginScrollView(scrollPosition);
        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();

        DrawEnumButtons();


        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();
        GUILayout.EndScrollView();

        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        //GUILayout.Label(Utility.EnumNameToReadable(characterIDOptions[selectedBtnIndex].ToString()));
        GUILayout.Label("");
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();

        GUILayout.Space(5);
        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        GUI.color = CustomColor.GetColor(ColorName.ERROR_RED);
        if (GUILayout.Button("Cancel", GUILayout.Width(100))) {
            OkCallback = null;
            if (CancelCallback != null) {
                CancelCallback();
                CancelCallback = null;
            }
        }

        GUI.color = CustomColor.GetColor(ColorName.SUBMIT_GREEN);
        if (GUILayout.Button("OK", GUILayout.Width(100))) {
            //AddIDToList(characterIDOptions[selectedBtnIndex]);
            CancelCallback = null;
            if (OkCallback != null) {
                OkCallback(selectedValue);
                OkCallback = null;
            }
        }
        GUI.color = Color.white;
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();



        GUILayout.EndVertical();
    }
}
