﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using System.Text;

public class QuestionEditorWindow : EditorWindow {

    private enum WindowMode {
        QuestionUtil,
        JSONUtil
    }
    private WindowMode currentWindowMode = WindowMode.QuestionUtil;

    private Action<int> enumWindowCallback;
    private bool inputDisabled = false;
    private bool deleteConfirmWaiting = false;
    private string jsonFilesPath;

    // ==== JSON UTIL ==== //

    private Dictionary<int, ExportToQuestionSetInfo> qsExportInfos = new Dictionary<int, ExportToQuestionSetInfo>();
    private string jsonPathToUse;
    private string splitExportPath;
    private int qsObjectsToSplitInto = 0;

    // ==== END OF JSON UTIL ==== //

    // ==== QUESTION UTIL ==== //
    private bool viewingGeneratedSets = false;
    private Vector2 questionScrollPosition;
    private string generatedQuestionsPath = "";
    private string searchString = "";
    private int subCategoryIndexToChange = -1;
    private int answerCategoryIndexToChange = -1;
    //private List<QuestionSetData> questionSetDatas;
    private List<Question> questions;

    private Question selectedQuestion;

    // ==== END OF QUESTION UTIL ==== //

    private EnumSelectWindow enumSelectWindow;
    private TextInputEditorWindow textInputWindow;

    private Color darkGray = new Color(0.172549f, 0.172549f, 0.172549f, 1f);
    private Texture2D windowBGTexture;
    private Texture2D menuBGTexture;
    private Texture2D raysTexture;
    private GUISkin baseGUISkin;
    private GUISkin myGUISkin;

    private Vector2 menuScrollPosition;

    private float Padding { get { return 10; } }
    private float WindowWidth { get { return position.width; } }
    private float WindowHeight { get { return position.height; } }
    private float ToolbarHeight { get { return 100; } }
    private float ToolbarButtonWidth { get { return 120; } }
    private float ToolbarButtonHeight { get { return 50; } }
    private float LeftColumnWidth { get { return WindowWidth / 3; } }
    private float RightColumnWidth { get { return (WindowWidth / 3) * 2; } }
    private float RightFieldButtonHeight { get { return 28; } }

    void Awake() {
        //augmentIDDatabase = AssetDatabase.LoadAssetAtPath<AugmentIDDatabaseDataObject>("Assets/Resources/Data/AugmentIDDatabase.asset");
        this.minSize = new Vector2(1400, 800);
        jsonFilesPath = Application.dataPath + "/Resources/Questions/";
        InitTextures();
        GetGUISkin();
        LoadQuestions();
    }

    void OnEnable() {
        InitTextures();
        GetGUISkin();
        LoadQuestions();
    }

    void OnDestroy() {
        //if (characterSelectionWindow != null) {
        //    DestroyImmediate(characterSelectionWindow);
        //    characterSelectionWindow = null;
        //    inputDisabled = false;
        //}
    }

    private void InitTextures() {
        raysTexture = (Texture2D)AssetDatabase.LoadAssetAtPath("Assets/Art/2D/RaysBackground.png", typeof(Texture2D));
        
        windowBGTexture = new Texture2D(1, 1, TextureFormat.RGBA32, false);
        windowBGTexture.SetPixel(0, 0, new Color(0.3803922f, 0.6431373f, 0.8666667f, 1f));
        windowBGTexture.Apply();

        menuBGTexture = new Texture2D(1, 1, TextureFormat.RGBA32, false);
        menuBGTexture.SetPixel(0, 0, new Color(0.3333333f, 0.5960785f, 0.9098039f, 1f));
        menuBGTexture.Apply();
    }

    private void LoadQuestions() {
        //questionSetDatas = new List<QuestionSetData>();
        questions = new List<Question>();

        QuestionSetData setData = null;
        setData = JsonUtility.FromJson<QuestionSetData>(System.IO.File.ReadAllText(jsonFilesPath + "ALLQUESTIONSv1.json"));
        for (int i = 0; i < setData.questions.Count; i++) {
            questions.Add(setData.questions[i]);
        }
        
    }

    
    private void LoadGeneratedQuestions() {
        questions = new List<Question>();
        QuestionSetData setData = null;
        if (!string.IsNullOrEmpty(generatedQuestionsPath)) {
            string filename = Path.Combine(Application.persistentDataPath + "/GeneratedSets/v1", generatedQuestionsPath + ".json");
            if (File.Exists(filename)) {
                setData = JsonUtility.FromJson<QuestionSetData>(System.IO.File.ReadAllText(filename));
                for (int i = 0; i < setData.questions.Count; i++) {
                    questions.Add(setData.questions[i]);
                }
            }
            else {
                Debug.LogError("File/Folder does not exist");
            }
        }
        else {
            string path = Path.Combine(Application.persistentDataPath, "GeneratedSets/v1");
            if (Directory.Exists(path)) {
                string[] paths = Directory.GetFiles(path, "*.json");

                setData = null;
                QuestionSet qs = null;
                for (int i = 0; i < paths.Length; i++) {
                    setData = JsonUtility.FromJson<QuestionSetData>(System.IO.File.ReadAllText(paths[i]));
                    qs = ScriptableObject.CreateInstance<QuestionSet>();
                    qs.data = setData;
                    for (int j = 0; j < setData.questions.Count; j++) {
                        questions.Add(setData.questions[j]);
                    }
                }
            }
            else {
                Debug.LogError("File/Folder does not exist");
            }
        }
    }

    private void PrintLongestQuestion() {
        int longest = 0;
        int indexToUse = 0;
        for (int i = 0; i < questions.Count; i++) {
            if (questions[i].QuestionText.Length > longest) {
                longest = questions[i].QuestionText.Length;
                indexToUse = i;
            }
        }

        Debug.Log("Longest Question is:");
        Debug.Log(questions[indexToUse].QuestionText.Bold().Colored(Color.blue));
    }

    private void GetGUISkin() {
        myGUISkin = (GUISkin)AssetDatabase.LoadAssetAtPath("Assets/Resources/MyGUISkin.guiskin", typeof(GUISkin));
    }

    private void SaveQuestions() {
        QuestionSetData setData = new QuestionSetData();
        setData.name = "ALLQUESTIONSv1";
        setData.GenerateID();
        setData.category = QuestionCategory.General;
        for (int i = 0; i < questions.Count; i++) {
            setData.questions.Add(questions[i]);
        }

        if (File.Exists(jsonFilesPath + "ALLQUESTIONSv1.json")) {
            string backupsFolderPath = jsonFilesPath + "Backups";
            Directory.CreateDirectory(backupsFolderPath);
            string[] backupsList = Directory.GetFiles(backupsFolderPath, "*.json");
            int fileCountPrefix = EditorPrefs.GetInt("QuestionBackupPrefix");
            fileCountPrefix++;

            if (fileCountPrefix > 10) {
                fileCountPrefix = 1;
            }
            EditorPrefs.SetInt("QuestionBackupPrefix", fileCountPrefix);

            DateTime now = DateTime.Now;
            string timestamp = now.Year + "-" + now.Month + "-" + now.Day + "_" + now.Hour + "." + now.Minute + "." + now.Second + ")";
            string numericPrefix = (fileCountPrefix != 10) ? "0" + fileCountPrefix.ToString() : "10";
            string newFilePath = jsonFilesPath + "Backups/" + numericPrefix + "_ALLQUESTIONSv1_BACKUP(" + timestamp + ".json";

            // Check if a backup exists with the prefix
            string filename = "";
            for (int i = 0; i < backupsList.Length; i++) {
                filename = Path.GetFileNameWithoutExtension(backupsList[i]);
                if (filename.Substring(0, 2) == numericPrefix.ToString()) {
                    File.Delete(backupsList[i]);
                    break;
                }
            }

            File.Move(jsonFilesPath + "ALLQUESTIONSv1.json", newFilePath);
        }

        Utility.QuestionSetToJson(setData, "ALLQUESTIONSv1", jsonFilesPath);
        AssetDatabase.Refresh();
        selectedQuestion = null;
    }

    #region WINDOWS
    private void DrawSelectWindow(int id) {
        if (enumSelectWindow != null) {
            enumSelectWindow.DrawWindow();
        }
        if (textInputWindow != null) {
            textInputWindow.DrawWindow();
        }
        GUI.DragWindow();
    }

    private void OpenTextInputWindow() {
        if (textInputWindow == null) {
            searchString = "";
            textInputWindow = CreateInstance<TextInputEditorWindow>();
            textInputWindow.Init();
            textInputWindow.SetCallbacks(OnTextInputOkPressed, OnTextInputCancelPressed);
            textInputWindow.windowRect = new Rect((Screen.width / 2) - (textInputWindow.minSize.x / 2), (Screen.height / 2) - (textInputWindow.minSize.y / 2), textInputWindow.minSize.x, textInputWindow.minSize.y);
            inputDisabled = true;
        }
    }

    private void OnTextInputOkPressed(string value) {
        textInputWindow = null;
        inputDisabled = false;
        searchString = value.ToUpper();
        Repaint();
        SearchForQuestion();
    }

    private void OnTextInputCancelPressed() {
        textInputWindow = null;
        inputDisabled = false;
        searchString = "";
    }

    private void OpenEnumSelectWindow(Enum e, int selectedValue) {
        if (enumSelectWindow == null) {
            enumSelectWindow = CreateInstance<EnumSelectWindow>();
            enumSelectWindow.Init(e, selectedValue);
            enumSelectWindow.SetCallbacks(OnEnumSelectOKPressed, OnEnumSelectCancelPressed);
            enumSelectWindow.windowRect = new Rect((Screen.width / 2) - (enumSelectWindow.minSize.x / 2), (Screen.height / 2) - (enumSelectWindow.minSize.y / 2), enumSelectWindow.minSize.x, enumSelectWindow.minSize.y);
            inputDisabled = true;
        }
    }

    private void OnEnumSelectOKPressed(int selectedValue) {
        enumSelectWindow = null;
        inputDisabled = false;
        if (enumWindowCallback != null) {
            enumWindowCallback.Invoke(selectedValue);
            enumWindowCallback = null;
        }
    }

    private void OnQuestionCategorySelected(int selectedValue) {
        if (selectedQuestion != null) {
            selectedQuestion.questionCategory = (QuestionCategory)selectedValue;
        }
    }

    private void OnQuestionTypeSelected(int selectedValue) {
        if (selectedQuestion != null) {
            selectedQuestion.questionType = (QuestionType)selectedValue;
        }
    }

    private void OnButtonColorAnswerSelected(int selectedValue) {
        if (selectedQuestion != null) {
            selectedQuestion.correctButtonColorAnswer = (ColorAnswerValue)selectedValue;
        }
    }

    private void OnLabelColorAnswerSelected(int selectedValue) {
        if (selectedQuestion != null) {
            selectedQuestion.correctLabelColorAnswer = (ColorAnswerValue)selectedValue;
        }
    }

    private void OnSubCategorySelected(int selectedValue) {
        if (selectedQuestion != null) {
            selectedQuestion.subCategories[subCategoryIndexToChange] = (QuestionCategory)selectedValue;
            subCategoryIndexToChange= -1;
        }
    }

    private void OnAnswerCategorySelected(int selectedValue) {
        if (selectedQuestion != null) {
            selectedQuestion.answerCategories[answerCategoryIndexToChange] = (AnswerCategory)selectedValue;
            answerCategoryIndexToChange = -1;
        }
    }

    private void OnEnumSelectCancelPressed() {
        enumSelectWindow = null;
        inputDisabled = false;
        //gameRuleIndexToChange = -1;
    }
    #endregion WINDOWS

    #region EDITOR_MENU_METHODS
    [MenuItem("Game Tools/Game Data Editor %&q")]
    static void Init() {
        // Get existing open window or if none, make a new one:
        QuestionEditorWindow window = (QuestionEditorWindow)EditorWindow.GetWindow(typeof(QuestionEditorWindow));
        window.Show();
    }
    #endregion EDITOR_MENU_METHODS

    #region DRAW_FIELD_METHODS
    private void DrawHorizontalLabeledTextField(string label, ref string value) {
        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(label);
        value = EditorGUILayout.TextField(value);
        GUILayout.EndHorizontal();
    }

    private T DrawHorizontalLabeledEnumField<T>(string label, Enum enumType) {
        T rVal;
        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(label);
        rVal = (T)(object)EditorGUILayout.EnumPopup(enumType);
        GUILayout.EndHorizontal();
        return rVal;
    }

    private void DrawHorizontalLabeledFloatField(string label, ref float value) {
        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(label);
        value = EditorGUILayout.FloatField(value);
        GUILayout.EndHorizontal();
    }

    private void DrawHorizontalLabeledIntField(string label, ref int value) {
        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(label);
        value = EditorGUILayout.IntField(value);
        GUILayout.EndHorizontal();
    }
    #endregion DRAW_FIELD_METHODS

    private void DrawToolbar() {
        GUILayout.BeginArea(new Rect(Padding, Padding, WindowWidth - (Padding * 2), ToolbarHeight - (Padding * 2)));
        GUILayout.BeginHorizontal();

        GUI.color = (currentWindowMode == WindowMode.QuestionUtil && !viewingGeneratedSets) ? CustomColor.GetColor(ColorName.SUBMIT_GREEN) : Color.white;
        if (GUILayout.Button("QUESTION", GUILayout.Width(ToolbarButtonWidth), GUILayout.Height(ToolbarButtonHeight))) {
            currentWindowMode = WindowMode.QuestionUtil;
            viewingGeneratedSets = false;
            LoadQuestions();
        }

        GUI.color = (currentWindowMode == WindowMode.QuestionUtil && viewingGeneratedSets) ? CustomColor.GetColor(ColorName.SUBMIT_GREEN) : Color.white;
        if (GUILayout.Button("Generated", GUILayout.Width(ToolbarButtonWidth), GUILayout.Height(ToolbarButtonHeight))) {
            currentWindowMode = WindowMode.QuestionUtil;
            viewingGeneratedSets = true;
            LoadGeneratedQuestions();
        }

        GUI.color = (currentWindowMode == WindowMode.JSONUtil) ? CustomColor.GetColor(ColorName.SUBMIT_GREEN) : Color.white;
        if (GUILayout.Button("JSON", GUILayout.Width(ToolbarButtonWidth), GUILayout.Height(ToolbarButtonHeight))) {
            viewingGeneratedSets = false;
            currentWindowMode = WindowMode.JSONUtil;
        }
        GUI.color = Color.white;
        
        GUILayout.Space(10);
        GUILayout.BeginHorizontal();
        int baseFontSize = GUI.skin.label.fontSize;
        TextAnchor baseFontAnchor = GUI.skin.label.alignment;
        GUI.skin.label.alignment = TextAnchor.UpperRight;
        GUI.skin.label.fontSize = 14;
        GUILayout.Label("Custom Set", GUILayout.Width(ToolbarButtonWidth / 2));
        GUI.skin.label.fontSize = baseFontSize;
        GUI.skin.label.alignment = baseFontAnchor;
        generatedQuestionsPath = EditorGUILayout.TextArea(generatedQuestionsPath, myGUISkin.GetStyle("textarea"), GUILayout.Height(ToolbarButtonHeight), GUILayout.Width(ToolbarButtonWidth));
        GUILayout.EndHorizontal();

        // == End of left side of toolbar == //
        
        // Pushes everything else to the right
        GUILayout.FlexibleSpace();
        if (currentWindowMode == WindowMode.QuestionUtil) {

            GUI.color = CustomColor.GetColor(ColorName.SONI_POOP);
            if (GUILayout.Button("Check Dupes", GUILayout.Width(ToolbarButtonWidth), GUILayout.Height(ToolbarButtonHeight))) {
                //PrintLongestQuestion();

                for (int i = 0; i < questions.Count; i++) {
                    for (int j = 0; j < questions.Count; j++) {
                        if (i != j && questions[i].uid == questions[j].uid) {
                            Debug.Log(questions[i].uid);
                        }
                    }
                }
            }
            GUI.color = Color.white;

            GUILayout.Space(20);

            GUI.color = CustomColor.GetColor(ColorName.YALLOW);
            if (GUILayout.Button("NEW", GUILayout.Width(ToolbarButtonWidth), GUILayout.Height(ToolbarButtonHeight))) {
                Question q = new Question();
                questions.Add(q);
                selectedQuestion = q;
                GUI.FocusControl("");
                menuScrollPosition = menuScrollPosition.SetY(100000);
            }

            GUI.color = CustomColor.GetColor(ColorName.SKIER_BLUE);
            if (GUILayout.Button("DEFAULTS", GUILayout.Width(ToolbarButtonWidth), GUILayout.Height(ToolbarButtonHeight))) {
                if (selectedQuestion != null) {
                    selectedQuestion.questionCategory = QuestionCategory.General;
                    selectedQuestion.questionType = QuestionType.Label;
                    selectedQuestion.answerCategories.Add(AnswerCategory.Uncategorized);
                    if (string.IsNullOrEmpty(selectedQuestion.uid)) {
                        selectedQuestion.GenerateUID();
                    }
                }
            }

            GUI.color = CustomColor.GetColor(ColorName.PURPLE_PLUM);
            if (GUILayout.Button("SEARCH ID", GUILayout.Width(ToolbarButtonWidth), GUILayout.Height(ToolbarButtonHeight))) {
                
            }

            GUI.color = (string.IsNullOrEmpty(searchString)) ? CustomColor.GetColor(ColorName.PRS_PURPLE) : CustomColor.GetColor(ColorName.ERROR_RED);
            string btnLabel = (string.IsNullOrEmpty(searchString)) ? "SEARCH KEYWORD" : "CLEAR SEARCH";
            if (GUILayout.Button(btnLabel, GUILayout.Width(ToolbarButtonWidth), GUILayout.Height(ToolbarButtonHeight))) {
                if (string.IsNullOrEmpty(searchString)) {
                    OpenTextInputWindow();
                }
                else {
                    searchString = "";
                }
            }

            GUILayout.Space(40);
            GUI.color = CustomColor.GetColor(ColorName.SUBMIT_GREEN);
            if (GUILayout.Button("SAVE", GUILayout.Width(ToolbarButtonWidth), GUILayout.Height(ToolbarButtonHeight))) {
                SaveQuestions();
            }
            GUI.color = Color.white;
        }

        GUILayout.EndHorizontal();
        GUILayout.EndArea();
    }

    private void DrawLeftMenu() {
        GUILayout.BeginArea(new Rect(Padding, ToolbarHeight, LeftColumnWidth - (Padding * 2), WindowHeight - ToolbarHeight - (Padding * 2)));

        if (currentWindowMode == WindowMode.JSONUtil) {
            if (GUILayout.Button("DO")) {

                AnswerTable table = Resources.Load<AnswerTable>("DataObjects/ActressesTable");
                string answer = "";
                StringBuilder sb = new StringBuilder();
                for (int i = 0; i < table.answers.Count; i++) {
                    answer = table.answers[i];

                    // Remove from index of word
                    //index = answer.IndexOf("ACTRESS");
                    //if (index > 0)
                    //    table.answers[i] = answer.Substring(0, index);

                    // Remove end white space
                    //if (answer[answer.Length - 1] == ' ') {
                    //    table.answers[i] = answer.Remove(answer.Length - 1);
                    //}

                    // Add commas for export
                    sb.Append(answer);
                    if (i < table.answers.Count - 1) {
                        sb.Append(",");
                    }
                }


                
                System.IO.Directory.CreateDirectory("Assets/NewFolderWithExports");

                string folderPath = System.IO.Path.Combine("Assets/NewFolderWithExports", "Exported.txt");

                Debug.LogFormat(("Saving at: {0}").Colored(Color.magenta), folderPath);
                System.IO.File.WriteAllText(folderPath,sb.ToString());
                AssetDatabase.Refresh();

            }
        }
        else if (currentWindowMode == WindowMode.QuestionUtil) {
            GUI.color = CustomColor.GetColor(ColorName.URGENT_URANGE);
            int totalMath = 0;
            int totalBC = 0;
            for (int i = 0; i < questions.Count; i++) {
                if (questions[i].questionType == QuestionType.RandomMath) {
                    totalMath++;
                }
                else if (questions[i].questionType == QuestionType.ButtonColor) {
                    totalBC++;
                }
            }
            GUILayout.Label("Total: " + questions.Count + " | Math: " + totalMath.ToString() + " | BC: " + totalBC.ToString());
            GUI.color = Color.white;
            menuScrollPosition = GUILayout.BeginScrollView(menuScrollPosition);

            for (int i = 0; i < questions.Count; i++) {
                GUI.color = (selectedQuestion != null && selectedQuestion == questions[i]) ? CustomColor.GetColor(ColorName.SUBMIT_GREEN) : (i % 2 == 0) ? Color.white : CustomColor.GetColor(ColorName.YALLOW);

                // ==** Use here to check questions for a specific trait **== //

                // If there's a search string
                if (!string.IsNullOrEmpty(searchString)) {
                    string qText = questions[i].questionText.ToUpper();
                    string aText = questions[i].correctLabelAnswer.ToUpper();
                    if (qText.Contains(searchString) || aText.Contains(searchString) || questions[i].uid == searchString) {
                        GUI.color = CustomColor.GetColor(ColorName.ERROR_RED);
                        if (GUILayout.Button(questions[i].questionText + ": " + i.ToString())) {
                            selectedQuestion = questions[i];
                            questionScrollPosition = Vector2.zero;
                            GUI.FocusControl("");
                        }
                    }
                }
                else {
                    if (GUILayout.Button(questions[i].questionText)) {
                        selectedQuestion = questions[i];
                        questionScrollPosition = Vector2.zero;
                        GUI.FocusControl("");
                    }
                }
                GUI.color = Color.white;
            }

            GUILayout.EndScrollView();
        }

        GUILayout.EndArea(); // Area of left column size
    }

    private void DrawRightContent() {
        GUILayout.BeginArea(new Rect(LeftColumnWidth + Padding, ToolbarHeight, RightColumnWidth - (Padding * 2), WindowHeight - ToolbarHeight - (Padding * 2)));

        // ================================================== //
        // Math question
        // game rules (eg. answer questions x and y wrong. Answer them wrong if they're option W or V... and so on)
        // ================================================== //
        if (currentWindowMode == WindowMode.JSONUtil) {
            DrawJSONRightColumn();
        }
        else if (currentWindowMode == WindowMode.QuestionUtil) {
            DrawQuestionRightColumn();
        }

        GUILayout.EndArea(); // Area of right column size
    }

    private void DrawJSONRightColumn() {
        DrawHorizontalLabeledTextField("JSON File Path", ref jsonPathToUse);
        DrawHorizontalLabeledTextField("Export To Path", ref splitExportPath);
        DrawHorizontalLabeledIntField("Question Sets To Create", ref qsObjectsToSplitInto);

        // == Load QuestionSetData from path indicated == //
        // THIS IS BAD, IT'S BEING LOADED EVERY FRAME
        QuestionSetData loadedQSData = null;
        int totalQuestionsToSplit = 0;
        int totalQuestionsUsed = 0;
        if (!string.IsNullOrEmpty(jsonPathToUse)) {
            loadedQSData = JsonUtility.FromJson<QuestionSetData>(System.IO.File.ReadAllText(System.IO.Path.Combine(Application.dataPath, jsonPathToUse.Replace("Assets/", ""))));
            totalQuestionsToSplit = loadedQSData.questions.Count;
        }

        GUILayout.Space(10);

        // == Create the Export Info == //
        ExportToQuestionSetInfo exportInfo;
        for (int i = 0; i < qsObjectsToSplitInto; i++) {
            if (!qsExportInfos.ContainsKey(i)) {
                exportInfo = new ExportToQuestionSetInfo();
                qsExportInfos[i] = exportInfo;
            }
            else {
                exportInfo = qsExportInfos[i];
            }

            // == Draw Export Info Variable Fields == //
            GUILayout.Label("Object " + (i + 1).ToString());
            DrawHorizontalLabeledTextField("Filename", ref exportInfo.filename);
            exportInfo.category = DrawHorizontalLabeledEnumField<QuestionCategory>("Category", exportInfo.category);
            exportInfo.totalQuestions = EditorGUILayout.IntSlider(exportInfo.totalQuestions, 0, totalQuestionsToSplit - totalQuestionsUsed);
            DrawHorizontalLabeledIntField("Cost", ref exportInfo.cost);
            totalQuestionsUsed += exportInfo.totalQuestions;
        }

        GUILayout.Space(10);

        // == Split Logic == //
        GUI.color = CustomColor.GetColor(ColorName.YALLOW);
        if (GUILayout.Button("Split") && !string.IsNullOrEmpty(splitExportPath) && loadedQSData != null && qsObjectsToSplitInto > 0) {

            // == Shuffle the questions into a Queue == //
            QuestionSet qs = null;
            string assetPathAndName = "";
            Queue<Question> shuffledQuestions = new Queue<Question>(Utility.ShuffleList<Question>(loadedQSData.questions, UnityEngine.Random.Range(1, 10291)));

            // == Create the number of QuestionSet objects indicated == //
            for (int i = 0; i < qsObjectsToSplitInto; i++) {
                exportInfo = qsExportInfos[i];
                qs = ScriptableObject.CreateInstance<QuestionSet>();

                // == Dequeue the shuffled questions into the newly created data object == //
                for (int j = 0; j < exportInfo.totalQuestions; j++) {
                    qs.data.questions.Add(shuffledQuestions.Dequeue());
                }
                qs.data.name = exportInfo.filename;
                qs.data.cost = exportInfo.cost;
                qs.data.category = exportInfo.category;
                qs.data.GenerateID();

                assetPathAndName = AssetDatabase.GenerateUniqueAssetPath(splitExportPath + "/" + exportInfo.filename + ".asset");
                AssetDatabase.CreateAsset(qs, assetPathAndName);
            }
            AssetDatabase.SaveAssets();
            AssetDatabase.Refresh();
        }
        GUI.color = Color.white;
    }

    private void DrawQuestionRightColumn() {
        if (selectedQuestion != null) {
            questionScrollPosition = GUILayout.BeginScrollView(questionScrollPosition);

            // == QUESTION TEXT == //
            GUILayout.BeginHorizontal();
            GUILayout.Label("Question", GUILayout.Width((RightColumnWidth / 3) - (Padding * 2)));
            selectedQuestion.questionText = EditorGUILayout.TextArea(selectedQuestion.questionText, myGUISkin.GetStyle("textarea"), GUILayout.Width(((RightColumnWidth / 3) * 2) - (Padding * 3)));
            GUILayout.EndHorizontal();

            // == VERSION SET == //
            GUILayout.BeginHorizontal();
            GUILayout.Label("Version Set", GUILayout.Width((RightColumnWidth / 3) - (Padding * 2)));
            string tempSetString = EditorGUILayout.TextField(selectedQuestion.versionSet.ToString(), myGUISkin.GetStyle("textarea"), GUILayout.Width(((RightColumnWidth / 3) * 2) - (Padding * 3)), GUILayout.MinHeight(30));
            Int32.TryParse(tempSetString, out selectedQuestion.versionSet);
            GUILayout.EndHorizontal();

            /*
            // == INSTRUCTION == //
            GUILayout.BeginHorizontal();
            GUILayout.Label("Instruction", GUILayout.Width((RightColumnWidth / 3) - (Padding * 2)));
            //selectedQuestion.instruction = GUILayout.TextField(selectedQuestion.instruction, textAreaCharLimit, GUILayout.Width(((RightColumnWidth / 3) * 2) - (Padding * 3)));
            selectedQuestion.instruction = EditorGUILayout.TextField(selectedQuestion.instruction, myGUISkin.GetStyle("textarea"), GUILayout.Width(((RightColumnWidth / 3) * 2) - (Padding * 3)), GUILayout.MinHeight(30));
            GUILayout.EndHorizontal();
            */

            // == UID == //
            GUILayout.BeginHorizontal();
            GUILayout.Label("UID", GUILayout.Width((RightColumnWidth / 3) - (Padding * 2)));
            if (string.IsNullOrEmpty(selectedQuestion.uid)) {
                GUI.color = CustomColor.GetColor(ColorName.RACY_RED);
                if (GUILayout.Button("Generate UID", GUILayout.Width(((RightColumnWidth / 3) * 2) - (Padding * 3)))) {
                    selectedQuestion.GenerateUID();
                }
                GUI.color = Color.white;
            }
            else {
                GUILayout.Label(selectedQuestion.uid, GUILayout.Width(((RightColumnWidth / 3) * 2) - (Padding * 3)));
            }
            GUILayout.EndHorizontal();

            // == QUESTION CATEGORY == //
            GUILayout.BeginHorizontal();
            GUILayout.Label("Question Category", GUILayout.Width((RightColumnWidth / 3) - (Padding * 2)));
            GUI.color = CustomColor.GetColor(ColorName.URGENT_URANGE);
            if (GUILayout.Button(Utility.EnumNameToReadable(selectedQuestion.questionCategory.ToString()), GUILayout.Width(((RightColumnWidth / 3) * 2) - (Padding * 3)))) {
                OpenEnumSelectWindow(selectedQuestion.questionCategory, (int)selectedQuestion.questionCategory);
                enumWindowCallback = OnQuestionCategorySelected;
            }
            GUI.color = Color.white;
            GUILayout.EndHorizontal();

            // == QUESTION TYPE == //
            GUILayout.BeginHorizontal();
            GUILayout.Label("Question Type", GUILayout.Width((RightColumnWidth / 3) - (Padding * 2)));
            GUI.color = CustomColor.GetColor(ColorName.URGENT_URANGE);
            if (GUILayout.Button(Utility.EnumNameToReadable(selectedQuestion.questionType.ToString()), GUILayout.Width(((RightColumnWidth / 3) * 2) - (Padding * 3)))) {
                OpenEnumSelectWindow(selectedQuestion.questionType, (int)selectedQuestion.questionType);
                enumWindowCallback = OnQuestionTypeSelected;
            }
            GUI.color = Color.white;
            GUILayout.EndHorizontal();

            // == SUB CATEGORIES == //
            GUILayout.BeginHorizontal();
            GUILayout.Label("Sub Categories", GUILayout.Width((RightColumnWidth / 3) - (Padding * 2)));
            GUI.color = CustomColor.GetColor(ColorName.RACY_RED);
            if (GUILayout.Button("-", GUILayout.Width(ToolbarButtonWidth), GUILayout.Height(RightFieldButtonHeight))) {
                if (selectedQuestion.subCategories.Count > 0) {
                    selectedQuestion.subCategories.RemoveAt(selectedQuestion.subCategories.Count - 1);
                }
            }
            GUI.color = CustomColor.GetColor(ColorName.SUBMIT_GREEN);
            if (GUILayout.Button("+", GUILayout.Width(ToolbarButtonWidth), GUILayout.Height(RightFieldButtonHeight))) {
                selectedQuestion.subCategories.Add(QuestionCategory.General);
            }
            GUI.color = Color.white;
            GUILayout.EndHorizontal();

            for (int i = 0; i < selectedQuestion.subCategories.Count; i++) {
                GUILayout.BeginHorizontal();
                GUILayout.Label("Sub Category " + (i + 1).ToString(), GUILayout.Width((RightColumnWidth / 3) - (Padding * 2)));
                GUI.color = CustomColor.GetColor(ColorName.URGENT_URANGE);
                if (GUILayout.Button(Utility.EnumNameToReadable(selectedQuestion.subCategories[i].ToString()), GUILayout.Width(((RightColumnWidth / 3) * 2) - (Padding * 3)))) {
                    OpenEnumSelectWindow(selectedQuestion.subCategories[i], (int)selectedQuestion.subCategories[i]);
                    subCategoryIndexToChange = i;
                    enumWindowCallback = OnSubCategorySelected;
                }
                GUI.color = Color.white;
                GUILayout.EndHorizontal();
            }

            // == ANSWERS HEADER LABEL == //
            GUILayout.Space(20);
            GUI.color = CustomColor.GetColor(ColorName.YALLOW);
            GUILayout.Label("-Answers-");
            
            GUI.color = Color.white;

            // == CORRECT LABEL ANSWER == //
            GUILayout.BeginHorizontal();
            GUILayout.Label("Correct Label Answer", GUILayout.Width((RightColumnWidth / 3) - (Padding * 2)));
            selectedQuestion.correctLabelAnswer = EditorGUILayout.TextField(selectedQuestion.correctLabelAnswer, myGUISkin.GetStyle("textarea"), GUILayout.Width(((RightColumnWidth / 3) * 2) - (Padding * 3)), GUILayout.MinHeight(30));
            GUILayout.EndHorizontal();

            if (selectedQuestion.questionType == QuestionType.ButtonColor) {
                // == CORRECT BUTTON COLOR ANSWER == //
                GUILayout.BeginHorizontal();
                GUILayout.Label("Correct Button Color Answer", GUILayout.Width((RightColumnWidth / 3) - (Padding * 2)));
                GUI.color = CustomColor.GetColor(ColorName.URGENT_URANGE);
                if (GUILayout.Button(Utility.EnumNameToReadable(selectedQuestion.correctButtonColorAnswer.ToString()), GUILayout.Width(((RightColumnWidth / 3) * 2) - (Padding * 3)))) {
                    OpenEnumSelectWindow(selectedQuestion.correctButtonColorAnswer, (int)selectedQuestion.correctButtonColorAnswer);
                    enumWindowCallback = OnButtonColorAnswerSelected;
                }
                GUI.color = Color.white;
                GUILayout.EndHorizontal();

                // == CORRECT LABEL COLOR ANSWER == //
                GUILayout.BeginHorizontal();
                GUILayout.Label("Correct Label Color Answer", GUILayout.Width((RightColumnWidth / 3) - (Padding * 2)));
                GUI.color = CustomColor.GetColor(ColorName.URGENT_URANGE);
                if (GUILayout.Button(Utility.EnumNameToReadable(selectedQuestion.correctLabelColorAnswer.ToString()), GUILayout.Width(((RightColumnWidth / 3) * 2) - (Padding * 3)))) {
                    OpenEnumSelectWindow(selectedQuestion.correctLabelColorAnswer, (int)selectedQuestion.correctLabelColorAnswer);
                    enumWindowCallback = OnLabelColorAnswerSelected;
                }
                GUI.color = Color.white;
                GUILayout.EndHorizontal();
            }

            // == ANSWER CATEGORIES == //
            GUILayout.BeginHorizontal();
            GUILayout.Label("Answer Categories", GUILayout.Width((RightColumnWidth / 3) - (Padding * 2)));
            GUI.color = CustomColor.GetColor(ColorName.RACY_RED);
            if (GUILayout.Button("-", GUILayout.Width(ToolbarButtonWidth), GUILayout.Height(RightFieldButtonHeight))) {
                if (selectedQuestion.answerCategories.Count > 0) {
                    selectedQuestion.answerCategories.RemoveAt(selectedQuestion.answerCategories.Count - 1);
                }
            }
            GUI.color = CustomColor.GetColor(ColorName.SUBMIT_GREEN);
            if (GUILayout.Button("+", GUILayout.Width(ToolbarButtonWidth), GUILayout.Height(RightFieldButtonHeight))) {
                selectedQuestion.answerCategories.Add(AnswerCategory.Nouns);
            }
            GUI.color = Color.white;
            GUILayout.EndHorizontal();

            for (int i = 0; i < selectedQuestion.answerCategories.Count; i++) {
                GUILayout.BeginHorizontal();
                GUILayout.Label("Answer Category " + (i + 1).ToString(), GUILayout.Width((RightColumnWidth / 3) - (Padding * 2)));
                GUI.color = CustomColor.GetColor(ColorName.URGENT_URANGE);
                if (GUILayout.Button(Utility.EnumNameToReadable(selectedQuestion.answerCategories[i].ToString()), GUILayout.Width(((RightColumnWidth / 3) * 2) - (Padding * 3)))) {
                    OpenEnumSelectWindow(selectedQuestion.answerCategories[i], (int)selectedQuestion.answerCategories[i]);
                    answerCategoryIndexToChange = i;
                    enumWindowCallback = OnAnswerCategorySelected;
                }
                GUI.color = Color.white;
                GUILayout.EndHorizontal();
            }

            // == BUTTON LABEL HEADER LABEL == //
            GUILayout.Space(20);
            GUI.color = CustomColor.GetColor(ColorName.YALLOW);
            GUILayout.Label("-Button Label Options-");
            GUI.color = Color.white;

            // == BUTTON LABEL OPTIONS == //
            GUILayout.BeginHorizontal();
            GUILayout.Label("Button Label Options", GUILayout.Width((RightColumnWidth / 3) - (Padding * 2)));
            GUI.color = CustomColor.GetColor(ColorName.RACY_RED);
            if (GUILayout.Button("-", GUILayout.Width(ToolbarButtonWidth), GUILayout.Height(RightFieldButtonHeight))) {
                if (selectedQuestion.buttonLabelOptions.Count > 0) {
                    selectedQuestion.buttonLabelOptions.RemoveAt(selectedQuestion.buttonLabelOptions.Count - 1);
                }
            }
            GUI.color = CustomColor.GetColor(ColorName.SUBMIT_GREEN);
            if (GUILayout.Button("+", GUILayout.Width(ToolbarButtonWidth), GUILayout.Height(RightFieldButtonHeight))) {
                string label = "";
                
                if (!string.IsNullOrEmpty(selectedQuestion.correctLabelAnswer)) {
                    label = selectedQuestion.correctLabelAnswer;
                    for (int i = 0; i < selectedQuestion.buttonLabelOptions.Count; i++) {
                        if (selectedQuestion.buttonLabelOptions[i] == selectedQuestion.correctLabelAnswer) {
                            label = "";
                            break;
                        }
                    }
                }

                if (!string.IsNullOrEmpty(label)) {
                    selectedQuestion.buttonLabelOptions.Add(label);
                }
                else {
                    selectedQuestion.buttonLabelOptions.Add("");
                }
                questionScrollPosition = questionScrollPosition.AddY(10000);
            }
            GUI.color = Color.white;
            GUILayout.EndHorizontal();

            for (int i = 0; i < selectedQuestion.buttonLabelOptions.Count; i++) {
                GUILayout.BeginHorizontal();
                GUILayout.Label("Button Label Option " + (i + 1).ToString(), GUILayout.Width((RightColumnWidth / 3) - (Padding * 2)));
                selectedQuestion.buttonLabelOptions[i] = EditorGUILayout.TextField(selectedQuestion.buttonLabelOptions[i], myGUISkin.GetStyle("textarea"), GUILayout.Width(((RightColumnWidth / 3) * 2) - (Padding * 3)), GUILayout.MinHeight(30));
                GUILayout.EndHorizontal();
            }

            // == DELETE QUESTION == //
            GUILayout.Space(20);
            GUILayout.BeginHorizontal();
            GUILayout.FlexibleSpace();

            if (deleteConfirmWaiting) {
                GUI.color = CustomColor.GetColor(ColorName.RACY_RED);
                if (GUILayout.Button("Cancel", GUILayout.Width(ToolbarButtonWidth), GUILayout.Height(RightFieldButtonHeight))) {
                    deleteConfirmWaiting = false;
                }
                GUILayout.Space(10);
                GUI.color = CustomColor.GetColor(ColorName.SUBMIT_GREEN);
                if (GUILayout.Button("Confirm", GUILayout.Width(ToolbarButtonWidth), GUILayout.Height(RightFieldButtonHeight))) {
                    questions.Remove(selectedQuestion);
                    SaveQuestions();
                    deleteConfirmWaiting = false;
                }
            }
            else {
                GUI.color = CustomColor.GetColor(ColorName.RACY_RED);
                if (GUILayout.Button("Delete", GUILayout.Width(ToolbarButtonWidth * 2), GUILayout.Height(RightFieldButtonHeight))) {
                    deleteConfirmWaiting = true;
                }
            }
            GUI.color = Color.white;

            GUILayout.FlexibleSpace();
            GUILayout.EndHorizontal();

            GUILayout.EndScrollView();
        }
    }

    void OnGUI() {
        if (menuBGTexture == null || windowBGTexture == null) {
            InitTextures();
        }

        baseGUISkin = GUI.skin;
        GUI.skin = myGUISkin;
        GUI.color = darkGray;
        GUI.DrawTexture(new Rect(0, 0, maxSize.x - 4, maxSize.y - 4), raysTexture, ScaleMode.ScaleAndCrop);
        GUI.color = Color.white;

        EditorGUI.BeginDisabledGroup(inputDisabled);

        GUILayout.BeginVertical();
        DrawToolbar();

        GUILayout.BeginHorizontal();

        DrawLeftMenu();

        DrawRightContent();

        GUILayout.EndHorizontal();

        GUILayout.EndVertical();
        
        EditorGUI.EndDisabledGroup();

        BeginWindows();
        if (enumSelectWindow != null) {
            enumSelectWindow.windowRect = GUILayout.Window(0, enumSelectWindow.windowRect, DrawSelectWindow, enumSelectWindow.windowTitle);
        }
        if (textInputWindow != null) {
            textInputWindow.windowRect = GUILayout.Window(0, textInputWindow.windowRect, DrawSelectWindow, textInputWindow.windowTitle);
        }
        EndWindows();

        GUI.skin = baseGUISkin;
    }

    private void SearchForQuestion() {
        Debug.Log(searchString);
    }

}

public class ExportToQuestionSetInfo {

    public QuestionCategory category;
    public string filename = "";
    public int totalQuestions = 0;
    public int cost = 0;

}