﻿using UnityEngine;
using UnityEditor;
using UnityEditorInternal;
using System.Collections;
using System.Collections.Generic;

[CustomEditor(typeof(SFXManager))]
public class SFXManagerEditor : Editor {

    //private static List<bool> expandedFlags = new List<bool>();

    //public override void OnInspectorGUI()
    //{
    //    EditorGUILayout.BeginHorizontal();

    //    List<SFX> sfxList = (target as SFXManager).SFXList;

    //    SerializedProperty list = serializedObject.FindProperty("_SFXList");
    //    EditorGUILayout.PropertyField(list.FindPropertyRelative("Array.size"));

    //    EditorGUILayout.EndHorizontal();

    //    SFX sfx;
    //    string label = "";

    //    EditorGUI.indentLevel = 1;
    //    while (expandedFlags.Count < sfxList.Count)
    //    {
    //        expandedFlags.Add(false); // expand by default
    //    }

    //    while (expandedFlags.Count > sfxList.Count)
    //    {
    //        expandedFlags.RemoveAt(expandedFlags.Count - 1);
    //    }

    //    for (int i = 0; i < sfxList.Count; i++)
    //    {
    //        sfx = sfxList[i];
    //        label = StringUtility.FormatEnumProperty(sfx.SFXType.ToString());
    //        expandedFlags[i] = EditorGUILayout.Foldout(expandedFlags[i], label);

    //        if (expandedFlags[i])
    //        {
    //            sfx.SFXType = (SFX_TYPE)EditorGUILayout.EnumPopup("SFX Type", sfx.SFXType);
    //            sfx.clip = (AudioClip)EditorGUILayout.ObjectField("Audio Clip", sfx.clip, typeof(AudioClip), true);
    //        }
    //    }
    //    if (GUI.changed)
    //        EditorUtility.SetDirty(target);
    //    serializedObject.ApplyModifiedProperties();
    //}

    private ReorderableList list;

    private void OnEnable() {
        list = new ReorderableList(serializedObject, serializedObject.FindProperty("sfxList"), true, true, true, true);
        list.elementHeight = EditorGUIUtility.singleLineHeight * 3f;

        list.drawElementCallback = (Rect rect, int index, bool isActive, bool isFocused) => {
            SerializedProperty element = list.serializedProperty.GetArrayElementAtIndex(index);

            rect.y += 2;

            EditorGUI.LabelField(new Rect(rect.x, rect.y, rect.width / 2, EditorGUIUtility.singleLineHeight), "SFX Type");
            EditorGUI.PropertyField(new Rect(rect.x, rect.y + EditorGUIUtility.singleLineHeight, rect.width / 2, EditorGUIUtility.singleLineHeight),
                element.FindPropertyRelative("SFXType"), GUIContent.none);

            EditorGUI.LabelField(new Rect(rect.x + rect.width / 2, rect.y, rect.width / 2, EditorGUIUtility.singleLineHeight), "Audio Clip");
            EditorGUI.PropertyField(new Rect(rect.x + rect.width / 2, rect.y + EditorGUIUtility.singleLineHeight, rect.width / 2, EditorGUIUtility.singleLineHeight),
                element.FindPropertyRelative("clip"), GUIContent.none);
        };

        list.drawHeaderCallback = (Rect rect) => {
            EditorGUI.LabelField(rect, "SFX List");
        };
    }

    public override void OnInspectorGUI() {
        serializedObject.Update();
        EditorGUILayout.Space();
        list.DoLayoutList();
        serializedObject.ApplyModifiedProperties();
    }



}
