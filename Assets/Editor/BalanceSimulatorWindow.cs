﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System;

public class BalanceSimulatorWindow : EditorWindow {

    private int uniqueQuestionReward = 10;
    private int seenQuestionReward = 2;
    private int questionSetCompleteReward = 200;
    private int selectedResetIndex = -1;

    private bool inputDisabled = false;

    private Color darkGray = new Color(0.172549f, 0.172549f, 0.172549f, 1f);
    private Texture2D windowBGTexture;
    private Texture2D menuBGTexture;
    private Texture2D raysTexture;
    private GUISkin baseGUISkin;
    private GUISkin myGUISkin;

    private Vector2 menuScrollPosition;
    private Vector2 leftScrollPosition;
    private Vector2 rightScrollPosition;
    private QuestionSetGenerationRules generationRules;
    private QuestionSetResetData selectedResetData;

    private float Padding { get { return 10; } }
    private float WindowWidth { get { return position.width; } }
    private float WindowHeight { get { return position.height; } }
    private float ToolbarHeight { get { return 100; } }
    private float ToolbarButtonWidth { get { return 120; } }
    private float ToolbarButtonHeight { get { return 50; } }
    private float BodyColumnWidth { get { return WindowWidth / 2; } }
    private float RightFieldButtonHeight { get { return 28; } }

    void Awake() {
        this.minSize = new Vector2(1400, 800);
        InitTextures();
        GetGUISkin();
        GetGenerationRules();
    }

    void OnEnable() {
        InitTextures();
        GetGUISkin();
        GetGenerationRules();
    }

    void OnDestroy() {
        
    }

    private void GetGenerationRules() {
        selectedResetData = null;
        selectedResetIndex = -1;
        string setGenerationRulesPath = "DataObjects/GenerationRules";
        generationRules = Resources.Load<QuestionSetGenerationRules>(setGenerationRulesPath);
    }

    private void InitTextures() {
        raysTexture = (Texture2D)AssetDatabase.LoadAssetAtPath("Assets/Art/2D/RaysBackground.png", typeof(Texture2D));

        windowBGTexture = new Texture2D(1, 1, TextureFormat.RGBA32, false);
        windowBGTexture.SetPixel(0, 0, new Color(0.3803922f, 0.6431373f, 0.8666667f, 1f));
        windowBGTexture.Apply();

        menuBGTexture = new Texture2D(1, 1, TextureFormat.RGBA32, false);
        menuBGTexture.SetPixel(0, 0, new Color(0.3333333f, 0.5960785f, 0.9098039f, 1f));
        menuBGTexture.Apply();
    }

    private void GetGUISkin() {
        myGUISkin = (GUISkin)AssetDatabase.LoadAssetAtPath("Assets/Resources/MyGUISkin.guiskin", typeof(GUISkin));
    }

    #region EDITOR_MENU_METHODS
    [MenuItem("Game Tools/Game Data Editor %&b")]
    static void Init() {
        // Get existing open window or if none, make a new one:
        BalanceSimulatorWindow window = (BalanceSimulatorWindow)EditorWindow.GetWindow(typeof(BalanceSimulatorWindow));
        window.Show();
    }
    #endregion EDITOR_MENU_METHODS

    private void DrawHorizontalLabeledTextField(string label, string value, GUILayoutOption labelWidth, GUILayoutOption valueWidth, Color valueColor) {
        GUILayout.BeginHorizontal();
        GUILayout.Label(label, labelWidth);
        GUI.color = valueColor;
        GUILayout.Label(value, valueWidth);
        GUI.color = Color.white;
        GUILayout.EndHorizontal();
    }

    private int CompletedSetsNeeded(QuestionSetLevelData levelData) {
        int rVal = 0;
        int questionsPerSet = 20;

        int totalForUniqueSet = (questionsPerSet * uniqueQuestionReward) + questionSetCompleteReward;
        int targetForNextSet = levelData.cost;
        rVal = Mathf.CeilToInt((float)targetForNextSet / (float)totalForUniqueSet);

        return rVal;
    }

    private int SeenQuestionsNeeded(QuestionSetLevelData levelData) {
        int rVal = 0;
        int targetForNextSet = levelData.cost;
        rVal = Mathf.CeilToInt((float)targetForNextSet / (float)seenQuestionReward);

        return rVal;
    }

    /// <summary>
    /// Returns the amount the player would earn if they "one-shot" the level
    /// i.e. answer every question correct on the first try.
    /// </summary>
    /// <param name="levelData"></param>
    /// <returns></returns>
    private int OneShotValue(QuestionSetLevelData levelData) {
        int rVal = 0;
        int questionsPerSet = 20;

        int completeBonus = levelData.setCompleteBonus;
        rVal = (questionsPerSet * uniqueQuestionReward) + completeBonus;

        return rVal;
    }

    /// <summary>
    /// Returns the one shot value minus the cost to unlock the set.
    /// NOTE: CAN return negative value
    /// </summary>
    /// <param name="levelData"></param>
    /// <returns></returns>
    private int NetReward(QuestionSetLevelData levelData) {
        int rVal = 0;

        int oneShotValue = OneShotValue(levelData);
        rVal = oneShotValue - levelData.cost;

        return rVal;
    }


    /// <summary>
    /// Returns the possible currency that can be banked BEFORE unlocking the set passed in.
    /// By "possible currency", we mean if the player got every question correct on their first try up to that point
    /// and also have not viewed any forced or voluntary ads for rewards.
    /// Also includes the amount spent to unlock any set prior that is needed to be able to play this set
    /// </summary>
    /// <param name="levelIndex"></param>
    /// <param name="resetData">If NULL, uses selectedResetData</param>
    /// <returns></returns>
    private int PossibleCurrencyBeforeUnlock(int levelIndex, QuestionSetResetData resetData = null) {
        int rVal = 0;
        int resetIndex = selectedResetIndex;

        // If the resetData passed in was not null and not the selected data, get the index of it
        if (resetData != null && resetData != selectedResetData) {
            // Get reset index if the resetData is not the selected
            for (int i = 0; i < generationRules.resetDataList.Count; i++) {
                if (resetData == generationRules.resetDataList[i]) {
                    resetIndex = i;
                    break;
                }
            }
        }
        else if (resetData == null && selectedResetData != null) {
            // If the resetData passed in was null, default to the selectedResetData and index
            resetData = selectedResetData;
            resetIndex = selectedResetIndex;
        }

        // If any after the first level index was passed in
        if (levelIndex > 0) {
            int previousSetTotal = 0;
            for (int i = levelIndex - 1; i >= 0; i--) {
                previousSetTotal = NetReward(resetData.levelDatas[i]);

                rVal += previousSetTotal;
            }

            // If the reset data passed is not the first one, RECURSE (indirectly) this method, and get the previous generation AFTER currency
            if (resetIndex > 0) {
                rVal += PossibleCurrencyAfterGeneration(resetIndex - 1) - resetData.costToUnlock;
            }
        }
        else if (levelIndex == 0 && resetData != generationRules.resetDataList[0]) {
            // If the first level data of the generation was passed in
            rVal = PossibleCurrencyAfterGeneration(resetIndex - 1) - resetData.costToUnlock;
        }

        return rVal;
    }

    /// <summary>
    /// Returns the possible currency that can be available at the END of the generation passed in
    /// </summary>
    /// <param name="p"></param>
    /// <returns></returns>
    private int PossibleCurrencyAfterGeneration(int generationResetIndex) {
        int rVal = 0;

        if (generationResetIndex >= 0) {
            QuestionSetResetData resetData = generationRules.resetDataList[generationResetIndex];
            int levelDataIndex = resetData.levelDatas.Count - 1;
            QuestionSetLevelData levelData = resetData.GetLevelData(levelDataIndex);
            rVal = PossibleCurrencyBeforeUnlock(levelDataIndex, resetData) + NetReward(levelData);
        }

        return rVal;
    }

    private void DrawToolbar() {
        GUILayout.BeginArea(new Rect(Padding, Padding, WindowWidth - (Padding * 2), ToolbarHeight - (Padding * 2)));
        GUILayout.BeginVertical();
        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();

        GUILayout.Label("GAME BALANCE SIMULATOR 3000");
        
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();

        GUILayout.BeginHorizontal();
        string tempIndexString = "";
        /* == UNIQUE QUESTION REWARD == */
        //GUILayout.BeginHorizontal();
        GUILayout.Label("Unique Question Reward");
        GUILayout.Space(10);
        tempIndexString = GUILayout.TextField(uniqueQuestionReward.ToString(), GUILayout.Width(WindowWidth / 8));
        Int32.TryParse(tempIndexString, out uniqueQuestionReward);
        //GUILayout.EndHorizontal();

        GUILayout.FlexibleSpace();

        /* == SEEN QUESTION REWARD == */
        //GUILayout.BeginHorizontal();
        GUILayout.Label("Seen Question Reward");
        GUILayout.Space(10);
        tempIndexString = GUILayout.TextField(seenQuestionReward.ToString(), GUILayout.Width(WindowWidth / 8));
        Int32.TryParse(tempIndexString, out seenQuestionReward);
        //GUILayout.EndHorizontal();

        GUILayout.EndHorizontal();
        
        GUILayout.EndVertical();
        GUILayout.EndArea();
    }

    private void DrawLeftBody() {
        GUILayout.BeginArea(new Rect(Padding, ToolbarHeight, BodyColumnWidth - (Padding * 2), WindowHeight - ToolbarHeight - (Padding * 2)));
        leftScrollPosition = GUILayout.BeginScrollView(leftScrollPosition);

        for (int i = 0; i < generationRules.resetDataList.Count; i++) {
            GUILayout.BeginHorizontal();
            GUI.color = (selectedResetData != null && selectedResetData == generationRules.resetDataList[i]) ? CustomColor.GetColor(ColorName.SUBMIT_GREEN) : Color.white;
            if (GUILayout.Button(i.ToString())) {
                if (selectedResetData != generationRules.resetDataList[i]) {
                    selectedResetData = generationRules.resetDataList[i];
                    selectedResetIndex = i;
                    rightScrollPosition = Vector2.zero;
                }
            }
            GUI.color = Color.white;
            GUILayout.EndHorizontal();
        }

        GUILayout.EndScrollView();

        GUILayout.EndArea();
    }

    private void DrawRightBody() {
        GUILayout.BeginArea(new Rect(BodyColumnWidth + Padding, ToolbarHeight, BodyColumnWidth - Padding, WindowHeight - ToolbarHeight - (Padding * 2)));

        if (selectedResetData != null) {
            GUILayout.BeginVertical();
            
            rightScrollPosition = GUILayout.BeginScrollView(rightScrollPosition);

            float labelWidth = ((BodyColumnWidth / 3) * 2) - (Padding * 4);
            float valueWidth = (BodyColumnWidth / 3) - (Padding * 4);

            int startCurrencyBeforeBuying = PossibleCurrencyAfterGeneration(selectedResetIndex - 1);
            int startCurrencyAfterBuying = startCurrencyBeforeBuying - selectedResetData.costToUnlock;
            DrawHorizontalLabeledTextField("Possible Currency Before Unlock", startCurrencyBeforeBuying.ToString(), GUILayout.Width(labelWidth), GUILayout.Width(valueWidth), CustomColor.GetColor(ColorName.SKY_BLUE));
            DrawHorizontalLabeledTextField("Generation Cost", selectedResetData.costToUnlock.ToString(), GUILayout.Width(labelWidth), GUILayout.Width(valueWidth), CustomColor.GetColor(ColorName.RACY_RED));
            DrawHorizontalLabeledTextField("Possible Currency After Unlock", startCurrencyAfterBuying.ToString(), GUILayout.Width(labelWidth), GUILayout.Width(valueWidth), CustomColor.GetColor(ColorName.SUBMIT_GREEN));

            bool oneShotPathBroken = false;
            bool isInOneShotPath = false;

            for (int i = 0; i < selectedResetData.levelDatas.Count; i++) {
                QuestionSetLevelData levelData = selectedResetData.levelDatas[i];
                isInOneShotPath = false;

                if (PossibleCurrencyBeforeUnlock(i) >= levelData.cost && !oneShotPathBroken) {
                    isInOneShotPath = true;
                }
                else {
                    oneShotPathBroken = true;
                }

                GUILayout.BeginVertical();

                GUI.color = CustomColor.GetColor(ColorName.YALLOW);
                GUILayout.Label("-- Set " + i.ToString() + " --", GUILayout.Width((BodyColumnWidth - (Padding * 4))));
                GUI.color = Color.white;

                DrawHorizontalLabeledTextField("Is In One Shot Path", (isInOneShotPath) ? "True" : "False", GUILayout.Width(labelWidth), GUILayout.Width(valueWidth), CustomColor.GetColor(ColorName.SONI_POOP));

                DrawHorizontalLabeledTextField("Cost", levelData.cost.ToString(), GUILayout.Width(labelWidth), GUILayout.Width(valueWidth), CustomColor.GetColor(ColorName.SUBMIT_GREEN));
                DrawHorizontalLabeledTextField("Completion Bonus", levelData.setCompleteBonus.ToString(), GUILayout.Width(labelWidth), GUILayout.Width(valueWidth), CustomColor.GetColor(ColorName.YALLOW));

                int intValue = CompletedSetsNeeded(levelData);
                DrawHorizontalLabeledTextField("Unique Sets Needed To Unlock", intValue.ToString(), GUILayout.Width(labelWidth), GUILayout.Width(valueWidth), CustomColor.GetColor(ColorName.ROB_BLUE));

                intValue = SeenQuestionsNeeded(levelData);
                DrawHorizontalLabeledTextField("Seen Questions Needed To Unlock", intValue.ToString(), GUILayout.Width(labelWidth), GUILayout.Width(valueWidth), CustomColor.GetColor(ColorName.URGENT_URANGE));

                intValue = OneShotValue(levelData);
                DrawHorizontalLabeledTextField("One Shot Value", intValue.ToString(), GUILayout.Width(labelWidth), GUILayout.Width(valueWidth), CustomColor.GetColor(ColorName.SKIER_BLUE));

                intValue = PossibleCurrencyBeforeUnlock(i);
                DrawHorizontalLabeledTextField("Possible Currency Before Unlock", (isInOneShotPath) ? intValue.ToString() : "N/A", GUILayout.Width(labelWidth), GUILayout.Width(valueWidth), CustomColor.GetColor(ColorName.PRS_PURPLE));

                intValue = PossibleCurrencyBeforeUnlock(i) + NetReward(levelData);
                DrawHorizontalLabeledTextField("Possible Currency After Set Complete", (isInOneShotPath) ? intValue.ToString() : "N/A", GUILayout.Width(labelWidth), GUILayout.Width(valueWidth), CustomColor.GetColor(ColorName.SUBMIT_GREEN));

                intValue = NetReward(levelData);
                DrawHorizontalLabeledTextField("Net Reward", intValue.ToString(), GUILayout.Width(labelWidth), GUILayout.Width(valueWidth), CustomColor.GetColor(ColorName.ERROR_RED));


                GUILayout.EndVertical();
            }
            GUILayout.EndScrollView();

            GUILayout.EndVertical();
        }

        GUILayout.EndArea();
    }

    void OnGUI() {
        if (menuBGTexture == null || windowBGTexture == null) {
            InitTextures();
        }

        baseGUISkin = GUI.skin;
        GUI.skin = myGUISkin;
        GUI.color = darkGray;
        GUI.DrawTexture(new Rect(0, 0, maxSize.x - 4, maxSize.y - 4), raysTexture, ScaleMode.ScaleAndCrop);
        GUI.color = Color.white;

        EditorGUI.BeginDisabledGroup(inputDisabled);

        GUILayout.BeginVertical();
        DrawToolbar();

        GUILayout.BeginHorizontal();

        DrawLeftBody();

        DrawRightBody();

        //DrawLeftMenu();

        //DrawRightContent();

        GUILayout.EndHorizontal();

        GUILayout.EndVertical();

        EditorGUI.EndDisabledGroup();

        BeginWindows();

        EndWindows();

        GUI.skin = baseGUISkin;
    }

}
