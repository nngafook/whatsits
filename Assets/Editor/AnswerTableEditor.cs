﻿using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;

[CustomEditor(typeof(AnswerTable))]
public class AnswerTableEditor : Editor {

    public override void OnInspectorGUI() {

        if (GUILayout.Button("Refresh Table")) {
            AnswerTable answerTable = (target as AnswerTable);
            string answersString = "";
            TextAsset textAsset = AssetDatabase.LoadAssetAtPath<TextAsset>(answerTable.textFilePath);

            if (textAsset != null) {
                answersString = textAsset.text;
                answerTable.GetAnswersFromFile(answersString);
            }
            else {
                Debug.LogError(("Text Asset at path: " + answerTable.textFilePath + " was empty/null").Colored(Color.red));
            }
        }

        GUILayout.Space(10);
        base.OnInspectorGUI();
    }

}
