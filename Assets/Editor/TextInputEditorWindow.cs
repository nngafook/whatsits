﻿using UnityEngine;
using System.Collections;
using System;
using UnityEditor;

public class TextInputEditorWindow : ScriptableObject {

    private Action<string> OkCallback;
    private Action CancelCallback;

    private Vector2 scrollPosition;

    //private Enum enumToShow;
    private string textValue = "";

    public Rect windowRect;
    public string windowTitle = "Text Input";
    public Vector2 minSize = new Vector2(420, 250);

    public void Init() {
        
    }

    public void SetCallbacks(Action<string> okCallback, Action cancelCallback) {
        OkCallback = okCallback;
        CancelCallback = cancelCallback;
    }

    private void DrawEnumButtons() {
        GUILayout.BeginVertical();
        //int totalColumns = 3;
        //int i = 0;

        //GUILayout.BeginHorizontal();
        //GUILayout.Label("Question", GUILayout.Width((RightColumnWidth / 3) - (Padding * 2)));
        GUILayout.FlexibleSpace();
        GUI.SetNextControlName("InputField");
        textValue = EditorGUILayout.TextField(textValue, GUI.skin.GetStyle("textarea"), GUILayout.Width(380), GUILayout.MinHeight(30));
        //textValue = GUILayout.TextField(textValue, GUILayout.Width(380));
        GUILayout.FlexibleSpace();
        //GUILayout.EndHorizontal();       

        GUI.FocusControl("InputField");

        GUILayout.EndVertical();
    }

    public void DrawWindow() {
        GUILayout.BeginVertical();
        GUILayout.Space(10);

        scrollPosition = GUILayout.BeginScrollView(scrollPosition);
        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();

        DrawEnumButtons();


        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();
        GUILayout.EndScrollView();

        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        //GUILayout.Label(Utility.EnumNameToReadable(characterIDOptions[selectedBtnIndex].ToString()));
        GUILayout.Label("China?");
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();

        GUILayout.Space(5);
        GUILayout.BeginHorizontal();
        GUILayout.FlexibleSpace();
        GUI.color = CustomColor.GetColor(ColorName.ERROR_RED);
        if (GUILayout.Button("Cancel", GUILayout.Width(100))) {
            OkCallback = null;
            if (CancelCallback != null) {
                CancelCallback();
                CancelCallback = null;
            }
        }

        GUI.color = CustomColor.GetColor(ColorName.SUBMIT_GREEN);
        if (GUILayout.Button("OK", GUILayout.Width(100))) {
            //AddIDToList(characterIDOptions[selectedBtnIndex]);
            CancelCallback = null;
            if (OkCallback != null) {
                OkCallback(textValue);
                OkCallback = null;
            }
        }

        Event e = Event.current;
        if (e.isKey && e.keyCode == KeyCode.Return) {
            if (e.type == EventType.KeyUp) {
                CancelCallback = null;
                if (OkCallback != null) {
                    OkCallback(textValue);
                    OkCallback = null;
                }
            }
        }

        GUI.color = Color.white;
        GUILayout.FlexibleSpace();
        GUILayout.EndHorizontal();



        GUILayout.EndVertical();
    }
}
