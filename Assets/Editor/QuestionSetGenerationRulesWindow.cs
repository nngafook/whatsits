﻿using UnityEngine;
using System.Collections;
using UnityEditor;
using System;

public class QuestionSetGenerationRulesWindow : EditorWindow {

    private string generationDataObjectPath = "DataObjects/GenerationRules";
    
    private Action<int> enumWindowCallback = null;
    private int mathSignIndexToChange = -1;
    private int gameRuleIndexToChange = -1;
    private int resetIndexToView = -1;
    private int levelIndexToView = -1;
    private int totalQuestionsAvailable = 0;
    private int selectedLevelDataIndex = -1;
    private int currencyAfterBuyingGeneration = 0;
    // This is the number of questions used to create a set dynamically. This is important for upkeeping the correct total
    // of questions available to make sets when planning generation rules
    private int questionsUsedPerSet = 10;

    private bool ctrlHeld = false;
    private bool duplicateIndex = true;
    private bool inputDisabled = false;

    private Vector2 resetScrollPosition;
    private Vector2 levelScrollPosition;

    private Color darkGray = new Color(0.172549f, 0.172549f, 0.172549f, 1f);

    private EnumSelectWindow enumSelectWindow;

    private QuestionSetResetData selectedResetData;
    private QuestionSetLevelData selectedLevelData;
    private QuestionSetGenerationRules generationRulesSO;
    private Texture2D windowBGTexture;
    private Texture2D menuBGTexture;
    private Texture2D raysTexture;
    private GUISkin baseGUISkin;
    private GUISkin myGUISkin;

    private Vector2 rightColumnScrollPosition;
    private Vector2 menuScrollPosition;

    private float Padding { get { return 10; } }
    private float WindowWidth { get { return position.width; } }
    private float WindowHeight { get { return position.height; } }
    private float ToolbarHeight { get { return 100; } }
    private float ToolbarButtonSize { get { return 120; } }
    private float ToolbarFieldSize { get { return 120; } }
    private float LeftColumnWidth { get { return WindowWidth / 3; } }
    private float MiddleColumnWidth { get { return WindowWidth / 3; } }
    private float RightColumnWidth { get { return WindowWidth / 3; } }

    void Awake() {
        //augmentIDDatabase = AssetDatabase.LoadAssetAtPath<AugmentIDDatabaseDataObject>("Assets/Resources/Data/AugmentIDDatabase.asset");
        this.minSize = new Vector2(1400, 800);
        InitTextures();
        GetGUISkin();
        LoadGenerationObject();
        LoadQuestions();
    }

    void OnEnable() {
        InitTextures();
        GetGUISkin();
        LoadGenerationObject();
        LoadQuestions();
    }

    void OnDestroy() {
        //if (characterSelectionWindow != null) {
        //    DestroyImmediate(characterSelectionWindow);
        //    characterSelectionWindow = null;
        //    inputDisabled = false;
        //}
    }

    private void InitTextures() {
        raysTexture = (Texture2D)AssetDatabase.LoadAssetAtPath("Assets/Art/2D/RaysBackground.png", typeof(Texture2D));

        windowBGTexture = new Texture2D(1, 1, TextureFormat.RGBA32, false);
        windowBGTexture.SetPixel(0, 0, new Color(0.3803922f, 0.6431373f, 0.8666667f, 1f));
        windowBGTexture.Apply();

        menuBGTexture = new Texture2D(1, 1, TextureFormat.RGBA32, false);
        menuBGTexture.SetPixel(0, 0, new Color(0.3333333f, 0.5960785f, 0.9098039f, 1f));
        menuBGTexture.Apply();
    }

    private void LoadGenerationObject() {
        generationRulesSO = Resources.Load<QuestionSetGenerationRules>(generationDataObjectPath);
    }

    private void LoadQuestions() {
        QuestionSetData setData = null;
        string jsonFilesPath = Application.dataPath + "/Resources/Questions/";
        setData = JsonUtility.FromJson<QuestionSetData>(System.IO.File.ReadAllText(jsonFilesPath + "ALLQUESTIONSv1.json"));
        totalQuestionsAvailable = Mathf.CeilToInt(setData.questions.Count / questionsUsedPerSet);
    }

    private void GetGUISkin() {
        myGUISkin = (GUISkin)AssetDatabase.LoadAssetAtPath("Assets/Resources/MyGUISkin.guiskin", typeof(GUISkin));
    }

    #region DRAW_FIELD_METHODS
    private void DrawHorizontalLabeledTextField(string label, ref string value) {
        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(label);
        value = EditorGUILayout.TextField(value);
        GUILayout.EndHorizontal();
    }

    private T DrawHorizontalLabeledEnumField<T>(string label, Enum enumType) {
        T rVal;
        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(label);
        rVal = (T)(object)EditorGUILayout.EnumPopup(enumType);
        GUILayout.EndHorizontal();
        return rVal;
    }

    private void DrawHorizontalLabeledFloatField(string label, ref float value) {
        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(label);
        value = EditorGUILayout.FloatField(value);
        GUILayout.EndHorizontal();
    }

    private void DrawHorizontalLabeledIntField(string label, ref int value) {
        GUILayout.BeginHorizontal();
        EditorGUILayout.LabelField(label);
        value = EditorGUILayout.IntField(value);
        GUILayout.EndHorizontal();
    }
    #endregion DRAW_FIELD_METHODS

    #region WINDOWS
    private void DrawSelectWindow(int id) {
        if (enumSelectWindow != null) {
            enumSelectWindow.DrawWindow();
        }
        GUI.DragWindow();
    }

    private void OpenEnumSelectWindow(Enum e, int selectedValue) {
        if (enumSelectWindow == null) {
            enumSelectWindow = CreateInstance<EnumSelectWindow>();
            enumSelectWindow.Init(e, selectedValue);
            enumSelectWindow.SetCallbacks(OnEnumSelectOKPressed, OnEnumSelectCancelPressed);
            enumSelectWindow.windowRect = new Rect((Screen.width / 2) - (enumSelectWindow.minSize.x / 2), (Screen.height / 2) - (enumSelectWindow.minSize.y / 2), enumSelectWindow.minSize.x, enumSelectWindow.minSize.y);
            inputDisabled = true;
        }
    }

    private void OnEnumSelectOKPressed(int selectedValue) {
        enumSelectWindow = null;
        inputDisabled = false;
        if (enumWindowCallback != null) {
            enumWindowCallback.Invoke(selectedValue);
            enumWindowCallback = null;
        }
    }

    private void OnEnumSelectCancelPressed() {
        enumSelectWindow = null;
        inputDisabled = false;
        gameRuleIndexToChange = -1;
    }

    private void OnMathSignOKSelected(int selectedValue) {
        selectedLevelData.mathSigns[mathSignIndexToChange] = (MathSign)selectedValue;
        mathSignIndexToChange = -1;
    }

    private void OnGameRuleOKSelected(int selectedValue) {
        selectedLevelData.possibleGameRules[gameRuleIndexToChange] = (GameRule)selectedValue;
        gameRuleIndexToChange = -1;
    }
    #endregion WINDOWS

    #region EDITOR_MENU_METHODS
    [MenuItem("Game Tools/Question Set Generation Rules %&g")]
    static void Init() {
        // Get existing open window or if none, make a new one:
        QuestionSetGenerationRulesWindow window = (QuestionSetGenerationRulesWindow)EditorWindow.GetWindow(typeof(QuestionSetGenerationRulesWindow));
        window.Show();
    }
    #endregion EDITOR_MENU_METHODS

    private void DrawToolbar() {
        GUILayout.BeginArea(new Rect(Padding, Padding, WindowWidth - (Padding * 2), ToolbarHeight - (Padding * 2)));

        GUILayout.BeginHorizontal();

        // == Reset Index Label and Int Field == //
        GUILayout.BeginVertical();
        GUILayout.Label("Reset Index", GUILayout.Width(ToolbarFieldSize));
        string tempIndexString = GUILayout.TextField(resetIndexToView.ToString(), GUILayout.Width(ToolbarFieldSize));
        Int32.TryParse(tempIndexString, out resetIndexToView);
        GUILayout.EndVertical();
        // == -- * -- == //

        GUILayout.Space(Padding);

        // == Reset Index View Button == //
        GUILayout.BeginVertical();
        GUILayout.FlexibleSpace();
        GUI.color = CustomColor.GetColor(ColorName.SUBMIT_GREEN);
        if (GUILayout.Button("View", GUILayout.Width(ToolbarButtonSize), GUILayout.Height(ToolbarButtonSize / 2))) {
            if (resetIndexToView >= 0 && generationRulesSO.resetDataList.IsIndexValid(resetIndexToView)) {
                selectedResetData = generationRulesSO.resetDataList[resetIndexToView];
            }
        }
        GUI.color = Color.white;
        GUILayout.FlexibleSpace();
        GUILayout.EndVertical();
        // == -- * -- == //

        GUILayout.Space(Padding);

        // == Level Index Label and Int Field == //
        GUILayout.BeginVertical();
        GUILayout.Label("Level Index", GUILayout.Width(ToolbarFieldSize));
        tempIndexString = GUILayout.TextField(levelIndexToView.ToString(), GUILayout.Width(ToolbarFieldSize));
        Int32.TryParse(tempIndexString, out levelIndexToView);
        GUILayout.EndVertical();
        // == -- * -- == //

        GUILayout.Space(Padding);

        // == Reset Index View Button == //
        GUILayout.BeginVertical();
        GUILayout.FlexibleSpace();
        GUI.color = CustomColor.GetColor(ColorName.SUBMIT_GREEN);
        if (GUILayout.Button("View", GUILayout.Width(ToolbarButtonSize), GUILayout.Height(ToolbarButtonSize / 2))) {
            if (selectedResetData != null && levelIndexToView >= 0 && selectedResetData.levelDatas.IsIndexValid(levelIndexToView)) {
                selectedLevelData = selectedResetData.levelDatas[levelIndexToView];
            }
        }
        GUI.color = Color.white;
        GUILayout.FlexibleSpace();
        GUILayout.EndVertical();
        // == -- * -- == //

        GUILayout.BeginVertical();
        // == Duplicate Index Toggle == //
        duplicateIndex = GUILayout.Toggle(duplicateIndex, "Duplicate Index On Add");
        // == -- * -- == //
        GUI.color = CustomColor.GetColor(ColorName.YALLOW);
        GUILayout.Label("- NOTE: Hold CTRL To Delete Generations/Levels -");
        GUI.color = Color.white;
        GUILayout.EndVertical();
        
        // == Pushes the int fields and buttons to the left == //
        GUILayout.FlexibleSpace();
        // == -- * -- == //

        // == Pushes reset button to the right == //
        GUILayout.FlexibleSpace();
        // == -- * -- == //

        // == Default Difficulty Button == //
        if (selectedResetData != null) {
            GUI.color = CustomColor.GetColor(ColorName.URGENT_URANGE);
            if (GUILayout.Button("Set Default Difficulty", GUILayout.Width(ToolbarButtonSize), GUILayout.Height(ToolbarButtonSize / 2)) && ctrlHeld) {
                QuestionSetLevelData level = null;
                for (int i = 0; i < selectedResetData.levelDatas.Count; i++) {
                    level = selectedResetData.levelDatas[i];

                    level.possibleGameRules.Clear();
                    level.possibleGameRules.Add(GameRule.CorrectAnswerMinus);
                    level.possibleGameRules.Add(GameRule.CorrectAnswerMinusOne);
                    level.possibleGameRules.Add(GameRule.CorrectAnswerPlus);
                    level.possibleGameRules.Add(GameRule.CorrectAnswerPlusOne);
                    level.possibleGameRules.Add(GameRule.ModularWrong);
                    level.possibleGameRules.Add(GameRule.RandomWrong);
                    level.possibleGameRules.Add(GameRule.WrongIfAnswer);

                    level.mathSigns.Clear();
                    level.mathSigns.Add(MathSign.Divide);
                    level.mathSigns.Add(MathSign.Minus);
                    level.mathSigns.Add(MathSign.Multiply);
                    level.mathSigns.Add(MathSign.Plus);

                    level.timePerQuestion = 8;
                    level.levelTime = 60;
                    level.shuffleButtons = true;

                    level.mathValueRange = new RangedFloat(10, 100);
                    level.mathMultiplyRange = new RangedFloat(2, 4);
                    level.mathDivideRange = new RangedFloat(2, 5);
                }
            }
            GUI.color = Color.white;
        }
        // == -- * -- == //

        // == Reset Button == //
        GUI.color = CustomColor.GetColor(ColorName.RACY_RED);
        if (GUILayout.Button("Reset View", GUILayout.Width(ToolbarButtonSize), GUILayout.Height(ToolbarButtonSize / 2))) {
            levelIndexToView = -1;
            resetIndexToView = -1;
        }
        GUI.color = Color.white;
        // == -- * -- == //

        GUILayout.EndHorizontal();

        GUILayout.EndArea();
    }

    private void DrawLeftColumn() {
        GUILayout.BeginArea(new Rect(Padding, ToolbarHeight, LeftColumnWidth - (Padding * 2), WindowHeight - ToolbarHeight));

        GUILayout.BeginVertical();

        // == Add reset data button == //
        if (GUILayout.Button("Add")) {
            if (duplicateIndex) {
                QuestionSetResetData qsrd = new QuestionSetResetData();
                qsrd.Copy(generationRulesSO.resetDataList[generationRulesSO.resetDataList.Count - 1]);
                generationRulesSO.resetDataList.Add(qsrd);
            }
            else {
                generationRulesSO.resetDataList.Add(new QuestionSetResetData());
            }
        }

        resetScrollPosition = GUILayout.BeginScrollView(resetScrollPosition);

        for (int i = 0; i < generationRulesSO.resetDataList.Count; i++) {
            GUILayout.BeginHorizontal();
            GUI.color = (selectedResetData != null && selectedResetData == generationRulesSO.resetDataList[i]) ? CustomColor.GetColor(ColorName.SUBMIT_GREEN) : Color.white;
            if (GUILayout.Button(i.ToString())) {
                selectedResetData = generationRulesSO.resetDataList[i];
                selectedLevelData = null;
            }
            GUI.color = Color.white;

            // == Remove reset data button == //
            GUI.color = CustomColor.GetColor(ColorName.RACY_RED);
            if (GUILayout.Button("-", GUILayout.Width(80)) && (ctrlHeld)) {
                generationRulesSO.resetDataList.RemoveAt(i);
                break;
            }
            GUI.color = Color.white;

            // == Add reset data button == //
            GUI.color = CustomColor.GetColor(ColorName.SUBMIT_GREEN);
            if (GUILayout.Button("+", GUILayout.Width(80))) {
                if (selectedResetData != null) {
                    if (duplicateIndex) {
                        QuestionSetResetData qsr = new QuestionSetResetData();
                        qsr.Copy(generationRulesSO.resetDataList[i]);
                        generationRulesSO.resetDataList.Insert(i + 1, qsr);
                    }
                    else {
                        generationRulesSO.resetDataList.Insert(i + 1, new QuestionSetResetData());
                    }
                    break;
                }
            }
            GUI.color = Color.white;
            GUILayout.EndHorizontal();
        }

        GUILayout.EndScrollView();

        GUILayout.EndVertical();

        GUILayout.EndArea();
    }

    private void DrawMiddleColumn() {
        GUILayout.BeginArea(new Rect(LeftColumnWidth + (Padding / 2), ToolbarHeight, MiddleColumnWidth - Padding, WindowHeight - ToolbarHeight));

        GUILayout.BeginVertical();

        GUILayout.BeginHorizontal();
        // == Add reset data button == //
        GUI.color = CustomColor.GetColor(ColorName.SUBMIT_GREEN);
        if (GUILayout.Button("Add")) {
            if (selectedResetData != null && selectedResetData.levelDatas.Count < totalQuestionsAvailable) {
                if (duplicateIndex) {
                    QuestionSetLevelData sld = new QuestionSetLevelData();
                    sld.Copy(selectedResetData.levelDatas[selectedResetData.levelDatas.Count - 1]);
                    selectedResetData.levelDatas.Add(sld);
                }
                else {
                    selectedResetData.levelDatas.Add(new QuestionSetLevelData());
                }
            }
        }

        // == Add max reset data button == //
        GUI.color = CustomColor.GetColor(ColorName.YALLOW);
        if (GUILayout.Button("Add Max")) {
            if (selectedResetData != null) {
                int difference = totalQuestionsAvailable - selectedResetData.levelDatas.Count;
                if (duplicateIndex) {
                    for (int i = 0; i < difference; i++) {
                        QuestionSetLevelData sld = new QuestionSetLevelData();
                        sld.Copy(selectedResetData.levelDatas[selectedResetData.levelDatas.Count - 1]);
                        selectedResetData.levelDatas.Add(sld);
                    }
                }
                else {
                    for (int i = 0; i < difference; i++) {
                        selectedResetData.levelDatas.Add(new QuestionSetLevelData());
                    }
                }
            }
        }
        GUI.color = Color.white;
        GUILayout.EndHorizontal();

        levelScrollPosition = GUILayout.BeginScrollView(levelScrollPosition);

        if (selectedResetData != null) {
            for (int i = 0; i < selectedResetData.levelDatas.Count; i++) {
                GUILayout.BeginHorizontal();

                GUI.color = (selectedLevelData != null && selectedLevelData == selectedResetData.levelDatas[i]) ? CustomColor.GetColor(ColorName.SUBMIT_GREEN) : Color.white;
                if (GUILayout.Button(i.ToString())) {
                    selectedLevelData = selectedResetData.levelDatas[i];
                    selectedLevelDataIndex = i;
                }
                GUI.color = Color.white;

                // == Remove reset data button == //
                GUI.color = CustomColor.GetColor(ColorName.RACY_RED);
                if (GUILayout.Button("-", GUILayout.Width(80)) && (ctrlHeld)) {
                    selectedResetData.levelDatas.RemoveAt(i);
                    break;
                }
                GUI.color = Color.white;

                // == Add reset data button == //
                GUI.color = CustomColor.GetColor(ColorName.SUBMIT_GREEN);
                if (GUILayout.Button("+", GUILayout.Width(80))) {
                    if (duplicateIndex) {
                        QuestionSetLevelData sld = new QuestionSetLevelData();
                        sld.Copy(selectedResetData.levelDatas[i]);
                        selectedResetData.levelDatas.Insert(i + 1, sld);
                    }
                    else {
                        selectedResetData.levelDatas.Insert(i + 1, new QuestionSetLevelData());
                    }
                    break;
                }
                GUI.color = Color.white;

                GUILayout.EndHorizontal();
            }
        }

        GUILayout.EndScrollView();

        GUILayout.EndVertical();

        GUILayout.EndArea();
    }

    private void DrawRightColumn() {
        GUILayout.BeginArea(new Rect(LeftColumnWidth + MiddleColumnWidth + Padding, ToolbarHeight, RightColumnWidth - (Padding), WindowHeight - ToolbarHeight));

        GUILayout.BeginVertical();
        rightColumnScrollPosition = GUILayout.BeginScrollView(rightColumnScrollPosition);

        if (selectedResetData != null && selectedLevelData == null) {
            // == COST == //
            GUILayout.BeginHorizontal();
            GUILayout.Label("Cost To Unlock", GUILayout.Width((RightColumnWidth / 2) - (Padding * 2)));
            string costString = GUILayout.TextField(selectedResetData.costToUnlock.ToString(), GUILayout.Width((RightColumnWidth / 2) - (Padding / 2)));
            Int32.TryParse(costString, out selectedResetData.costToUnlock);
            GUILayout.EndHorizontal();
        }
        else if (selectedLevelData != null) {

            // SUGGESTED COST AND REWARD == //
            float netProfitFromSetBonusOnly = 0;
            float setBonusTotal = 0;
            float costTotal = 0;
            QuestionSetLevelData levelData;

            GUILayout.BeginHorizontal();
            GUI.color = CustomColor.GetColor(ColorName.URGENT_URANGE);
            GUILayout.Label("Available After Buying Generation", GUILayout.Width((RightColumnWidth / 2) - (Padding * 4)));
            string availableAfterGenerationString = GUILayout.TextField(currencyAfterBuyingGeneration.ToString(), GUILayout.Width((RightColumnWidth / 2) - (Padding / 2)));
            Int32.TryParse(availableAfterGenerationString, out currencyAfterBuyingGeneration);
            GUI.color = Color.white;
            GUILayout.EndHorizontal();

            for (int i = 0; i < selectedLevelDataIndex; i++) {
                levelData = selectedResetData.levelDatas[i];
                setBonusTotal += levelData.setCompleteBonus;
                costTotal += levelData.cost;
            }

            netProfitFromSetBonusOnly = currencyAfterBuyingGeneration + (setBonusTotal - costTotal);

            GUILayout.BeginHorizontal();
            GUI.color = CustomColor.GetColor(ColorName.URGENT_URANGE);
            GUILayout.Label("Set Bonus Net", GUILayout.Width((RightColumnWidth / 2) - (Padding * 4)));
            GUILayout.Label(netProfitFromSetBonusOnly.ToString(), GUILayout.Width((RightColumnWidth / 2) - (Padding * 4)));
            GUI.color = Color.white;
            GUILayout.EndHorizontal();
            

            // == COPY COST AND REWARD FROM PREVIOUS LEVEL == //
            if (selectedLevelDataIndex > 0) {
                if (GUILayout.Button("Copy Cost And Reward")) {
                    selectedLevelData.cost = selectedResetData.levelDatas[selectedLevelDataIndex - 1].cost;
                    selectedLevelData.setCompleteBonus = selectedResetData.levelDatas[selectedLevelDataIndex - 1].setCompleteBonus;
                }
            }

            // == COST == //
            GUILayout.BeginHorizontal();
            GUILayout.Label("Cost", GUILayout.Width((RightColumnWidth / 2) - (Padding * 4)));
            string costString = GUILayout.TextField(selectedLevelData.cost.ToString(), GUILayout.Width((RightColumnWidth / 2) - (Padding / 2)));
            Int32.TryParse(costString, out selectedLevelData.cost);
            GUILayout.EndHorizontal();

            // == Set Complete Bonus Presets (Multiples) == //
            GUILayout.BeginHorizontal();
            GUI.color = (selectedLevelData.cost > 0) ? CustomColor.GetColor(ColorName.YALLOW) : CustomColor.GetColor(ColorName.BUTTON_DISABLED_GRAY);
            if (GUILayout.Button("1.25")) {
                if (selectedLevelData.cost > 0) {
                    selectedLevelData.setCompleteBonus = (int)(selectedLevelData.cost * 1.25f);
                }
            }
            GUI.color = (selectedLevelData.cost > 0) ? CustomColor.GetColor(ColorName.ROB_BLUE) : CustomColor.GetColor(ColorName.BUTTON_DISABLED_GRAY);
            if (GUILayout.Button("1.5")) {
                if (selectedLevelData.cost > 0) {
                    selectedLevelData.setCompleteBonus = (int)(selectedLevelData.cost * 1.5f);
                }
            }
            GUI.color = (selectedLevelData.cost > 0) ? CustomColor.GetColor(ColorName.PURPLE_PLUM) : CustomColor.GetColor(ColorName.BUTTON_DISABLED_GRAY);
            if (GUILayout.Button("1.75")) {
                if (selectedLevelData.cost > 0) {
                    selectedLevelData.setCompleteBonus = (int)(selectedLevelData.cost * 1.75f);
                }
            }
            GUI.color = (selectedLevelData.cost > 0) ? CustomColor.GetColor(ColorName.SUBMIT_GREEN) : CustomColor.GetColor(ColorName.BUTTON_DISABLED_GRAY);
            if (GUILayout.Button("2")) {
                if (selectedLevelData.cost > 0) {
                    selectedLevelData.setCompleteBonus = (int)(selectedLevelData.cost * 2);
                }
            }
            GUI.color = Color.white;
            GUILayout.EndHorizontal();

            // == Set Complete Bonus Presets (Values) == //
            GUILayout.BeginHorizontal();
            GUI.color = (selectedLevelData.cost > 0) ? CustomColor.GetColor(ColorName.YALLOW) : CustomColor.GetColor(ColorName.BUTTON_DISABLED_GRAY);
            if (GUILayout.Button("100")) {
                if (selectedLevelData.cost > 0) {
                    selectedLevelData.setCompleteBonus = (int)(selectedLevelData.cost + 100);
                }
            }
            GUI.color = (selectedLevelData.cost > 0) ? CustomColor.GetColor(ColorName.ROB_BLUE) : CustomColor.GetColor(ColorName.BUTTON_DISABLED_GRAY);
            if (GUILayout.Button("200")) {
                if (selectedLevelData.cost > 0) {
                    selectedLevelData.setCompleteBonus = (int)(selectedLevelData.cost + 200);
                }
            }
            GUI.color = (selectedLevelData.cost > 0) ? CustomColor.GetColor(ColorName.PURPLE_PLUM) : CustomColor.GetColor(ColorName.BUTTON_DISABLED_GRAY);
            if (GUILayout.Button("500")) {
                if (selectedLevelData.cost > 0) {
                    selectedLevelData.setCompleteBonus = (int)(selectedLevelData.cost + 500);
                }
            }
            GUI.color = (selectedLevelData.cost > 0) ? CustomColor.GetColor(ColorName.SUBMIT_GREEN) : CustomColor.GetColor(ColorName.BUTTON_DISABLED_GRAY);
            if (GUILayout.Button("1000")) {
                if (selectedLevelData.cost > 0) {
                    selectedLevelData.setCompleteBonus = (int)(selectedLevelData.cost + 1000);
                }
            }
            GUI.color = Color.white;
            GUILayout.EndHorizontal();

            // == Set Complete Bonus == //
            GUILayout.BeginHorizontal();
            GUILayout.Label("Set Complete Bonus", GUILayout.Width((RightColumnWidth / 2) - (Padding * 4)));
            string setCompleteString = GUILayout.TextField(selectedLevelData.setCompleteBonus.ToString(), GUILayout.Width((RightColumnWidth / 2) - (Padding / 2)));
            Int32.TryParse(setCompleteString, out selectedLevelData.setCompleteBonus);
            GUILayout.EndHorizontal();

            // == Time Per Question == //
            GUILayout.BeginHorizontal();
            GUILayout.Label("Time Per Question", GUILayout.Width((RightColumnWidth / 2) - (Padding * 4)));
            string timeString = GUILayout.TextField(selectedLevelData.timePerQuestion.ToString(), GUILayout.Width((RightColumnWidth / 2) - (Padding / 2)));
            Int32.TryParse(timeString, out selectedLevelData.timePerQuestion);
            GUILayout.EndHorizontal();

            // == Level Time == //
            GUILayout.BeginHorizontal();
            GUILayout.Label("Level Time", GUILayout.Width((RightColumnWidth / 2) - (Padding * 4)));
            string levelTimeString = GUILayout.TextField(selectedLevelData.levelTime.ToString(), GUILayout.Width((RightColumnWidth / 2) - (Padding / 2)));
            Int32.TryParse(levelTimeString, out selectedLevelData.levelTime);
            GUILayout.EndHorizontal();

            // == Shuffle Buttons == //
            GUILayout.BeginHorizontal();
            GUILayout.Label("Shuffle Buttons", GUILayout.Width((RightColumnWidth / 2) - (Padding * 4)));
            selectedLevelData.shuffleButtons = GUILayout.Toggle(selectedLevelData.shuffleButtons, "", GUILayout.Width(30), GUILayout.Height(30));
            GUILayout.EndHorizontal();

            // == Game Rules == //
            GUILayout.BeginHorizontal();
            GUILayout.Label("Game Rules", GUILayout.Width((RightColumnWidth / 2) - (Padding * 4)));

            GUILayout.FlexibleSpace();

            GUI.color = CustomColor.GetColor(ColorName.RACY_RED);
            if (GUILayout.Button("-", GUILayout.Width(80))) {
                if (selectedLevelData.possibleGameRules.Count > 0) {
                    selectedLevelData.possibleGameRules.RemoveAt(selectedLevelData.possibleGameRules.Count - 1);
                }
            }
            GUI.color = Color.white;

            GUI.color = CustomColor.GetColor(ColorName.SUBMIT_GREEN);
            if (GUILayout.Button("+", GUILayout.Width(80))) {
                if (duplicateIndex && selectedLevelData.possibleGameRules.Count > 0) {
                    selectedLevelData.possibleGameRules.Add(selectedLevelData.possibleGameRules[selectedLevelData.possibleGameRules.Count - 1]);
                }
                else {
                    selectedLevelData.possibleGameRules.Add(GameRule.RandomWrong);
                }
            }
            GUI.color = Color.white;

            GUILayout.EndHorizontal();

            for (int i = 0; i < selectedLevelData.possibleGameRules.Count; i++) {
                if (GUILayout.Button(selectedLevelData.possibleGameRules[i].ToString())) {
                    gameRuleIndexToChange = i;
                    OpenEnumSelectWindow(selectedLevelData.possibleGameRules[i], (int)selectedLevelData.possibleGameRules[i]);
                    enumWindowCallback = OnGameRuleOKSelected;
                }
            }

            // == Math Rules == //

            // == Math Sign == //
            GUILayout.BeginHorizontal();
            GUILayout.Label("Math Signs", GUILayout.Width((RightColumnWidth / 2) - (Padding * 4)));

            GUILayout.FlexibleSpace();

            GUI.color = CustomColor.GetColor(ColorName.RACY_RED);
            if (GUILayout.Button("-", GUILayout.Width(80))) {
                if (selectedLevelData.mathSigns.Count > 0) {
                    selectedLevelData.mathSigns.RemoveAt(selectedLevelData.mathSigns.Count - 1);
                }
            }
            GUI.color = Color.white;

            GUI.color = CustomColor.GetColor(ColorName.SUBMIT_GREEN);
            if (GUILayout.Button("+", GUILayout.Width(80))) {
                if (duplicateIndex && selectedLevelData.mathSigns.Count > 0) {
                    selectedLevelData.mathSigns.Add(selectedLevelData.mathSigns[selectedLevelData.mathSigns.Count - 1]);
                }
                else {
                    selectedLevelData.mathSigns.Add(MathSign.Plus);
                }
            }
            GUI.color = Color.white;

            GUILayout.EndHorizontal();

            for (int i = 0; i < selectedLevelData.mathSigns.Count; i++) {
                if (GUILayout.Button(selectedLevelData.mathSigns[i].ToString())) {
                    mathSignIndexToChange = i;
                    OpenEnumSelectWindow(selectedLevelData.mathSigns[i], (int)selectedLevelData.mathSigns[i]);
                    enumWindowCallback = OnMathSignOKSelected;
                }
            }

            // == Math Value == //
            GUILayout.Label("- Math Value -".Colored(CustomColor.GetColor(ColorName.URGENT_URANGE)), GUILayout.Width((RightColumnWidth / 2) - (Padding * 4)));
            
            GUILayout.BeginHorizontal();
            GUILayout.Label("Min", GUILayout.Width((RightColumnWidth / 2) - (Padding * 4)));
            string mathMinString = GUILayout.TextField(selectedLevelData.mathValueRange.min.ToString(), GUILayout.Width((RightColumnWidth / 2) - (Padding / 2)));
            float.TryParse(mathMinString, out selectedLevelData.mathValueRange.min);
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Max", GUILayout.Width((RightColumnWidth / 2) - (Padding * 4)));
            string mathMaxString = GUILayout.TextField(selectedLevelData.mathValueRange.max.ToString(), GUILayout.Width((RightColumnWidth / 2) - (Padding / 2)));
            float.TryParse(mathMaxString, out selectedLevelData.mathValueRange.max);
            GUILayout.EndHorizontal();

            // == Math Multiply == //
            GUILayout.Label("- Math Multiply -".Colored(CustomColor.GetColor(ColorName.URGENT_URANGE)), GUILayout.Width((RightColumnWidth / 2) - (Padding * 4)));

            GUILayout.BeginHorizontal();
            GUILayout.Label("Min", GUILayout.Width((RightColumnWidth / 2) - (Padding * 4)));
            string multiplyMinString = GUILayout.TextField(selectedLevelData.mathMultiplyRange.min.ToString(), GUILayout.Width((RightColumnWidth / 2) - (Padding / 2)));
            float.TryParse(multiplyMinString, out selectedLevelData.mathMultiplyRange.min);
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Max", GUILayout.Width((RightColumnWidth / 2) - (Padding * 4)));
            string multiplyMaxString = GUILayout.TextField(selectedLevelData.mathMultiplyRange.max.ToString(), GUILayout.Width((RightColumnWidth / 2) - (Padding / 2)));
            float.TryParse(multiplyMaxString, out selectedLevelData.mathMultiplyRange.max);
            GUILayout.EndHorizontal();

            // == Math Multiply == //
            GUILayout.Label("- Math Divide -".Colored(CustomColor.GetColor(ColorName.URGENT_URANGE)), GUILayout.Width((RightColumnWidth / 2) - (Padding * 4)));

            GUILayout.BeginHorizontal();
            GUILayout.Label("Min", GUILayout.Width((RightColumnWidth / 2) - (Padding * 4)));
            string divideMinString = GUILayout.TextField(selectedLevelData.mathDivideRange.min.ToString(), GUILayout.Width((RightColumnWidth / 2) - (Padding / 2)));
            float.TryParse(divideMinString, out selectedLevelData.mathDivideRange.min);
            GUILayout.EndHorizontal();

            GUILayout.BeginHorizontal();
            GUILayout.Label("Max", GUILayout.Width((RightColumnWidth / 2) - (Padding * 4)));
            string divideMaxString = GUILayout.TextField(selectedLevelData.mathDivideRange.max.ToString(), GUILayout.Width((RightColumnWidth / 2) - (Padding / 2)));
            float.TryParse(divideMaxString, out selectedLevelData.mathDivideRange.max);
            GUILayout.EndHorizontal();

        }

        GUILayout.EndScrollView();
        GUILayout.EndVertical();

        GUILayout.EndArea();
    }

    void OnGUI() {
        if (menuBGTexture == null || windowBGTexture == null) {
            InitTextures();
        }

        baseGUISkin = GUI.skin;
        GUI.skin = myGUISkin;

        GUI.color = darkGray;
        GUI.DrawTexture(new Rect(0, 0, maxSize.x - 4, maxSize.y - 4), raysTexture, ScaleMode.ScaleAndCrop);
        GUI.color = Color.white;

        //GUI.DrawTexture(new Rect(0, 0, maxSize.x - 4, maxSize.y - 4), windowBGTexture, ScaleMode.StretchToFill);

        EditorGUI.BeginDisabledGroup(inputDisabled);

        Event e = Event.current;
        if (e.control) {
            ctrlHeld = true;
        }
        else {
            ctrlHeld = false;
        }

        GUILayout.BeginVertical();
        DrawToolbar();

        GUILayout.BeginHorizontal();

        DrawLeftColumn();
        DrawMiddleColumn();
        DrawRightColumn();

        GUILayout.EndHorizontal();

        GUILayout.EndVertical();

        EditorGUI.EndDisabledGroup();

        BeginWindows();
        if (enumSelectWindow != null) {
            enumSelectWindow.windowRect = GUILayout.Window(0, enumSelectWindow.windowRect, DrawSelectWindow, enumSelectWindow.windowTitle);
        }
        EndWindows();

        GUI.skin = baseGUISkin;

        if (GUI.changed) {
            if (generationRulesSO != null) {
                EditorUtility.SetDirty(generationRulesSO);
            }
        }
    }

}
