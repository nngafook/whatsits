﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Text;

public class RuleSet : ScriptableObject {

    public RuleSetData data;

}

[Serializable]
public class RuleSetData {

    public GameRule gameRule;

    // Stores the INDICES of the questions affected by the rule
    public List<int> affectedQuestionIndices = new List<int>();

    // Miscellaneous usage. (Eg. number of choices below or above to use for CorrectAnswerPlus/Minus)
    public int xValue = 0;

    // RandomWrong,    // Random questions must be answered wrong (quantity can vary?)
    public int totalToGetWrong = 0;

    // ModularWrong,   // Question number MOD (%) 1 or 2 must be answered wrong
    public int modularValueToGetWrong = 0;

    // WrongIfAnswer,  // Answer question wrong if the correct multiple choice answer is x
    public MultipleChoice wrongIfAnswerIs;

    // ================================ //
    // Needs to be rethunk. If the variable is A, the player can just spam A and never get a question wrong
    // CorrectIfAnswer,    // Answer question correct only if the correct multiple choice answer is x
    public MultipleChoice correctIfAnswerIs;
    // ================================ //

    // CorrectAnswerPlus, // Must select the option one above the correct answer (wraps, so D == A)
    //public MultipleChoice correctAnswerPlusOne;

    // CorrectAnswerMinus  // Must select the option one below the correct answer (wraps, so A == D)
    //public MultipleChoice corrrectAnswerMinusOne;

    // CorrectAnswerPlus, // Must select the option x above the correct answer (wraps, so D == A)
    //public MultipleChoice correctAnswerPlus;

    // CorrectAnswerMinus  // Must select the option x below the correct answer (wraps, so A == D)
    //public MultipleChoice corrrectAnswerMinus;

    public string RuleDescription {
        get {
            string rVal = "";

            switch (gameRule) {
                case GameRule.RandomWrong:
                    StringBuilder sb = new StringBuilder();
                    if (totalToGetWrong > 1) {
                        sb.Append("Answer Questions ");
                    }
                    else {
                        sb.Append("Answer Question ");
                    }

                    for (int i = 0; i < totalToGetWrong; i++) {
                        if (i == 0) {
                            sb.Append("{" + i.ToString() + "}");
                        }
                        else if (i == (totalToGetWrong - 1)) {
                            sb.Append(" and {" + i.ToString() + "}");
                        }
                        else {
                            sb.Append(", {" + i.ToString() + "}");
                        }
                    }

                    sb.Append(" WRONG!".Colored(CustomColor.GetColor(ColorName.ERROR_RED)));
                    rVal = sb.ToString();
                    break;
                case GameRule.ModularWrong:
                    rVal = "Answer all Questions that are a factor of " + modularValueToGetWrong.ToString() + (" WRONG!").Colored(CustomColor.GetColor(ColorName.ERROR_RED));
                    break;
                case GameRule.WrongIfAnswer:
                    rVal = "If the correct answer is " + wrongIfAnswerIs.ToString() + ", answer the question " + ("WRONG!").Colored(CustomColor.GetColor(ColorName.ERROR_RED));
                    break;
                //case GameRule.CorrectIfAnswer:
                //    rVal = "Answer the question correct, " + ("ONLY").Colored(CustomColor.GetColor(ColorName.ERROR_RED)) + " if the correct answer is " + correctIfAnswerIs.ToString();
                //    break;
                case GameRule.CorrectAnswerPlusOne:
                    rVal = "Answer the question using the option " + ("AFTER").Colored(CustomColor.GetColor(ColorName.ERROR_RED)) + " the correct answer";
                    break;
                case GameRule.CorrectAnswerMinusOne:
                    rVal = "Answer the question using the option " + ("BEFORE").Colored(CustomColor.GetColor(ColorName.ERROR_RED)) + " the correct answer";
                    break;
                case GameRule.CorrectAnswerPlus:
                    rVal = "Answer the question using the option " + (xValue.ToString()).Colored(CustomColor.GetColor(ColorName.SKIER_BLUE)) + ((xValue == 1) ? " choice " : " choices ") + ("AFTER").Colored(CustomColor.GetColor(ColorName.ERROR_RED)) + " the correct answer";
                    break;
                case GameRule.CorrectAnswerMinus:
                    rVal = "Answer the question using the option " + (xValue.ToString()).Colored(CustomColor.GetColor(ColorName.SKIER_BLUE)) + ((xValue == 1) ? " choice " : " choices ") + ("BEFORE").Colored(CustomColor.GetColor(ColorName.ERROR_RED)) + " the correct answer";
                    break;
                default:
                    break;
            }

            return rVal;
        }
    }

}