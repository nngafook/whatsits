﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[CreateAssetMenu(menuName = "DataObjects/QuestionSet")]
public class QuestionSet : ScriptableObject {

    public QuestionSetData data = new QuestionSetData();

    public List<Question> Questions { get { return data.questions; } }

    public Question RandomQuestion() {
        return data.questions.RandomElement<Question>();
    }

    public bool HasQuestionID(string uid) {
        bool rVal = false;

        for (int i = 0; i < Questions.Count; i++) {
            if (Questions[i].uid == uid) {
                rVal = true;
                break;
            }
        }

        return rVal;
    }

}

[Serializable]
public class QuestionSetData {
    public string name = "";
    public string id = "";
    public int cost = 0;
    public int levelTime = 60;
    public int timePerQuestion = 816;
    public int setCompleteBonus = 1000;
    public bool shuffleButtons = false;
    [Tooltip("The number of the set in the game's progression, used in the Lobby list.")]
    public int gameProgressionRank = -1;
    public QuestionCategory category = QuestionCategory.Uncategorized;
    public List<Question> questions = new List<Question>();
    public List<GameRule> possibleGameRules = new List<GameRule>();

    public void Copy(QuestionSetData from) {
        this.name = from.name;
        this.id = from.id;
        this.cost = from.cost;
        this.gameProgressionRank = from.gameProgressionRank;
        this.category = from.category;
        this.questions = new List<Question>(from.questions);
    }

    public void GenerateID() {
        id = Utility.GenerateUID();
    }

    public bool ValidateQuestions() {
        bool rVal = true;

        if (questions.IsNullOrEmpty()) {
            rVal = false;
            Debug.LogError("ValidateQuestions failed in QuestionSetData. Questions List was either null or empty");
        }
        else {
            for (int i = 0; i < questions.Count; i++) {
                if (rVal) {
                    rVal = questions[i].Validate();
                }
            }
        }

        return rVal;
    }
}