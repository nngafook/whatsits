﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

[CreateAssetMenu(menuName = "DataObjects/QuestionSetGenerationRules")]
public class QuestionSetGenerationRules : ScriptableObject {

    // Each index is the reset count. i.e. [0] is the first generation. [1] is after the first reset
    public List<QuestionSetResetData> resetDataList = new List<QuestionSetResetData>();

    public QuestionSetResetData FirstLockedResetData() {
        QuestionSetResetData rVal = resetDataList[0];
        for (int i = 0; i < resetDataList.Count; i++) {
            if (resetDataList[i].costToUnlock > 0) {
                rVal = resetDataList[i];
            }
        }
        return rVal;
    }

    public QuestionSetLevelData FirstQuestionSetLevelData() {
        QuestionSetLevelData rVal = resetDataList[0].levelDatas[0];
        for (int i = 0; i < resetDataList[0].levelDatas.Count; i++) {
            if (resetDataList[0].levelDatas[i].cost > 0) {
                rVal = resetDataList[0].levelDatas[i];
            }
        }
        return rVal;
    }

    public QuestionSetResetData GetResetData(int index) {
        index = Mathf.Clamp(index, 0, resetDataList.Count - 1);
        return resetDataList[index];
    }

    public GameRule RandomGameRule(int resetCount, int level) {
        GameRule rVal = GameRule.RandomWrong;
        if (resetDataList[resetCount].levelDatas[level].possibleGameRules.Count > 0) {
            rVal = resetDataList[resetCount].levelDatas[level].possibleGameRules.RandomElement<GameRule>();
        }
        return rVal;
    }

}

/// <summary>
/// Contains a list of level generation rules. Each index is the reset number
/// i.e. [0] == first generation rules. [1] == first reset generation rules
/// </summary>
[Serializable]
public class QuestionSetResetData {
    public int costToUnlock = 0;
    public List<QuestionSetLevelData> levelDatas = new List<QuestionSetLevelData>();

    public QuestionSetLevelData GetLevelData(int index) {
        index = Mathf.Clamp(index, 0, levelDatas.Count - 1);
        return levelDatas[index];
    }

    public void Copy(QuestionSetResetData from) {
        levelDatas = new List<QuestionSetLevelData>(from.levelDatas);
    }

}

[Serializable]
public class QuestionSetLevelData {
    public int cost = 0;
    public int levelTime = 60;
    public int timePerQuestion = 10;
    public int setCompleteBonus = 1000;
    public bool shuffleButtons = false;
    public List<GameRule> possibleGameRules = new List<GameRule>();

    public List<MathSign> mathSigns = new List<MathSign>();
    public RangedFloat mathValueRange = new RangedFloat(1, 100);
    public RangedFloat mathMultiplyRange = new RangedFloat(2, 4);
    public RangedFloat mathDivideRange = new RangedFloat(2, 4);

    public void Copy(QuestionSetLevelData from) {
        cost = from.cost;
        possibleGameRules = new List<GameRule>(from.possibleGameRules);
    }
}

[Serializable]
public class QuestionSetHashInfo {
    // The number of times the player has reset the game (0 == fresh)
    public int resetVersion = 0;
}