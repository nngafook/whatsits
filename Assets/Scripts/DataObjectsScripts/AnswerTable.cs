﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu(menuName = "DataObjects/AnswerTable")]
public class AnswerTable : ScriptableObject {

    public AnswerCategory answerCategory;

    public List<string> answers;

    [Header("Text File")]
    public string textFilePath = "";

    public void GetAnswersFromFile(string answersString) {
        string[] strings = answersString.Split(',');

        answers = new List<string>();
        for (int i = 0; i < strings.Length; i++) {
            answers.Add(strings[i]);
        }

    }

    public string RandomAnswer() {
        return answers.RandomElement<string>();
    }

    public string RandomUniqueAnswer(ref List<string> usedStrings) {
        string rVal = RandomAnswer();

        for (int i = 0; i < usedStrings.Count; i++) {
            if (rVal.ToUpper() == usedStrings[i].ToUpper()) {
                rVal = RandomAnswer();
                break;
            }
        }

        //while (usedStrings.Contains(rVal)) {
        //    rVal = RandomAnswer();
        //}

        return rVal;
    }

}
