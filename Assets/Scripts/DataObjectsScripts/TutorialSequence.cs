﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu(menuName = "DataObjects/TutorialSequence")]
public class TutorialSequence : ScriptableObject {

    public string sequenceName = "";
    public string question = "";
    public MultipleChoice correctAnswer = MultipleChoice.A;
    public List<string> answerStrings = new List<string>();
    public List<TutorialTarget> targets = new List<TutorialTarget>();

}