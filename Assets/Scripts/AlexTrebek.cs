﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using TextFx;
using DG.Tweening;
using UnityEngine.SceneManagement;
using System;
using Random = UnityEngine.Random;
using Opencoding.CommandHandlerSystem;

public class AlexTrebek : MonoBehaviour {

    private WaitForSeconds oneSecondWait = new WaitForSeconds(1);

    private GameRule currentGameRule;

    private MultipleChoice correctButtonChoice;

    public string recapCyclePrefsString = "UsedRecapCycleButtons";
    public string recapUsedPrefsString = "UsedRecap";

    private float questionTimeRemainingScoreFactor = 0.1f;
    private int pointsPerUniqueQuestion = 10;
    private int pointsPerSeenQuestion = 2;
    private int accumulatedScoreWithoutBonus = 0;
    //private int accumulatedBonusPoints = 0;       // Total, after multiplied by questionTimeRemainingScoreFactor

    private float recapScaleSpeed = 0.25f;

    private int indexBeingRecapped = -1;
    private int totalQuestions = 20;

    private int questionsAsked = 0;
    private int uniqueAnswersCorrect = 0;
    private int seenAnswersCorrect = 0;

    private bool usedRecapCycleButtons = false;

    private Vector2 recapScaleFrom = new Vector2(2f, 2f);

    private RuleSetData ruleSetData;

    private List<int> questionIndexesToAnswerWrong;

    private List<AnswerSheetEntry> answerSheetEntries;
    private SetProgression setProgression;
    private QuestionSet loadedQuestionSet;

    // This stores the questions AFTER they are shown. It is used to show the recap at the end of the level.
    // I know I could just refactor the queue to not be a queue, but Trump is president, so I don't know how much time we have left.
    private List<QuestionRecapData> questionRecapList;
    private Queue<Question> questionsQueue;

    public bool forceUnansweredQuestions = false;

    [Space(10)]
    public QuestionSet debugQuestionSet;

    [Space(10)]
    [Header("Tables")]
    public AnswerTable adjectivesAnswerTable;
    public AnswerTable nounsAnswerTable;
    public AnswerTable colorsAnswerTable;
    public AnswerTable namesAnswerTable;
    public AnswerTable moviesAnswerTable;
    public AnswerTable sportsAnswerTable;
    public AnswerTable actorsAnswerTable;
    public AnswerTable animalsAnswerTable;
    public AnswerTable usStatesAnswerTable;
    public AnswerTable usCitiesAnswerTable;
    public AnswerTable countriesAnswerTable;
    public AnswerTable verbsAnswerTable;

    [Space(10)]
    public TimeManager timeManager;
    public CanvasGroup answerButtonsCG;
    public LevelStartWindow levelStartWindow;
    public LevelSummaryWindow levelSummaryWindow;
    public TutorialStage tutorialStage;
    public GameObject pauseButton;
    public GameObject levelTimerLabel;
    public GameObject questionTimerLabel;

    [Space(10)]
    public TextFxUGUI questionTextFX;
    public Text questionInstructionsLabel;
    public PointsNotification pointsNotification;
    public List<AnswerButton> answerButtons;

    [Header("Recap Icons")]
    public RectTransform choiceMadeIcon;
    public RectTransform answeredWrongIcon;
    public RectTransform answeredRightIcon;
    public GameObject recapButtonsParent;
    public GameObject recapCycleMessage;
    public GameObject recapButtonPrompt;
    public List<RectTransform> answerRecapIcons;

    [Header("Cheat Things")]
    public Text cheatQuestionsAskedLabel;
    public Text cheatRuleLabel;

    void Awake() {
        #if UNITY_EDITOR
            if (forceUnansweredQuestions) {
                Debug.Log("forceUnansweredQuestions is set to TRUE. Toggle this before publishing!".Bold().Colored(Color.red).Sized(12));
            }
        #endif
        MusicManager.Instance.PlayLobbyMusic();

        answerButtonsCG.interactable = false;
        HideQuestionAndCategory();
        AddButtonEvents();

        timeManager.OnLevelTimeUpEvent.AddListener(OnLevelTimeUp);
        timeManager.OnQuestionTimeUpEvent.AddListener(OnQuestionTimeUp);

        CommandHandlers.RegisterCommandHandlers(this);
    }

    public void OnDestroy() {
        CommandHandlers.UnregisterCommandHandlers(this);
    }

    // Use this for initialization
    void Start() {
        #if UNITY_EDITOR
        if (Lazarus.Instance.QuestionSetInPlay == null) {
            loadedQuestionSet = debugQuestionSet;
            Lazarus.Instance.QuestionSetInPlay = loadedQuestionSet;
        }
        #endif

        pointsPerUniqueQuestion = Lazarus.Instance.uniqueQuestionReward;
        pointsPerSeenQuestion = Lazarus.Instance.seenQuestionReward;

        loadedQuestionSet = Lazarus.Instance.QuestionSetInPlay;
        timeManager.SetTimes(ref loadedQuestionSet);

        questionsQueue = new Queue<Question>(Utility.ShuffleList<Question>(loadedQuestionSet.Questions, Random.Range(0, 10000)));
        questionRecapList = new List<QuestionRecapData>();

        #if UNITY_EDITOR
            // == THIS SHOULD REALLY BE GONE BEFORE PUBLISHING == //
            if (forceUnansweredQuestions) {
                List<Question> reordererdList = new List<Question>();
                List<Question> lowPrioList = new List<Question>();
                for (int i = 0; i < loadedQuestionSet.Questions.Count; i++) {
                    if (QuestionSetProgression.Instance.HasSeenQuestion(loadedQuestionSet.data.id, loadedQuestionSet.Questions[i].uid)) {
                        lowPrioList.Add(loadedQuestionSet.Questions[i]);
                    }
                    else {
                        reordererdList.Add(loadedQuestionSet.Questions[i]);
                    }
                }
                for (int i = 0; i < lowPrioList.Count; i++) {
                    reordererdList.Add(lowPrioList[i]);
                }
                questionsQueue = new Queue<Question>(reordererdList);
            }
        #endif

        setProgression = new SetProgression();
        setProgression.questionSetID = loadedQuestionSet.data.id;
        
        answerSheetEntries = new List<AnswerSheetEntry>();

        SetAnswerButtonsVisible(false);

        for (int i = 0; i < answerButtons.Count; i++) {
            answerButtons[i].SetChoiceValue(i);
            answerButtons[i].SetLabel("");
        }

        string rules = SetRules();
        cheatRuleLabel.text = rules;
        levelStartWindow.Open(rules, ruleSetData);

        if (PlayerPrefs.GetInt("SeenWelcomeTutorial", 0) == 0) {
            tutorialStage.OpenWelcomeTutorial();
        }
        else {
            Debug.Log(PlayerPrefs.GetInt("SeenWelcomeTutorial"));
        }
    }

    private string SetRules() {
        //currentGameRule = (GameRule)Random.Range(0, Enum.GetNames(typeof(GameRule)).Length);
        int randomIndex = Random.Range(0, loadedQuestionSet.data.possibleGameRules.Count);
        currentGameRule = loadedQuestionSet.data.possibleGameRules[randomIndex];
        //currentGameRule = GameRule.CorrectAnswerMinus;

        ruleSetData = new RuleSetData();
        ruleSetData.gameRule = currentGameRule;
        ruleSetData.totalToGetWrong = Random.Range(1, 5);
        ruleSetData.modularValueToGetWrong = Random.Range(2, 5);
        ruleSetData.wrongIfAnswerIs = (MultipleChoice)Random.Range(0, 4);
        ruleSetData.correctIfAnswerIs = (MultipleChoice)Random.Range(0, 4);     // This is still shit, cause the player can spam the option and always be right...
        ruleSetData.xValue = Random.Range(1, 4);

        string rules = ruleSetData.RuleDescription;

        questionIndexesToAnswerWrong = new List<int>();
        
        int index = 0;
        bool skip = false;

        switch (currentGameRule) {
            case GameRule.RandomWrong:
                while (questionIndexesToAnswerWrong.Count != ruleSetData.totalToGetWrong) {
                    skip = false;
                    index = Random.Range(0, loadedQuestionSet.Questions.Count);

                    for (int i = 0; i < questionIndexesToAnswerWrong.Count; i++) {
                        if (index == questionIndexesToAnswerWrong[i]) {
                            skip = true;
                            break;
                        }
                    }

                    if (!skip) {
                        questionIndexesToAnswerWrong.Add(index);
                        Debug.Log("Index to answer wrong Added: " + index);
                    }
                }

                questionIndexesToAnswerWrong.Sort((indexOne, indexTwo) => indexOne.CompareTo(indexTwo));

                string[] stringArgs = new string[ruleSetData.totalToGetWrong];
                for (int i = 0; i < questionIndexesToAnswerWrong.Count; i++) {
                    stringArgs[i] = (questionIndexesToAnswerWrong[i] + 1).ToString();
                }

                rules = string.Format(rules, stringArgs);
                break;
            case GameRule.ModularWrong:
                for (int i = 1; i <= totalQuestions; i++) {
                    if (i % ruleSetData.modularValueToGetWrong == 0) {
                        questionIndexesToAnswerWrong.Add(i - 1);
                        Debug.Log("Index to answer wrong Added: " + i);
                    }
                }
                break;
            case GameRule.WrongIfAnswer:
            //case GameRule.CorrectIfAnswer:
            case GameRule.CorrectAnswerPlusOne:
            case GameRule.CorrectAnswerMinusOne:
            case GameRule.CorrectAnswerPlus:
            case GameRule.CorrectAnswerMinus:
            default:
                // Do nothing
                break;
        }

        ruleSetData.affectedQuestionIndices = new List<int>(questionIndexesToAnswerWrong);

        return rules;
    }

    private void AddButtonEvents() {
        for (int i = 0; i < answerButtons.Count; i++) {
            answerButtons[i].OnPressedEvent.AddListener(OnAnswerMade);
        }
    }

    private void OnLevelTimeUp() {
        timeManager.OnLevelTimeUpEvent.RemoveListener(OnLevelTimeUp);
        timeManager.OnQuestionTimeUpEvent.RemoveListener(OnQuestionTimeUp);
        timeManager.StopQuestionTimer();
        StopCoroutine("QuestionAskedGracePeriod");
        setProgression.questionIDsSeen.RemoveAt(setProgression.questionIDsSeen.Count - 1);
        EndLevel();
    }

    private void OnQuestionTimeUp() {
        OnAnswerMade(MultipleChoice.NONE);
    }

    private void AddButtonHideEvent() {
        answerButtons[answerButtons.Count - 1].OnHideComplete.AddListener(OnButtonsHideComplete);
    }

    private void OnButtonsHideComplete() {
        answerButtons[answerButtons.Count - 1].OnHideComplete.RemoveListener(OnButtonsHideComplete);
        NewQuestion();
    }

    private void ShowLevelSummary() {
        SFXManager.Instance.PlaySFX(SFX_TYPE.Clapping);
        //int pointsEarned = accumulatedScoreWithoutBonus + accumulatedBonusPoints;

        if (!usedRecapCycleButtons) {
            levelSummaryWindow.OnReturningToLobby.AddListener(OnReturningToLobby);
        }

        recapButtonPrompt.SetActive(!PlayerPrefs.GetInt(recapUsedPrefsString, 0).AsBool());

        Lazarus.Instance.UpdatePlayerDetails(accumulatedScoreWithoutBonus, PlayerDetail.Currency);
        levelSummaryWindow.Open(questionsAsked, uniqueAnswersCorrect, seenAnswersCorrect, pointsPerUniqueQuestion, pointsPerSeenQuestion, accumulatedScoreWithoutBonus);
        questionTextFX.gameObject.SetActive(false);
    }

    private void OnReturningToLobby() {
        levelSummaryWindow.OnReturningToLobby.RemoveAllListeners();
        PlayerPrefs.SetInt(recapCyclePrefsString, usedRecapCycleButtons.AsInt());
    }

    private void HideRecapIcons() {
        for (int i = 0; i < answerRecapIcons.Count; i++) {
            answerRecapIcons[i].CG().alpha = 0;
        }
        answeredWrongIcon.CG().alpha = 0;
        answeredRightIcon.CG().alpha = 0;
        choiceMadeIcon.CG().alpha = 0;
    }

    private void HideAnswerLabels() {
        for (int i = 0; i < answerButtons.Count; i++) {
            answerButtons[i].AnimateHide();
        }
    }

    private void HideQuestionAndCategory() {
        questionTextFX.AnimationManager.ResetAnimation();
        questionInstructionsLabel.text = "";
        questionInstructionsLabel.CG().alpha = 0;
        questionInstructionsLabel.RT().localScale = new Vector3(1.5f, 1.5f, 1.5f);
    }

    private void ShowAnswerLabels() {
        for (int i = 0; i < answerButtons.Count; i++) {
            //answerButtons[i].RT().DOScale(1, 0.15f).SetEase(Ease.OutBack);
            answerButtons[i].AnimateShow();
        }
    }

    private void SetAnswerButtonsVisible(bool value) {
        for (int i = 0; i < answerButtons.Count; i++) {
            if (value) {
                answerButtons[i].SetShow();
            }
            else {
                answerButtons[i].SetHide();
            }
        }
    }

    private void ResetButtonOrder() {
        for (int i = 0; i < answerButtons.Count; i++) {
            answerButtons[i].SetChoiceValue(i);
        }
    }

    private void ShuffleButtonOrder() {
        List<int> shuffledIndices = new List<int>() { 0, 1, 2, 3 };
        Queue<int> indexQueue = new Queue<int>(Utility.ShuffleList<int>(shuffledIndices, Random.Range(0, 500)));

        for (int i = 0; i < answerButtons.Count; i++) {
            answerButtons[i].SetChoiceValue(indexQueue.Dequeue());
        }
    }

    private void NewQuestion() {
        if (questionsAsked < totalQuestions && questionsQueue.Count > 0) {

            StartCoroutine("QuestionAskedGracePeriod");

            // Randomly shuffle buttons to test things
            if (loadedQuestionSet.data.shuffleButtons && Random.Range(0, 2) == 0) {
                ShuffleButtonOrder();
            }
            else {
                ResetButtonOrder();
            }

            cheatQuestionsAskedLabel.text = "Question <color=#75C33FFF>#" + (questionsAsked + 1).ToString() + "</color>";

            //Question q = debugQuestionSet.RandomQuestion();
            //Question q = debugQuestionSet.Questions[debugQuestionSet.Questions.Count - 1];
            //Question q = debugQuestionSet.Questions[17];
            //Question q = (Random.Range(0, 2) == 0) ? debugQuestionSet.Questions[28] : debugQuestionSet.Questions[29];
            Question q = questionsQueue.Dequeue();
            int totalChoices = 4;
            int indexForRealAnswer = Random.Range(0, totalChoices);

            if (q.questionType == QuestionType.Label) {
                answerSheetEntries.Add(new AnswerSheetEntry(q.uid, q.QuestionText, q.correctLabelAnswer));
            }
            setProgression.questionIDsSeen.Add(q.uid);

            List<string> shuffledLabels = Utility.ShuffleList<string>(q.buttonLabelOptions, Random.Range(0, 100000));

            // Check if the shuffledLabels contains the real answer, if not, put it in
            if (!shuffledLabels.Contains(q.correctLabelAnswer)) {
                shuffledLabels.Add(q.correctLabelAnswer);
            }

            // Clear out any empty labels (sanity check)
            for (int i = 0; i < shuffledLabels.Count; i++) {
                if (string.IsNullOrEmpty(shuffledLabels[i])) {
                    shuffledLabels.RemoveAt(i);
                    i = 0;
                }
            }

            // If less than 4 prefered answers were supplied
            if (shuffledLabels.Count < totalChoices) {
                string btnText = "";
                AnswerCategory answerCat = AnswerCategory.Uncategorized;
                for (int i = shuffledLabels.Count; i < totalChoices; i++) {
                    // MAKE SURE THAT THE RANDOM ANSWERS DO NOT MEAN THE SAME THING AS THE REAL ANSWER... THESAURUS!!!

                    answerCat = q.answerCategories.RandomElement<AnswerCategory>();

                    switch (answerCat) {
                        case AnswerCategory.Adjectives:
                            btnText = adjectivesAnswerTable.RandomUniqueAnswer(ref shuffledLabels);
                            break;
                        case AnswerCategory.Nouns:
                            btnText = nounsAnswerTable.RandomUniqueAnswer(ref shuffledLabels);
                            break;
                        case AnswerCategory.ButtonColors:
                            btnText = colorsAnswerTable.RandomUniqueAnswer(ref shuffledLabels);
                            break;
                        case AnswerCategory.Names:
                            btnText = namesAnswerTable.RandomUniqueAnswer(ref shuffledLabels);
                            break;
                        case AnswerCategory.Actors:
                            btnText = actorsAnswerTable.RandomUniqueAnswer(ref shuffledLabels);
                            break;
                        case AnswerCategory.Sports:
                            btnText = sportsAnswerTable.RandomUniqueAnswer(ref shuffledLabels);
                            break;
                        case AnswerCategory.Movies:
                            btnText = moviesAnswerTable.RandomUniqueAnswer(ref shuffledLabels);
                            break;
                        case AnswerCategory.Animals:
                            btnText = animalsAnswerTable.RandomUniqueAnswer(ref shuffledLabels);
                            break;
                        case AnswerCategory.USStates:
                            btnText = usStatesAnswerTable.RandomUniqueAnswer(ref shuffledLabels);
                            break;
                        case AnswerCategory.USCities:
                            btnText = usCitiesAnswerTable.RandomUniqueAnswer(ref shuffledLabels);
                            break;
                        case AnswerCategory.Countries:
                            btnText = countriesAnswerTable.RandomUniqueAnswer(ref shuffledLabels);
                            break;
                        case AnswerCategory.Verbs:
                            btnText = verbsAnswerTable.RandomUniqueAnswer(ref shuffledLabels);
                            break;
                        case AnswerCategory.Uncategorized:
                        default:
                            Debug.Log("Question's Answer Category does not have an answer table... Defaulting to Nouns".Bold());
                            btnText = nounsAnswerTable.RandomUniqueAnswer(ref shuffledLabels);
                            break;
                    }

                    //switch (q.questionType) {       // I think this needs to be changed to AnswerCategory and not QuestionType... good luck with the refactor
                    //    case QuestionType.Label:
                    //        btnText = adjectivesAnswerTable.RandomUniqueAnswer(ref shuffledLabels);
                    //        break;
                    //    case QuestionType.ButtonColor:
                    //        btnText = colorsAnswerTable.RandomUniqueAnswer(ref shuffledLabels);
                    //        break;
                    //    case QuestionType.NounSpot:
                    //        // Should only get in here if the nouns answer table was supplied as the Answer Category,
                    //        // But ideally, prefered options are recommended cause then it's too easy.
                    //        btnText = nounsAnswerTable.RandomUniqueAnswer(ref shuffledLabels);
                    //        break;
                    //    case QuestionType.Uncategorized:
                    //        break;
                    //}
                    shuffledLabels.Add(btnText);
                }

                shuffledLabels = Utility.ShuffleList<string>(shuffledLabels, Random.Range(0, 100000));
            }

            // This should always get in, cause of the two blocks above. One adds the correct, the other fills out the shuffledLabels list
            if (shuffledLabels.Count > indexForRealAnswer && shuffledLabels.Contains(q.correctLabelAnswer)) {
                // Cause buttons get shuffled, the index != MultipleChoice value. i.e. 0 != A all the time.
                // Find the index of the button that has the correct label (as it refers to the indexForRealAnswer)
                int buttonIndexWithCorrectLabel = -1;
                for (int i = 0; i < answerButtons.Count; i++) {
                    if ((int)answerButtons[i].ChoiceValue == indexForRealAnswer) {
                        buttonIndexWithCorrectLabel = i;
                        break;
                    }
                }

                shuffledLabels.Swap<string>(buttonIndexWithCorrectLabel, shuffledLabels.IndexOf(q.correctLabelAnswer));
            }

            // Apply the answer texts to the button labels
            Queue<string> answerQueue = new Queue<string>(shuffledLabels);
            for (int i = 0; i < totalChoices; i++) {
                if (answerQueue.Count > 0) {
                    string text = answerQueue.Dequeue();
                    answerButtons[i].SetLabel(text);
                    //choicesAdded++;
                }
            }

            // Check the question for button colors
            if (q.questionType == QuestionType.ButtonColor) {
                ColorAnswerValue colorAnswerValue = q.correctButtonColorAnswer;
                Color correctColor = ColorAnswer.GetColor(colorAnswerValue);
                Queue<Color> colorsToUse = new Queue<Color>(ColorAnswer.GetRandomList(3, correctColor));
                for (int i = 0; i < answerButtons.Count; i++) {
                    if ((int)answerButtons[i].ChoiceValue == indexForRealAnswer) {
                        answerButtons[i].SetColor(correctColor);
                    }
                    else {
                        answerButtons[i].SetColor(colorsToUse.Dequeue());
                    }
                }
            }

            questionTextFX.SetText(q.QuestionText.ToUpper());
            questionInstructionsLabel.text = q.instruction.ToUpper();
            //questionInstructionsLabel.text = (Utility.CamelCaseToReadable(q.questionCategory.ToString()).ToUpper());

            // This saves based on the labels that get set, not the button color
            correctButtonChoice = (MultipleChoice)indexForRealAnswer;

            // == RECAP INFO == //
            QuestionRecapData recapData = new QuestionRecapData();
            recapData.questionNumber = questionsAsked;
            recapData.question = q;
            recapData.correctMultipleChoice = correctButtonChoice;
            for (int i = 0; i < answerButtons.Count; i++) {
                recapData.buttonChoiceOrder.Add(answerButtons[i].ChoiceValue);
                recapData.buttonColorOrder.Add(answerButtons[i].CurrentColor);
            }
            recapData.buttonLabels = new List<string>(shuffledLabels);
            questionRecapList.Add(recapData);
            // == END of RECAP INFO == //

            questionInstructionsLabel.RT().DOScale(1, 0.45f).SetEase(Ease.OutBack);
            questionInstructionsLabel.CG().DOFade(1, 0.45f);
            questionTextFX.AnimationManager.PlayAnimation(0.1f, QuestionShowComplete);
        }
        else {
            EndLevel();
        }
    }

    private IEnumerator QuestionAskedGracePeriod() {
        yield return oneSecondWait;
        questionsAsked++;
    }

    private void QuestionShowComplete() {
        SFXManager.Instance.PlaySFX(SFX_TYPE.QuestionAppear);
        ShowAnswerLabels();
        timeManager.StartQuestionTimer();
        answerButtonsCG.interactable = true;
    }

    private void EndLevel() {
        answerButtonsCG.interactable = false;
        //ScreenMessage.PushMessage("Level Complete", CustomColor.GetColor(ColorName.PURPLE_PLUM), 2f);
        timeManager.StopTimer();

        pauseButton.SetActive(false);
        levelTimerLabel.SetActive(false);
        questionTimerLabel.SetActive(false);
        pointsNotification.gameObject.SetActive(false);
        //cheatQuestionsAskedLabel.gameObject.SetActive(false);

        QuestionSetProgression.Instance.AddAnswerSheetEntries(loadedQuestionSet.data.gameProgressionRank, answerSheetEntries);
        QuestionSetProgression.Instance.AddSetProgression(setProgression);
        QuestionSetProgression.Instance.Save();

        int currentAdShowPlayCount = Lazarus.Instance.GetPlayerDetail(PlayerDetail.AdShowPlayCount).Value;
        if (currentAdShowPlayCount > 0) {
            Lazarus.Instance.UpdatePlayerDetails(-1, PlayerDetail.AdShowPlayCount);
        }

        Invoke("ShowLevelSummary", 1f);
    }

    public void BeginLevel() {
        timeManager.BeginLevel();
        SetAnswerButtonsVisible(false);
        NewQuestion();
    }

    public void OnAnswerMade(MultipleChoice choiceMade) {
        bool correctButtonPressed = (correctButtonChoice == choiceMade);
        bool supposedToAnswerWrong = false;
        int newCorrectChoice = 0;

        answerButtonsCG.interactable = false;

        if (choiceMade != MultipleChoice.NONE) {

            switch (ruleSetData.gameRule) {
                case GameRule.RandomWrong:
                case GameRule.ModularWrong:
                    for (int i = 0; i < questionIndexesToAnswerWrong.Count; i++) {
                        if (questionsAsked == questionIndexesToAnswerWrong[i] + 1) {
                            supposedToAnswerWrong = true;
                            break;
                        }
                    }
                    break;
                case GameRule.WrongIfAnswer:
                    supposedToAnswerWrong = (correctButtonChoice == ruleSetData.wrongIfAnswerIs);
                    break;
                //case GameRule.CorrectIfAnswer:
                //    supposedToAnswerWrong = !(correctButtonChoice == ruleSetData.correctIfAnswerIs);
                //    break;
                case GameRule.CorrectAnswerPlusOne:
                    correctButtonChoice = (correctButtonChoice == MultipleChoice.D) ? MultipleChoice.A : (MultipleChoice)((int)correctButtonChoice + 1);
                    correctButtonPressed = (correctButtonChoice == choiceMade);
                    break;
                case GameRule.CorrectAnswerMinusOne:
                    correctButtonChoice = (correctButtonChoice == MultipleChoice.A) ? MultipleChoice.D : (MultipleChoice)((int)correctButtonChoice - 1);
                    correctButtonPressed = (correctButtonChoice == choiceMade);
                    break;
                case GameRule.CorrectAnswerPlus:
                    newCorrectChoice = (int)correctButtonChoice + ruleSetData.xValue;
                    newCorrectChoice = newCorrectChoice < 0 ? 4 - (0 - newCorrectChoice) : newCorrectChoice > 3 ? 0 + (newCorrectChoice - 4) : newCorrectChoice;
                    correctButtonChoice = (MultipleChoice)newCorrectChoice;
                    correctButtonPressed = (correctButtonChoice == choiceMade);
                    break;
                case GameRule.CorrectAnswerMinus:
                    newCorrectChoice = (int)correctButtonChoice - ruleSetData.xValue;
                    newCorrectChoice = newCorrectChoice < 0 ? 4 - (0 - newCorrectChoice) : newCorrectChoice > 3 ? 0 + (newCorrectChoice - 4) : newCorrectChoice;
                    correctButtonChoice = (MultipleChoice)newCorrectChoice;
                    correctButtonPressed = (correctButtonChoice == choiceMade);
                    break;
                default:
                    break;
            }
        
        }

        QuestionRecapData recapData = questionRecapList[questionRecapList.Count - 1];
        recapData.supposedToAnswerWrong = supposedToAnswerWrong;
        recapData.choiceMade = choiceMade;

        if ((correctButtonPressed && !supposedToAnswerWrong) || (!correctButtonPressed && supposedToAnswerWrong)) {
            SFXManager.Instance.PlaySFX(SFX_TYPE.AnswerCorrect);
            ScreenMessage.PushMessage("Correct", CustomColor.GetColor(ColorName.SUBMIT_GREEN));
            
            
            if (!QuestionSetProgression.Instance.HasSeenQuestion(setProgression.questionSetID, setProgression.questionIDsSeen[setProgression.questionIDsSeen.Count - 1])) {
                uniqueAnswersCorrect++;
                accumulatedScoreWithoutBonus += pointsPerUniqueQuestion;
            }
            else {
                seenAnswersCorrect++;
                accumulatedScoreWithoutBonus += pointsPerSeenQuestion;
            }

            recapData.answeredRight = true;


            //int bonusMade = Mathf.CeilToInt(timeManager.QuestionTimeRemaining * questionTimeRemainingScoreFactor);
            //int scoreMade = pointsPerUniqueQuestion + bonusMade;
            //accumulatedScoreWithoutBonus += pointsPerUniqueQuestion;
            //accumulatedBonusPoints += bonusMade;
            //pointsNotification.Show(scoreMade);
        }
        else {
            SFXManager.Instance.PlaySFX(SFX_TYPE.AnswerWrong);
            ScreenMessage.PushMessage("Wrong", CustomColor.GetColor(ColorName.RACY_RED));
            recapData.answeredRight = false;
            setProgression.questionIDsSeen.RemoveAt(setProgression.questionIDsSeen.Count - 1);
        }

        SFXManager.Instance.PlaySFX(SFX_TYPE.QuestionDisappear);
        timeManager.StopQuestionTimer();
        AddButtonHideEvent();
        HideAnswerLabels();
        HideQuestionAndCategory();
        // Hides answer buttons then waits for the hide to complete before showing the new question
    }

    public void OnReturnToLevelSummaryButtonPressed() {
        SFXManager.Instance.PlaySFX(SFX_TYPE.ButtonClickToony);
        levelSummaryWindow.Open();
        questionTextFX.gameObject.SetActive(false);
        SetAnswerButtonsVisible(false);
        HideAnswerLabels();
        HideRecapIcons();
        recapButtonsParent.SetActive(false);
    }

    public void OnViewRecapButtonPressed() {
        SFXManager.Instance.PlaySFX(SFX_TYPE.ButtonClickToony);
        indexBeingRecapped = -1;
        levelSummaryWindow.Close();
        PlayerPrefs.SetInt(recapUsedPrefsString, 1);
        questionTextFX.gameObject.SetActive(true);
        questionTextFX.AnimationManager.RemoveAnimation(0);
        SetAnswerButtonsVisible(true);
        ShowAnswerLabels();
        recapButtonsParent.SetActive(true);
        CheckForRecapCycleViewed();
        RecapNext();
    }

    private void CheckForRecapCycleViewed() {
        usedRecapCycleButtons = PlayerPrefs.GetInt(recapCyclePrefsString, 0).AsBool();
        recapCycleMessage.SetActive(!usedRecapCycleButtons);
    }

    public void OnRecapPreviousPressed() {
        SFXManager.Instance.PlaySFX(SFX_TYPE.ButtonClickToony);
        RecapPrevious();
    }

    public void OnRecapNextPressed() {
        SFXManager.Instance.PlaySFX(SFX_TYPE.ButtonClickToony);
        if (!usedRecapCycleButtons) {
            usedRecapCycleButtons = true;
        }
        RecapNext();
    }

    private void RecapPrevious() {
        int pre = indexBeingRecapped;
        indexBeingRecapped = indexBeingRecapped.AddValueClamped(-1, 0, questionRecapList.Count - 1);
        if (pre != indexBeingRecapped) {
            Recap();
        }
    }

    private void RecapNext() {
        int pre = indexBeingRecapped;
        indexBeingRecapped = indexBeingRecapped.AddValueClamped(1, 0, questionRecapList.Count - 1);
        if (pre != indexBeingRecapped) {
            Recap();
        }
    }

    private void Recap() {
        QuestionRecapData recapData = questionRecapList[indexBeingRecapped];
        Transform tempParent;
        int choiceMadeButtonIndex = -1;

        cheatQuestionsAskedLabel.text = "Question <color=#75C33FFF>#" + (indexBeingRecapped + 1).ToString() + "</color>";

        questionInstructionsLabel.RT().localScale = Vector3.one;
        questionInstructionsLabel.CG().alpha = 1;

        // == Choice Made == //
        choiceMadeIcon.CG().alpha = 0;
        choiceMadeIcon.localScale = recapScaleFrom;

        if (recapData.choiceMade != MultipleChoice.NONE) {
            choiceMadeButtonIndex = (int)recapData.choiceMade;
            for (int i = 0; i < recapData.buttonChoiceOrder.Count; i++) {
                if (recapData.buttonChoiceOrder[i] == recapData.choiceMade) {
                    choiceMadeButtonIndex = i;
                    break;
                }
            }

            tempParent = choiceMadeIcon.transform.parent;
            choiceMadeIcon.transform.SetParent(answerButtons[choiceMadeButtonIndex].ButtonTransform, false);
            choiceMadeIcon.anchoredPosition = Vector3.zero;
            choiceMadeIcon.SetParent(tempParent, true);

            choiceMadeIcon.CG().DOFade(1, recapScaleSpeed);
            choiceMadeIcon.DOScale(1, recapScaleSpeed);
        }

        // == Correct Answer == //
        for (int i = 0; i < answerRecapIcons.Count; i++) {
			answerRecapIcons[i].CG().alpha = 0;
            answerRecapIcons[i].localScale = recapScaleFrom;
        }

        choiceMadeButtonIndex = (int)recapData.correctMultipleChoice;
        for (int i = 0; i < recapData.buttonChoiceOrder.Count; i++) {
            if (recapData.buttonChoiceOrder[i] == recapData.correctMultipleChoice) {
                choiceMadeButtonIndex = i;
                break;
            }
        }

        int answerIconIndex = 0;
        RectTransform answerRecapIconRT = answerRecapIcons[0];
        if (recapData.supposedToAnswerWrong) {
            for (int i = 0; i < answerButtons.Count; i++) {
                if (i == (int)recapData.correctMultipleChoice) {
                    continue;
                }

                answerRecapIconRT = answerRecapIcons[answerIconIndex];
                tempParent = answerRecapIconRT.transform.parent;
                answerRecapIconRT.transform.SetParent(answerButtons[i].ButtonTransform, false);
                answerRecapIconRT.anchoredPosition = Vector3.zero;
                answerRecapIconRT.transform.SetParent(tempParent, true);
                answerRecapIconRT.CG().DOFade(1, recapScaleSpeed);
                answerRecapIconRT.RT().DOScale(1, recapScaleSpeed);
                answerIconIndex++;
            }
        }
        else {
            answerRecapIconRT = answerRecapIcons[0];
            tempParent = answerRecapIconRT.transform.parent;
            answerRecapIconRT.transform.SetParent(answerButtons[choiceMadeButtonIndex].ButtonTransform, false);
            answerRecapIconRT.anchoredPosition = Vector3.zero;
            answerRecapIconRT.transform.SetParent(tempParent, true);
            answerRecapIconRT.CG().DOFade(1, recapScaleSpeed);
            answerRecapIconRT.DOScale(1, recapScaleSpeed);
        }

        // ==  Question Label == //
        questionTextFX.SetText(recapData.question.QuestionText.ToUpper());
        questionInstructionsLabel.text = recapData.question.instruction.ToUpper();
        //questionInstructionsLabel.text = (Utility.CamelCaseToReadable(recapData.question.questionCategory.ToString()));

        //Debug.Log(recapData.buttonLabels.Count);
        // ==  Answer Labels == //
        for (int i = 0; i < answerButtons.Count; i++) {
            answerButtons[i].SetLabel(recapData.buttonLabels[i]);
        }

        // == Answer Button Choice Labels == //
        for (int i = 0; i < recapData.buttonChoiceOrder.Count; i++) {
            answerButtons[i].SetChoiceValue((int)recapData.buttonChoiceOrder[i]);
        }

        // == Button Colors == //
        for (int i = 0; i < recapData.buttonColorOrder.Count; i++) {
            answerButtons[i].SetColor(recapData.buttonColorOrder[i]);
        }

        // == Answered Right == //
        answeredRightIcon.CG().alpha = 0;
        answeredWrongIcon.CG().alpha = 0;
        answeredRightIcon.localScale = recapScaleFrom;
        answeredWrongIcon.localScale = recapScaleFrom;
        if (recapData.answeredRight) {
            answeredRightIcon.CG().DOFade(1, recapScaleSpeed);
            answeredRightIcon.DOScale(1, recapScaleSpeed);
        }
        else {
            answeredWrongIcon.CG().DOFade(1, recapScaleSpeed);
            answeredWrongIcon.DOScale(1, recapScaleSpeed);
        }
        
    }

    public void PauseGame() {
        questionTextFX.AnimationManager.Paused = true;
        timeManager.PauseGame();
    }

    public void UnpauseGame() {
        questionTextFX.AnimationManager.Paused = false;
        timeManager.UnpauseGame();
    }

    [CommandHandler]
    private void DebugAnswerAll(bool correct) {
        DebugOnAnswerMade(correct);
        timeManager.StopQuestionTimer();
        while (questionsAsked < totalQuestions && questionsQueue.Count > 0) {

            Question q = questionsQueue.Dequeue();
            int totalChoices = 4;
            //int indexForRealAnswer = Random.Range(0, totalChoices);

            if (q.questionType == QuestionType.Label) {
                answerSheetEntries.Add(new AnswerSheetEntry(q.uid, q.QuestionText, q.correctLabelAnswer));
            }
            setProgression.questionIDsSeen.Add(q.uid);
            List<string> labels = new List<string>(q.buttonLabelOptions);
            //List<string> shuffledLabels = Utility.ShuffleList<string>(q.buttonLabelOptions, Random.Range(0, 100000));

            // Check if the shuffledLabels contains the real answer, if not, put it in
            if (!labels.Contains(q.correctLabelAnswer)) {
                labels.Add(q.correctLabelAnswer);
            }

            // Clear out any empty labels (sanity check)
            for (int i = 0; i < labels.Count; i++) {
                if (string.IsNullOrEmpty(labels[i])) {
                    labels.RemoveAt(i);
                    i = 0;
                }
            }

            // If less than 4 prefered answers were supplied
            if (labels.Count < totalChoices) {
                string btnText = "";
                for (int i = labels.Count; i < totalChoices; i++) {
                    // MAKE SURE THAT THE RANDOM ANSWERS DO NOT MEAN THE SAME THING AS THE REAL ANSWER... THESAURUS!!!
                    btnText = adjectivesAnswerTable.RandomAnswer();

                    labels.Add(btnText);
                }
            }

            // This should always get in, cause of the two blocks above. One adds the correct, the other fills out the shuffledLabels list
            if (labels.Count > 0 && labels.Contains(q.correctLabelAnswer)) {
                // Cause buttons get shuffled, the index != MultipleChoice value. i.e. 0 != A all the time.
                // Find the index of the button that has the correct label (as it refers to the indexForRealAnswer)
                int buttonIndexWithCorrectLabel = -1;
                for (int i = 0; i < answerButtons.Count; i++) {
                    if ((int)answerButtons[i].ChoiceValue == 0) {
                        buttonIndexWithCorrectLabel = i;
                        break;
                    }
                }

                labels.Swap<string>(buttonIndexWithCorrectLabel, labels.IndexOf(q.correctLabelAnswer));
            }

            // Apply the answer texts to the button labels
            Queue<string> answerQueue = new Queue<string>(labels);
            for (int i = 0; i < totalChoices; i++) {
                if (answerQueue.Count > 0) {
                    string text = answerQueue.Dequeue();
                    answerButtons[i].SetLabel(text);
                }
            }

            // Check the question for button colors
            if (q.questionType == QuestionType.ButtonColor) {
                ColorAnswerValue colorAnswerValue = q.correctButtonColorAnswer;
                Color correctColor = ColorAnswer.GetColor(colorAnswerValue);
                Queue<Color> colorsToUse = new Queue<Color>(ColorAnswer.GetRandomList(3, correctColor));
                for (int i = 0; i < answerButtons.Count; i++) {
                    if ((int)answerButtons[i].ChoiceValue == 0) {
                        answerButtons[i].SetColor(correctColor);
                    }
                    else {
                        answerButtons[i].SetColor(colorsToUse.Dequeue());
                    }
                }
            }

            // This saves based on the labels that get set, not the button color
            correctButtonChoice = (MultipleChoice)0;

            // == RECAP INFO == //
            QuestionRecapData recapData = new QuestionRecapData();
            recapData.questionNumber = questionsAsked;
            recapData.question = q;
            recapData.correctMultipleChoice = correctButtonChoice;
            for (int i = 0; i < answerButtons.Count; i++) {
                recapData.buttonChoiceOrder.Add(answerButtons[i].ChoiceValue);
                recapData.buttonColorOrder.Add(answerButtons[i].CurrentColor);
            }
            recapData.buttonLabels = new List<string>(labels);
            questionRecapList.Add(recapData);
            // == END of RECAP INFO == //

            questionsAsked++;
            DebugOnAnswerMade(correct);
        }

        EndLevel();
    }

    public void DebugOnAnswerMade(bool correct) {
        MultipleChoice choiceMade = (MultipleChoice)0;
        bool correctButtonPressed = correct;
        bool supposedToAnswerWrong = false;

        answerButtonsCG.interactable = false;

        QuestionRecapData recapData = questionRecapList[questionRecapList.Count - 1];
        recapData.supposedToAnswerWrong = supposedToAnswerWrong;
        recapData.choiceMade = choiceMade;

        if ((correctButtonPressed && !supposedToAnswerWrong) || (!correctButtonPressed && supposedToAnswerWrong)) {
            ScreenMessage.PushMessage("Correct", CustomColor.GetColor(ColorName.SUBMIT_GREEN));

            if (!QuestionSetProgression.Instance.HasSeenQuestion(setProgression.questionSetID, setProgression.questionIDsSeen[setProgression.questionIDsSeen.Count - 1])) {
                uniqueAnswersCorrect++;
                accumulatedScoreWithoutBonus += pointsPerUniqueQuestion;
            }
            else {
                seenAnswersCorrect++;
                accumulatedScoreWithoutBonus += pointsPerSeenQuestion;
            }

            recapData.answeredRight = true;

            //int bonusMade = Mathf.CeilToInt(timeManager.QuestionTimeRemaining * questionTimeRemainingScoreFactor);
            //int scoreMade = pointsPerUniqueQuestion + bonusMade;
            //accumulatedScoreWithoutBonus += pointsPerUniqueQuestion;
            //accumulatedBonusPoints += bonusMade;
            //pointsNotification.Show(scoreMade);
        }
        else {
            ScreenMessage.PushMessage("Wrong", CustomColor.GetColor(ColorName.RACY_RED));
            recapData.answeredRight = false;
            setProgression.questionIDsSeen.RemoveAt(setProgression.questionIDsSeen.Count - 1);
        }
    }

    // Update is called once per frame
    void Update() {
        #if UNITY_EDITOR
        if (Input.GetKeyUp(KeyCode.Alpha9)) {
            DebugAnswerAll(true);
        }
        
        if (Input.GetKeyUp(KeyCode.Alpha0)) {
            DebugAnswerAll(false);
        }
        #endif
    }

}
