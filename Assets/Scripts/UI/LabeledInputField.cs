﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class LabeledInputField : MonoBehaviour {

    private List<LabeledInputField> listOwner;
    private int listIndex;

    public Text nameLabel;
    public InputField inputField;
    public Button removeButton;
    public Text removeButtonLabel;

    [Header("Confirm Color Block")]
    public ColorBlock confirmColorBlock;

    public string Text { get { return inputField.text; } }

    void Awake() {
        
    }

    // Use this for initialization
    void Start() {
        removeButton.onClick.AddListener(OnRemoveButtonClicked);
    }

    private void OnRemoveButtonClicked() {
        ConfirmWindow.Open("Confirm Remove Item?", OnRemoveConfirmed);
    }

    private void OnRemoveConfirmed(bool value) {
        if (value) {
            listOwner.RemoveAt(listIndex);
            removeButton.onClick.RemoveListener(OnRemoveButtonClicked);

            for (int i = listIndex; i < listOwner.Count; i++) {
                listOwner[i].UpdateIndex(i);
            }

            Destroy(this.gameObject);
        }
    }

    public void SetNameLabel(string txt) {
        nameLabel.text = txt;
    }

    public void SetInputValue(string txt) {
        inputField.text = txt;
    }

    public void SetListAndIndex(ref List<LabeledInputField> options, int index) {
        listOwner = options;
        listIndex = index;
    }

    public void UpdateIndex(int index) {
        listIndex = index;
    }

    // Update is called once per frame
    void Update() {

    }
}
