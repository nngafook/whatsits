﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class AnswerSheetWindow : Popup {

    private int selectedSetIndex = 0;

    private List<AnswerSheetEntrySet> entrySets = new List<AnswerSheetEntrySet>();
    private List<GameObject> sheetEntryObjects = new List<GameObject>();

    public GameObject answerSheetItemPrefab;
    public Transform listParent;
    public Text setIndexLabel;
    public CanvasGroup nothingToSeeCG;
    public CanvasGroup previousSetButtonCG;
    public CanvasGroup nextSetButtonCG;

    public override void Start() {
        base.Start();
        Lazarus.Instance.OnQuestionSetsResetEvent.AddListener(OnQuestionSetsReset);
    }


    public void OnDestroy() {
        Lazarus.Instance.OnQuestionSetsResetEvent.RemoveListener(OnQuestionSetsReset);
    }

    private void OnQuestionSetsReset() {
        entrySets.Clear();
        DeleteListObjects();
    }

    private void UpdateScrollButtonsVisible() {
        bool visible = (selectedSetIndex != 0 && entrySets.Count > 1);
        previousSetButtonCG.SetAlphaAndBools(visible);
        visible = (selectedSetIndex != entrySets.Count - 1 && entrySets.Count > 1);
        nextSetButtonCG.SetAlphaAndBools(visible);
    }

    private void Load() {
        selectedSetIndex = 0;

        // Load the entries from saveprogression into a local list
        LoadEntrySetsToLocalList();

        RefreshList();
    }

    /// <summary>
    /// This method currently loads ALL question sets that have answer entries. If the number of question sets gets too high
    /// this method may need to stagger the loading. Maybe only keep 5 loaded at a time, two on either side of the currently showing one.
    /// </summary>
    private void LoadEntrySetsToLocalList() {
        List<AnswerSheetEntrySet> answerSheetEntrySets = QuestionSetProgression.Instance.answerSheetEntrySets;
        nothingToSeeCG.alpha = (answerSheetEntrySets == null || answerSheetEntrySets.Count <= 0) ? 1 : 0;
        setIndexLabel.text = "";

        bool exists = false;
        for (int i = 0; i < answerSheetEntrySets.Count; i++) {
            exists = false;
            for (int j = 0; j < entrySets.Count; j++) {
                if (entrySets[j].progressionIndex == answerSheetEntrySets[i].progressionIndex) {
                    exists = true;
                    break;
                }
            }
            if (!exists) {
                entrySets.Add(answerSheetEntrySets[i]);
            }
        }
    }

    private void RefreshList() {
        if (selectedSetIndex < entrySets.Count && entrySets.Count > 0) {
            DeleteListObjects();

            // Create the list items
            AnswerSheetItem item = null;
            AnswerSheetEntrySet entrySet = entrySets[selectedSetIndex];
            for (int i = 0; i < entrySet.entries.Count; i++) {
                item = (Instantiate(answerSheetItemPrefab, listParent) as GameObject).GetComponent<AnswerSheetItem>();
                item.SetInfo(entrySet.entries[i]);
                sheetEntryObjects.Add(item.gameObject);
            }

            setIndexLabel.text = "Question Set " + (selectedSetIndex + 1).ToString();

            UpdateScrollButtonsVisible();
        }
        else if (selectedSetIndex < entrySets.Count) {
            Debug.LogError("AnswerSheetWindow asks: 'Why the hell did the selectedSetIndex get higher than the amount of entries in entrySets?'");
        }
    }

    private void DeleteListObjects() {
        // Delete objects form scroll rect
        for (int i = 0; i < sheetEntryObjects.Count; i++) {
            Destroy(sheetEntryObjects[i]);
        }
        sheetEntryObjects = new List<GameObject>();
    }

    public override void Open() {
        SFXManager.Instance.PlaySFX(SFX_TYPE.ButtonClickToony);
        previousSetButtonCG.SetAlphaAndBools(false);
        nextSetButtonCG.SetAlphaAndBools(false);

        Load();

        base.Open();
    }

    protected override void OnOpenComplete() {
        base.OnOpenComplete();

        UpdateScrollButtonsVisible();
    }

    public override void Close() {
        SFXManager.Instance.PlaySFX(SFX_TYPE.ButtonClickToony);
        previousSetButtonCG.SetAlphaAndBools(false);
        nextSetButtonCG.SetAlphaAndBools(false);

        base.Close();
    }

    public void NextSet() {
        SFXManager.Instance.PlaySFX(SFX_TYPE.ButtonClickToony);
        selectedSetIndex = selectedSetIndex.AddValueClamped(1, 0, entrySets.Count - 1);

        RefreshList();
    }

    public void PreviousSet() {
        SFXManager.Instance.PlaySFX(SFX_TYPE.ButtonClickToony);
        selectedSetIndex = selectedSetIndex.AddValueClamped(-1, 0, entrySets.Count - 1);

        RefreshList();
    }

	// Update is called once per frame
	void Update () {
	
	}
}
