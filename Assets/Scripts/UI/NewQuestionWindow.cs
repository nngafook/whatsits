﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class NewQuestionWindow : MonoBehaviour {

    private List<EnumListItem> subcategoriesListItems;
    private List<LabeledInputField> labelOptions;

    public InputField questionText;
    public Dropdown questionTypeDropdown;
    public Dropdown questionCategoryDropdown;

    public InputField correctLabelAnswer;
    public Dropdown answerCategoryDropdown;
    public Dropdown correctButtonColorAnswerDropdown;
    public Dropdown correctLabelColorAnswerDropdown;

    [Space(10)]
    public GameObject labelOptionPrefab;
    public Transform labelOptionParent;
    public GameObject subcategoryItemPrefab;
    public Transform subcategoryParent;

    [Space(10)]
    public Button addLabelOptionsButton;
    public ScrollRect questionScrollRect;

    public List<string> LabelOptions {
        get {
            List<string> options = new List<string>();
            for (int i = 0; i < labelOptions.Count; i++) {
                if (!string.IsNullOrEmpty(labelOptions[i].Text)) {
                    options.Add(labelOptions[i].Text);
                }
            }
            return options;
        }
    }

    void Awake() {
        addLabelOptionsButton.onClick.AddListener(AddLabelOptions);
    }

    // Use this for initialization
    void Start() {

    }

    public void Open() {
        this.CG().SetAlphaAndBools(true);
    }

    public void Close() {
        this.CG().SetAlphaAndBools(false);
    }

    private void AddLabelOptions() {
        LabeledInputField labelOption = (Instantiate(labelOptionPrefab, Vector3.zero, Quaternion.identity) as GameObject).GetComponent<LabeledInputField>();
        labelOption.transform.SetParent(this.transform);
        labelOption.transform.SetAsLastSibling();
        addLabelOptionsButton.transform.SetAsLastSibling();
        labelOptions.Add(labelOption);
        labelOption.SetNameLabel("Label Option " + labelOptions.Count.ToString());
        labelOption.SetListAndIndex(ref labelOptions, labelOptions.Count - 1);
        ScrollToBottom();
    }

    private void ScrollToBottom() {
        questionScrollRect.verticalNormalizedPosition = 0;
    }

    public void ClearFields() {
        for (int i = 0; i < labelOptions.Count; i++) {
            Destroy(labelOptions[i].gameObject);
        }
        labelOptions.Clear();
    }

    // Update is called once per frame
    void Update() {

    }
}
