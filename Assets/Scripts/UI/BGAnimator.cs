﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using DG.Tweening;

public class BGAnimator : MonoBehaviour {

    public float scaleSpeed;
    public float colorSpeed;
    public float scaleAmount;
    public Color overlayLerpToColor;

    public Image bgImage;
    public Image blendOverlay;

    void Awake() {
        if (bgImage == null && blendOverlay == null) {
            this.enabled = false;
        }
    }

    // Use this for initialization
    void Start() {
        Go();
    }

    private void DoScale() {
        if (bgImage != null) {
            int random = Random.Range(0, 3);
            switch (random) {
                case 0:
                    bgImage.RT().DOScaleX(scaleAmount, scaleSpeed).SetLoops(2, LoopType.Yoyo).OnComplete(DoScale);
                    break;
                case 1:
                    bgImage.RT().DOScaleY(scaleAmount, scaleSpeed).SetLoops(2, LoopType.Yoyo).OnComplete(DoScale);
                    break;
                case 2:
                    bgImage.RT().DOScale(scaleAmount, scaleSpeed).SetLoops(2, LoopType.Yoyo).OnComplete(DoScale);
                    break;
                default:
                    break;
            }
        }
    }

    private void Go() {
        DoScale();
        if (blendOverlay != null) {
            blendOverlay.DOColor(overlayLerpToColor, colorSpeed).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.Linear);
        }
    }

    // Update is called once per frame
    void Update() {

    }
}
