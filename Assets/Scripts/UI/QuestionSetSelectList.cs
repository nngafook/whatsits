﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using Opencoding.CommandHandlerSystem;

public class QuestionSetSelectList : MonoBehaviour {

    // Stores the progressionIndex of the highest set owned in the selected category
    private int highestSetOwned = -1;
    private bool debugUnlockAllSets = false;

    private List<QuestionSet> questionSetObjects = null;
    private List<QuestionSetSelectButton> listButtons = new List<QuestionSetSelectButton>();

    public QuestionSetLobby lobby;
    public GameObject listItemPrefab;
    public Transform listItemParent;

    void Awake() {
        //dataObjectPathPrefix = System.IO.Path.Combine(Application.dataPath, dataObjectPathPrefix);
        CommandHandlers.RegisterCommandHandlers(this);
    }

    public void OnDestroy() {
        CommandHandlers.UnregisterCommandHandlers(this);
    }

    // Use this for initialization
    void Start() {
        
    }

    [CommandHandler]
    private void UnlockAllQuestionSets() {
        debugUnlockAllSets = true;
        LoadItems();
        debugUnlockAllSets = false;
    }

    private void AddItem(ref QuestionSetData data) {
        QuestionSetSelectButton btn = (Instantiate(listItemPrefab, Vector3.zero, Quaternion.identity) as GameObject).GetComponent<QuestionSetSelectButton>();
        btn.transform.SetParent(listItemParent, false);
        //btn.OnPressedEvent.AddListener(OnSetSelectButtonPressed);
        listButtons.Add(btn);

        btn.SetLabels(listButtons.Count);
        btn.SetData(ref data, this);
    }

    private void ClearItems() {
        for (int i = 0; i < listButtons.Count; i++) {
            //listButtons[i].OnPressedEvent.RemoveListener(OnSetSelectButtonPressed);
            Destroy(listButtons[i].gameObject);
        }
        listButtons.Clear();
    }

    private void UpdateHighestOwned() {
        highestSetOwned = -1;
        QuestionSetData data = null;
        if (questionSetObjects.Count > 0) {
            for (int i = 0; i < questionSetObjects.Count; i++) {
                data = questionSetObjects[i].data;
                if ((data.cost == 0 || QuestionSetProgression.Instance.OwnsSet(data.id)) && data.gameProgressionRank > highestSetOwned) {
                    highestSetOwned = data.gameProgressionRank;
                }
            }

            if (highestSetOwned < questionSetObjects.Count) {
                // highestSetOwned != 0 index, so we don't add one to it when setting the next button to be able to be unlocked
                listButtons[highestSetOwned].SetCanBeUnlocked(true);
            }
        }
    }

    public void UnlockQuestionSetPressed() {
        lobby.UpdateCurrencyLabel();
        UpdateHighestOwned();
    }

    public void QuestionSetSelected(int index) {
        // Index minus one for array index
        Lazarus.Instance.QuestionSetInPlay = questionSetObjects[index - 1];
        lobby.StartGame();
    }

    public void OpenResetConfirmPopup() {
        lobby.OpenResetConfirmPopup();
    }

    public void LoadItems() {
        ClearItems();
        string path = Path.Combine(Application.persistentDataPath, "GeneratedSets");
        path = Path.Combine(path, "v1");
        questionSetObjects = new List<QuestionSet>();

        string[] paths = Directory.GetFiles(path, "*.json");

        QuestionSetData setData = null;
        QuestionSet qs = null;
        for (int i = 0; i < paths.Length; i++) {
            setData = JsonUtility.FromJson<QuestionSetData>(System.IO.File.ReadAllText(paths[i]));
            qs = ScriptableObject.CreateInstance<QuestionSet>();
            qs.data = setData;
            questionSetObjects.Add(qs);
        }

        questionSetObjects.Sort((x, y) => x.data.gameProgressionRank.CompareTo(y.data.gameProgressionRank));

        for (int i = 0; i < questionSetObjects.Count; i++) {
            setData = questionSetObjects[i].data;

            if (setData.gameProgressionRank == -1) {
                Debug.Log("Question Set: " + setData.id + " skipped cause it's game progression ID was -1");
                continue;
            }



            AddItem(ref setData);

        }


        /*
        path = System.IO.Path.Combine(path, category.ToString());
        questionSetObjects = Resources.LoadAll<QuestionSet>(path);
        Array.Sort(questionSetObjects, delegate(QuestionSet x, QuestionSet y) { return x.data.gameProgressionRank.CompareTo(y.data.gameProgressionRank); });

        highestSetOwned = -1;
        QuestionSetData data = null;
        for (int i = 0; i < questionSetObjects.Length; i++) {
            data = questionSetObjects[i].data;

            if (data.gameProgressionRank == -1) {
                Debug.Log("Question Set: " + data.id + " skipped cause it's game progression ID was -1");
                continue;
            }

            

            AddItem(ref data);

        }
        */
        UpdateHighestOwned();

    }

    // Update is called once per frame
    void Update() {

    }
}
