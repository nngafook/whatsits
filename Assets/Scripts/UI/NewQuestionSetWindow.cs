﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.IO;

public class NewQuestionSetWindow : MonoBehaviour {

    private string newSetHeaderText = "Question Set <color=#F76060FF>*</color>";
    private string loadSetHeaderText = "Load Question Set";

    private List<string> questionSetPaths = new List<string>();
    private QuestionSetData questionSetData;

    public GameDesigner gameDesigner;
    public Text headerLabel;

    [Header("New Set")]
    public InputField newSetInputField;
    public CanvasGroup newQuestionSetCG;

    [Header("Load Set")]
    public Dropdown loadSetDropdown;
    public CanvasGroup loadQuestionSetCG;
    public Toggle loadProjectSetsToggle;

    void Awake() {
        #if UNITY_EDITOR
            loadProjectSetsToggle.isOn = true;
        #else
            loadProjectSetsToggle.gameObject.SetActive(false);
        #endif
    }

    // Use this for initialization
    void Start() {
        #if UNITY_EDITOR
            loadProjectSetsToggle.onValueChanged.AddListener(OnProjectSetsToggleChange);
        #endif

        this.CG().SetAlphaAndBools(false);
    }

    private void OnProjectSetsToggleChange(bool value) {
        gameDesigner.UsingProjectQuestionSets = value;
        LoadQuestionSetDropdown();
    }

    private bool QuestionSetNameExists(string setName) {
        bool rVal = false;
        string[] paths = Directory.GetFiles(Utility.SavedQuestionSetsPath);
        string filename = "";
        if (paths.Length > 0) {
            for (int i = 0; i < paths.Length; i++) {
                filename = Path.GetFileNameWithoutExtension(paths[i]);
                if (setName.ToUpper() == filename.ToUpper()) {
                    rVal = true;
                    break;
                }
            }
        }
        return rVal;
    }

    public void OnCreateQuestionSetButtonPressed() {
        if (!QuestionSetNameExists(newSetInputField.text)) {
            questionSetData = new QuestionSetData();
            questionSetData.name = newSetInputField.text.ToUpper().Replace(" ", string.Empty);
            questionSetData.questions = new List<Question>();
            gameDesigner.CreateQuestionSet(questionSetData);
            gameDesigner.SetBlackOverlay(false);
            this.CG().SetAlphaAndBools(false);
        }
        else {
            ScreenMessage.PushMessage("Question Set Name Exists", CustomColor.GetColor(ColorName.ERROR_RED), 1f);
        }
    }

    public void OnLoadQuestionSetButtonPressed() {
        questionSetData = new QuestionSetData();

        QuestionSetData loadedQuestionSetData = JsonUtility.FromJson<QuestionSetData>(System.IO.File.ReadAllText(questionSetPaths[loadSetDropdown.value]));
        questionSetData.Copy(loadedQuestionSetData);

        gameDesigner.LoadQuestionSet(questionSetData);
        gameDesigner.SetBlackOverlay(false);
        this.CG().SetAlphaAndBools(false);
    }

    public void OnCancelButtonPressed() {
        newSetInputField.text = "";
        this.CG().SetAlphaAndBools(false);
        gameDesigner.SetBlackOverlay(false);
    }

    public void ShowNewQuestionSetContent() {
        gameDesigner.SetBlackOverlay(true);
        newQuestionSetCG.SetAlphaAndBools(true);
        loadQuestionSetCG.SetAlphaAndBools(false);
        headerLabel.text = newSetHeaderText;
    }

    public void ShowLoadQuestionSetContent() {
        gameDesigner.SetBlackOverlay(true);
        newQuestionSetCG.SetAlphaAndBools(false);
        loadQuestionSetCG.SetAlphaAndBools(true);

        headerLabel.text = loadSetHeaderText;

        LoadQuestionSetDropdown();
    }

    private void LoadQuestionSetDropdown() {
        loadSetDropdown.options.Clear();

        questionSetPaths.Clear();

        string[] paths = (loadProjectSetsToggle.isOn) ? Directory.GetFiles(System.IO.Path.Combine(Application.dataPath, "QuestionSetJSONFiles"), "*.json") : Directory.GetFiles(Utility.SavedQuestionSetsPath);
        if (paths.Length > 0) {
            for (int i = 0; i < paths.Length; i++) {
                questionSetPaths.Add(paths[i]);
                loadSetDropdown.options.Add(new UnityEngine.UI.Dropdown.OptionData(Path.GetFileNameWithoutExtension(paths[i])));
            }

            //newSetInputField.text = Path.GetFileNameWithoutExtension(paths[0]);
        }
        else {
            loadSetDropdown.options.Add(new UnityEngine.UI.Dropdown.OptionData("<Empty>"));
        }

        loadSetDropdown.RefreshShownValue();
    }

    // Update is called once per frame
    void Update() {

    }
}
