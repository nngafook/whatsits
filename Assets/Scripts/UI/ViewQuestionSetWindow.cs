﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class ViewQuestionSetWindow : MonoBehaviour {

    private List<QuestionListItem> listItems = new List<QuestionListItem>();

    private bool isOpen = false;
    private float scrollPosition;

    public GameObject questionListItemPrefab;

    [Space(10)]
    public ScrollRect scrollRect;
    public Text totalQuestionsLabel;

    [Space(10)]
    public GameDesigner gameDesigner;
    public Transform questionListParent;

    // Use this for initialization
    void Start() {

    }

    private void ClearList() {
        for (int i = 0; i < listItems.Count; i++) {
            Destroy(listItems[i].gameObject);
        }
        listItems.Clear();
        scrollRect.normalizedPosition = Vector2.one;
    }

    public void SetData(QuestionSetData data) {
        ClearList();
        QuestionListItem listItem;
        for (int i = 0; i < data.questions.Count; i++) {
            listItem = (Instantiate(questionListItemPrefab, Vector3.zero, Quaternion.identity) as GameObject).GetComponent<QuestionListItem>();
            listItem.Init(ref gameDesigner, data.questions[i].QuestionText, i);
            listItems.Add(listItem);
            listItem.transform.SetParent(questionListParent, false);
        }
        totalQuestionsLabel.text = "Total Questions: <color=#35A3E2FF>" + data.questions.Count + "</color>";
    }

    public void Open() {
        isOpen = true;
        this.CG().SetAlphaAndBools(true);
        scrollRect.verticalNormalizedPosition = scrollPosition;
    }

    public void Close() {
        isOpen = false; ;
        scrollPosition = scrollRect.verticalNormalizedPosition;
        this.CG().SetAlphaAndBools(false);
    }

    public void ScrollToBottom() {
        scrollRect.normalizedPosition = Vector2.zero; 
    }

    // Update is called once per frame
    void Update() {
        if (isOpen && Input.GetKeyUp(KeyCode.N)) {
            gameDesigner.OnNewQuestionButtonPressed();
        }
    }
}
