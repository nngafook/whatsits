﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.Events;
using TextFx;

public class LevelSummaryWindow : Popup {

    [HideInInspector]
    public UnityEvent OnReturningToLobby = new UnityEvent();

    public GameObject seenQuestionsLabelObject;
    public GameObject uniqueQuestionsLabelObject;

    [Space(10)]
    public Text questionsValueLabel;
    public Text uniqueQuestionsScoreValueLabel;
    public Text seenQuestionsScoreValueLabel;
    public Text totalValueLabel;

    public SceneTransitionBlackOverlay sceneTransitionBlackOverlay;

    public void Open(int totalQuestions, int correctUniqueAnswers, int correctSeenAnswers, int pointsPerUniqueQuestion, int pointsPerSeenQuestion, int points) {
        questionsValueLabel.text = (correctUniqueAnswers + correctSeenAnswers).ToString() + " / " + totalQuestions.ToString();
        totalValueLabel.text = points.ToString();
        CheckForSeenAnswers(correctSeenAnswers, pointsPerSeenQuestion);
        CheckForUniqueAnswers(correctUniqueAnswers, pointsPerUniqueQuestion);
        Open();
    }

    private void CheckForSeenAnswers(int correctSeenAnswers, int pointsPerSeenQuestion) {
        if (correctSeenAnswers > 0) {
            seenQuestionsLabelObject.SetActive(true);
            seenQuestionsScoreValueLabel.gameObject.SetActive(true);
            seenQuestionsScoreValueLabel.text = correctSeenAnswers.ToString() + " x " + pointsPerSeenQuestion.ToString();
        }
        else {
            seenQuestionsLabelObject.SetActive(false);
            seenQuestionsScoreValueLabel.gameObject.SetActive(false);
        }
    }

    private void CheckForUniqueAnswers(int correctUniqueAnswers, int pointsPerUniqueQuestion) {
        if (correctUniqueAnswers > 0) {
            uniqueQuestionsLabelObject.SetActive(true);
            uniqueQuestionsLabelObject.GetComponent<TextFxUGUI>().AnimationManager.PlayAnimation();
            uniqueQuestionsScoreValueLabel.gameObject.SetActive(true);
            uniqueQuestionsScoreValueLabel.text = correctUniqueAnswers.ToString() + " x " + pointsPerUniqueQuestion.ToString();
        }
        else {
            uniqueQuestionsLabelObject.SetActive(false);
            uniqueQuestionsScoreValueLabel.gameObject.SetActive(false);
        }
    }

    public void OnOKPressed() {
        SFXManager.Instance.PlaySFX(SFX_TYPE.ButtonClickToony);
        if (OnReturningToLobby != null) {
            OnReturningToLobby.Invoke();
        }
        sceneTransitionBlackOverlay.FadeToScene("Lobby");
    }

    // Update is called once per frame
    void Update() {

    }
}
