﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

[CreateAssetMenu(menuName = "DataObjects/QuestionSetIOUtility")]
public class QuestionSetIOUtility : ScriptableObject {

    public string jsonPath = "";
    public string newQuestionSetName = "";

}
