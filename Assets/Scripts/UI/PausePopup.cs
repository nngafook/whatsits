﻿using UnityEngine;
using System.Collections;

public class PausePopup : OptionsPopup {

    public SceneTransitionBlackOverlay sceneTransitionBlackOverlay;

    public override void Start() {

    }

    public override void Close() {
        SFXManager.Instance.PlaySFX(SFX_TYPE.ButtonClickToony);
        base.Close();
    }

    public void OnQuitPressed() {
        SFXManager.Instance.PlaySFX(SFX_TYPE.ButtonClickToony);
        ConfirmWindow.Open("RETURN TO LEVEL SELECT?", OnQuitConfirmed);
    }

    private void OnQuitConfirmed(bool value) {
        if (value) {
            int currentAdShowPlayCount = Lazarus.Instance.GetPlayerDetail(PlayerDetail.AdShowPlayCount).Value;
            if (currentAdShowPlayCount > 0) {
                Lazarus.Instance.UpdatePlayerDetails(-1, PlayerDetail.AdShowPlayCount);
            }
            sceneTransitionBlackOverlay.FadeToScene("Lobby");
        }
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
