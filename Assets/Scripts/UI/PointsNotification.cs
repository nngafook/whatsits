﻿using UnityEngine;
using DG.Tweening;
using System.Collections;
using UnityEngine.UI;

[RequireComponent(typeof(CanvasGroup))]
[RequireComponent(typeof(Text))]
public class PointsNotification : MonoBehaviour {

    private float tweenSpeed = 0.5f;
    private float startY = 0;

    private CanvasGroup cg;
    private RectTransform rt;
    private Text label;

    void Awake() {
        cg = this.GetComponent<CanvasGroup>();
        rt = this.GetComponent<RectTransform>();
        label = this.GetComponent<Text>();
        startY = rt.anchoredPosition.y;
        Hide();
    }

	// Use this for initialization
	void Start () {
	
	}

    public void Show(float val) {
        label.text = ((int)val).ToString();
        rt.DOScale(1, tweenSpeed).SetEase(Ease.OutBounce);
        cg.DOFade(1, tweenSpeed).OnComplete(OnShowComplete);
    }

    private void OnShowComplete() {
        float to = rt.anchoredPosition.y + 50;
        cg.DOFade(0, tweenSpeed);
        rt.DOAnchorPosY(to, tweenSpeed).SetEase(Ease.Linear).OnComplete(OnScrollUpComplete);
    }

    private void OnScrollUpComplete() {
        Hide();
    }

    private void Hide() {
        cg.alpha = 0;
        rt.localScale = Vector3.zero;
        rt.anchoredPosition = rt.anchoredPosition.SetY(startY);
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
