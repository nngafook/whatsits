﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using UnityEngine.UI;

public class BottomMenu : MonoBehaviour {

    public GameDesigner gameDesigner;
    public NewQuestionSetWindow newQuestionSetWindow;
    public DefaultsWindow defaultsWindow;

    public Button emailButton;
    public Button deleteSetButton;

    void Awake() {
        
    }

    public void OnNewQuestionSetPressed() {
        newQuestionSetWindow.ShowNewQuestionSetContent();
        newQuestionSetWindow.CG().SetAlphaAndBools(true);
    }

    public void OnLoadQuestionSetPressed() {
        newQuestionSetWindow.ShowLoadQuestionSetContent();
        newQuestionSetWindow.CG().SetAlphaAndBools(true);
    }

    public void OnDeleteQuestionSetPressed() {
        ConfirmWindow.Open("Confirm Question Set Delete?", OnDeleteQuestionSetConfirm);
    }

    private void OnDeleteQuestionSetConfirm(bool value) {
        if (value) {
            gameDesigner.DeleteQuestionSet();
        }
    }

    public void OnSetDefaultsPressed() {
        defaultsWindow.Open();
    }

    public void OnEmailQuestionSetPressed() {
        /*        
        #if !UNITY_EDITOR
            gameDesigner.OnEmailSetsPressed();
        #else
            gameDesigner.CopySetsToProject();
        #endif    
        */

        gameDesigner.EmailLoadedSet();
    }

    public void OnQuitPressed() {
        gameDesigner.BackToTitle();
    }

    public void OnQuestionSetLoaded(bool value) {
        emailButton.interactable = value;
        deleteSetButton.interactable = value;
    }

    // Update is called once per frame
    void Update() {

    }
}
