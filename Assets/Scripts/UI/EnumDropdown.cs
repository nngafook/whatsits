﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;

public class EnumDropdown : MonoBehaviour {

    public Dropdown dropdownList;
    public Utility.EnumType enumType;

    public int Value { get { return dropdownList.value; } }

    void Awake() {
        // This might need to go awaye, and not happen automatically. Might have to be called from outside...
        UpdateDropdownOptions();
    }

    public void UpdateDropdownOptions<T>(Enum e) {
        enumType = (Utility.EnumType)(e);
        string[] names = Enum.GetNames(typeof(T));
        dropdownList.options.Clear();
        for (int i = 0; i < names.Length; i++) {
            dropdownList.options.Add(new UnityEngine.UI.Dropdown.OptionData(names[i]));
        }

        dropdownList.value = Convert.ToInt32(e);
        dropdownList.RefreshShownValue();
    }

    public void UpdateDropdownOptions() {
        string[] names = Enum.GetNames(Utility.EnumFromString<Type>(enumType));
        dropdownList.options.Clear();
        for (int i = 0; i < names.Length; i++) {
            dropdownList.options.Add(new UnityEngine.UI.Dropdown.OptionData(names[i]));
        }

        dropdownList.value = 0;
        dropdownList.RefreshShownValue();
    }

    void Start() {
        
    }

}
