﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class PlaytestOptionsWindow : Popup {

    public QuestionSetLobby lobby;
    public AdWarningIcon adWarningIcon;
    public Text currentCurrencyLabel;

    public override void Awake() {
        base.Awake();
    }

    public override void Start() {
        base.Start();
    }

    public override void Close() {
        SFXManager.Instance.PlaySFX(SFX_TYPE.ButtonClickToony);
        base.Close();
    }

    public void ResetAllTutorials() {
        SFXManager.Instance.PlaySFX(SFX_TYPE.ButtonClickToony);
        PlayerPrefs.SetInt("SeenWelcomeTutorial", 0);
        PlayerPrefs.SetInt("OpenedAnswerSheet", 0);
        PlayerPrefs.SetInt("UsedRecapCycleButtons", 0);
        PlayerPrefs.SetInt("UsedRecap", 0);
    }

    public void ResetGameProgress() {
        SFXManager.Instance.PlaySFX(SFX_TYPE.ButtonClickToony);
        Lazarus.Instance.DebugResetAllProgress();
        ResetAllTutorials();
        SetCurrencyToZero();
        ResetPlaysTillAd();
        ResetVoluntaryAdCount();
    }

    public void SetCurrencyToZero() {
        SFXManager.Instance.PlaySFX(SFX_TYPE.ButtonClickToony);
        Lazarus.Instance.SetPlayerDetail(0, PlayerDetail.Currency);
        UpdateCurrentCurrencyLabel();
    }

    public void AddOneHundredCurrency() {
        SFXManager.Instance.PlaySFX(SFX_TYPE.ButtonClickToony);
        Lazarus.Instance.UpdatePlayerDetails(100, PlayerDetail.Currency);
        UpdateCurrentCurrencyLabel();
    }

    private void UpdateCurrentCurrencyLabel() {
        SFXManager.Instance.PlaySFX(SFX_TYPE.ButtonClickToony);
        currentCurrencyLabel.text = Lazarus.Instance.GetPlayerDetail(PlayerDetail.Currency).ToString();
        lobby.UpdateCurrencyLabel();
    }

    private void ResetPlaysTillAd() {
        Lazarus.Instance.SetPlayerDetail(5, PlayerDetail.AdShowPlayCount);
        adWarningIcon.UpdatePlaysRemaining();
    }

    private void ResetVoluntaryAdCount() {
        Lazarus.Instance.SetPlayerDetail(0, PlayerDetail.VoluntaryAdCount);
    }

}
