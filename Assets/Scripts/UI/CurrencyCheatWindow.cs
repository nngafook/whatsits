﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;
using Opencoding.CommandHandlerSystem;

public class CurrencyCheatWindow : MonoBehaviour {

    private bool isOpen = false;

    private CanvasGroup mainCG;

    public QuestionSetLobby lobby;
    public Text currentCurrencyLabel;
    public InputField valueInputField;

    void Awake() {
        mainCG = this.GetComponent<CanvasGroup>();
        CommandHandlers.RegisterCommandHandlers(this);
    }

    public void OnDestroy() {
        CommandHandlers.UnregisterCommandHandlers(this);
    }

    // Use this for initialization
    void Start() {

    }

    private void UpdateCurrentCurrencyLabel() {
        currentCurrencyLabel.text = Lazarus.Instance.GetPlayerDetail(PlayerDetail.Currency).ToString();
        lobby.UpdateCurrencyLabel();
    }

    private void OnClearQuestionSetsConfirmed(bool value) {
        if (value) {
            QuestionSetProgression.Instance.ownedQuestionSets.Clear();
            QuestionSetProgression.Instance.Save();
        }
    }

    public void Toggle() {
        if (isOpen) {
            Close();
        }
        else {
            Open();
        }
    }

    [CommandHandler]
    public void Open() {
        isOpen = true;
        mainCG.SetAlphaAndBools(isOpen);
        UpdateCurrentCurrencyLabel();
    }

    public void Close() {
        isOpen = false;
        mainCG.SetAlphaAndBools(isOpen);
    }

    public void OnToZeroPressed() {
        Lazarus.Instance.SetPlayerDetail(0, PlayerDetail.Currency);
        UpdateCurrentCurrencyLabel();
    }

    public void OnAddPressed() {
        int value = 1000;
        if (!string.IsNullOrEmpty(valueInputField.text)) {
            Int32.TryParse(valueInputField.text, out value);
        }

        Lazarus.Instance.UpdatePlayerDetails(value, PlayerDetail.Currency);
        UpdateCurrentCurrencyLabel();
    }

    public void OnSubtractPressed() {
        int value = 100;
        
        if (!string.IsNullOrEmpty(valueInputField.text)) {
            Int32.TryParse(valueInputField.text, out value);
        }

        Lazarus.Instance.UpdatePlayerDetails(-value, PlayerDetail.Currency);
        UpdateCurrentCurrencyLabel();
    }

    public void ClearQuestionSets() {
        ConfirmWindow.Open("Clear Owned Question Sets?", OnClearQuestionSetsConfirmed);
    }

    // Update is called once per frame
    void Update() {
        if (Input.GetKey(KeyCode.LeftControl)) {
            if (Input.GetKeyUp(KeyCode.L)) {
                ClearQuestionSets();
            }
        }
    }
}
