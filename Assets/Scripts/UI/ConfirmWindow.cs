﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;

public class ConfirmWindow : MonoBehaviour {

    private static ConfirmWindow instance;

    private CanvasGroup cg;

    public Action<bool> AnswerResponseEvent;

    public Text confirmMessageLabel;

    void Awake() {
        instance = this;
        cg = this.GetComponent<CanvasGroup>();
    }

    public static void Open(string message, Action<bool> callback) {
        instance.confirmMessageLabel.text = message;
        instance.AnswerResponseEvent = callback;

        instance.cg.SetAlphaAndBools(true);
        instance.RT().SetAsLastSibling();
    }

    public void AnswerMade(bool value) {
        SFXManager.Instance.PlaySFX(SFX_TYPE.ButtonClickToony);
        if (AnswerResponseEvent != null) {
            AnswerResponseEvent(value);
        }
        instance.cg.SetAlphaAndBools(false);
    }

    // Update is called once per frame
    void Update() {

    }
}
