﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.IO;

public class ViewQuestionWindow : MonoBehaviour {

    private int loadedQuestionIndex = -1;

    private QuestionSetData loadedQuestionSetData;
    private Question loadedQuestion;

    private List<EnumListItem> subCategoriesList = new List<EnumListItem>();
    private List<EnumListItem> answerCategoriesList = new List<EnumListItem>();
    private List<LabeledInputField> labelOptionsList = new List<LabeledInputField>();

    public GameObject enumListItemPrefab;
    public GameObject inputFieldListItemPrefab;

    [Space(10)]
    public ScrollRect scrollRect;
    public GameDesigner gameDesigner;

    [Space(10)]
    public InputField questionInputField;
    public Text uidLabel;
    public EnumDropdown questionTypeDropdown;
    public EnumDropdown questionCategoryDropdown;

    public EnumDropdown answerCategoryDropdown;
    public InputField correctAnswerInputField;
    public EnumDropdown correctLabelColorDropdown;
    public EnumDropdown correctButtonColorDropdown;

    public Transform answerCategoryListItemParent;
    public Transform subCategoryListItemParent;
    public Transform labelOptionListItemParent;


    void Start() {
        
    }

    private void AddSubcategory(QuestionCategory cat) {
        EnumListItem listItem = (Instantiate(enumListItemPrefab, Vector3.zero, Quaternion.identity) as GameObject).GetComponent<EnumListItem>();
        listItem.transform.SetParent(subCategoryListItemParent, false);
        subCategoriesList.Add(listItem);

        listItem.UpdateEnumType<QuestionCategory>(cat);
        listItem.SetListAndIndex(ref subCategoriesList, subCategoriesList.Count - 1);
    }

    private void AddAnswerCategory(AnswerCategory cat) {
        EnumListItem listItem = (Instantiate(enumListItemPrefab, Vector3.zero, Quaternion.identity) as GameObject).GetComponent<EnumListItem>();
        listItem.transform.SetParent(answerCategoryListItemParent, false);
        answerCategoriesList.Add(listItem);

        listItem.UpdateEnumType<AnswerCategory>(cat);
        listItem.SetListAndIndex(ref answerCategoriesList, answerCategoriesList.Count - 1);
    }

    private void AddLabelOption(string text) {
        LabeledInputField listItem = (Instantiate(inputFieldListItemPrefab, Vector3.zero, Quaternion.identity) as GameObject).GetComponent<LabeledInputField>();
        listItem.transform.SetParent(labelOptionListItemParent, false);
        labelOptionsList.Add(listItem);

        listItem.SetNameLabel("Label Option " + labelOptionsList.Count);
        listItem.SetInputValue(text);

        listItem.SetListAndIndex(ref labelOptionsList, labelOptionsList.Count - 1);
    }

    private void ClearLists() {
        for (int i = 0; i < subCategoriesList.Count; i++) {
            Destroy(subCategoriesList[i].gameObject);
        }

        for (int i = 0; i < answerCategoriesList.Count; i++) {
            Destroy(answerCategoriesList[i].gameObject);
        }

        for (int i = 0; i < labelOptionsList.Count; i++) {
            Destroy(labelOptionsList[i].gameObject);
        }

        subCategoriesList.Clear();
        answerCategoriesList.Clear();
        labelOptionsList.Clear();
        scrollRect.normalizedPosition = Vector2.one;
    }

    public void Open() {
        this.CG().SetAlphaAndBools(true);
    }

    public void Close() {
        this.CG().SetAlphaAndBools(false);

        loadedQuestion = null;
        loadedQuestionSetData = null;
        loadedQuestionIndex = -1;
    }

    public void SetData(ref QuestionSetData data, int index) {
        loadedQuestionSetData = data;
        loadedQuestion = data.questions[index];
        loadedQuestionIndex = index;

        LoadQuestion();
    }

    //public void SetDataForNewQuestion(ref QuestionSetData data, Question q) {
    //    loadedQuestionSetData = data;
    //    loadedQuestion = q;

    //    loadedQuestionIndex = -1;
    //    LoadQuestion();
    //}

    private void LoadQuestion() {
        ClearLists();

        questionInputField.text = loadedQuestion.QuestionText;
        uidLabel.text = loadedQuestion.uid;
        questionTypeDropdown.UpdateDropdownOptions<QuestionType>(loadedQuestion.questionType);
        questionCategoryDropdown.UpdateDropdownOptions<QuestionCategory>(loadedQuestion.questionCategory);
        correctButtonColorDropdown.UpdateDropdownOptions<ColorAnswerValue>(loadedQuestion.correctButtonColorAnswer);
        correctLabelColorDropdown.UpdateDropdownOptions<ColorAnswerValue>(loadedQuestion.correctLabelColorAnswer);
        correctAnswerInputField.text = loadedQuestion.correctLabelAnswer;

        for (int i = 0; i < loadedQuestion.subCategories.Count; i++) {
            AddSubcategory(loadedQuestion.subCategories[i]);
        }

        for (int i = 0; i < loadedQuestion.answerCategories.Count; i++) {
            AddAnswerCategory(loadedQuestion.answerCategories[i]);
        }

        for (int i = 0; i < loadedQuestion.buttonLabelOptions.Count; i++) {
            AddLabelOption(loadedQuestion.buttonLabelOptions[i]);
        }

    }

    public void OnAddSubcategoryButtonPressed() {
        AddSubcategory(QuestionCategory.Uncategorized);
    }

    public void OnAddAnswerCategoryButtonPressed() {
        AddAnswerCategory(AnswerCategory.Uncategorized);
    }

    public void OnAddLabelOptionButtonPressed() {
        AddLabelOption("");
    }

    public void OnSaveButtonPressed() {
        loadedQuestion.questionText = questionInputField.text;
        loadedQuestion.uid = uidLabel.text;
        loadedQuestion.questionCategory = (QuestionCategory)questionCategoryDropdown.Value;
        loadedQuestion.questionType = (QuestionType)questionTypeDropdown.Value;

        loadedQuestion.subCategories.Clear();
        for (int i = 0; i < subCategoriesList.Count; i++) {
            loadedQuestion.subCategories.Add((QuestionCategory)subCategoriesList[i].Value);
        }

        loadedQuestion.correctLabelAnswer = correctAnswerInputField.text;
        loadedQuestion.correctButtonColorAnswer = (ColorAnswerValue)correctButtonColorDropdown.Value;
        loadedQuestion.correctLabelColorAnswer = (ColorAnswerValue)correctLabelColorDropdown.Value;

        //Debug.Log("Looking for old AnswerCateogry and not the list of answer categories");
        //loadedQuestion.answerCategories = (AnswerCategory)answerCategoryDropdown.Value;

        loadedQuestion.answerCategories.Clear();
        for (int i = 0; i < answerCategoriesList.Count; i++) {
            loadedQuestion.answerCategories.Add((AnswerCategory)answerCategoriesList[i].Value);
        }

        loadedQuestion.buttonLabelOptions.Clear();
        for (int i = 0; i < labelOptionsList.Count; i++) {
            loadedQuestion.buttonLabelOptions.Add(labelOptionsList[i].Text);
        }

        loadedQuestionSetData.questions[loadedQuestionIndex] = loadedQuestion;

        #if UNITY_EDITOR
            // If the loaded question set is from the project
            if (gameDesigner.UsingProjectQuestionSets) {
                Utility.QuestionSetToJson(loadedQuestionSetData, loadedQuestionSetData.name, System.IO.Path.Combine(Application.dataPath, "QuestionSetJSONFiles"));
                UnityEditor.AssetDatabase.Refresh();
            }
            else {
                Utility.QuestionSetToJson(loadedQuestionSetData, loadedQuestionSetData.name);
            }
        #else
            Utility.QuestionSetToJson(loadedQuestionSetData, loadedQuestionSetData.name);
        #endif

        gameDesigner.QuestionSaved();
    }

    public void OnDeleteButtonPressed() {
        ConfirmWindow.Open("Confirm Delete Question?", OnDeleteConfirmed);
    }

    private void OnDeleteConfirmed(bool value) {
        if (value) {
            loadedQuestionSetData.questions.RemoveAt(loadedQuestionIndex);

            #if UNITY_EDITOR
                // If the loaded question set is from the project
                if (gameDesigner.UsingProjectQuestionSets) {
                    Utility.QuestionSetToJson(loadedQuestionSetData, loadedQuestionSetData.name, System.IO.Path.Combine(Application.dataPath, "QuestionSetJSONFiles"));
                    UnityEditor.AssetDatabase.Refresh();
                }
                else {
                    Utility.QuestionSetToJson(loadedQuestionSetData, loadedQuestionSetData.name);
                }
            #else
                Utility.QuestionSetToJson(loadedQuestionSetData, loadedQuestionSetData.name);
            #endif

                //Utility.QuestionSetToJson(loadedQuestionSetData, loadedQuestionSetData.name);
            gameDesigner.QuestionDeleted();
        }
    }

    // Update is called once per frame
    void Update() {

    }
}
