﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class LevelStartWindow : MonoBehaviour {

    private CanvasGroup mainCG;
    private RuleSetData currentGameRuleSetData;

    public AlexTrebek alexTrebek;
    public Text rulesLabel;

    void Awake() {
        mainCG = this.GetComponent<CanvasGroup>();

        mainCG.SetAlphaAndBools(false);
    }

    // Use this for initialization
    void Start() {

    }

    public void Open(string ruleString, RuleSetData ruleSetData) {
        rulesLabel.text = ruleString;
        currentGameRuleSetData = ruleSetData;
        mainCG.SetAlphaAndBools(true);
    }

    public void OnOKPressed() {
        SFXManager.Instance.PlaySFX(SFX_TYPE.ButtonClickToony);
        alexTrebek.BeginLevel();
        mainCG.SetAlphaAndBools(false);
    }

    public void OnHelpPressed() {
        TutorialStage.Instance.SetGameRule(currentGameRuleSetData, rulesLabel.text);
        TutorialStage.Instance.Open();
    }

    // Update is called once per frame
    void Update() {

    }
}
