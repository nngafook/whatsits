﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using TextFx;
using DG.Tweening;
using UnityEngine.Events;

public class RewardPopup : MonoBehaviour {

    private static RewardPopup instance;

    [HideInInspector]
    public static UnityEvent OnClosedEvent = new UnityEvent();

    private int rewardAmount = 0;
    private bool isAnimating = false;
    private bool isOpen = false;

    private CanvasGroup mainCG;
    private CanvasGroup giftBoxCG;
    private RectTransform questionMarkIconRT;

    public Text titleLabel;
    public ParticleSystem questionMarkVFX;
    public TextFxUGUI rewardAmountLabel;
    public RectTransform giftBoxRT;
    public CanvasGroup questionMarkIconCG;
    public CanvasGroup okButtonCG;

    public static bool IsOpen { get { return instance.isOpen; } }

    void Awake() {
        instance = this;
        mainCG = this.CG();
        giftBoxCG = giftBoxRT.CG();
        questionMarkIconRT = questionMarkIconCG.RT();

        Reset();
    }

    // Use this for initialization
    void Start() {

    }

    private void Reset() {
        titleLabel.text = "REWARD";
        rewardAmount = 0;
        giftBoxCG.SetAlphaAndBools(true);
        questionMarkIconCG.alpha = 0;
        questionMarkIconRT.localScale = Vector3.zero;
        rewardAmountLabel.SetText("");
        okButtonCG.SetAlphaAndBools(false);
        this.RT().SetAsLastSibling();
    }

    public void OnGiftBoxPressed() {
        if (!isAnimating) {
            isAnimating = true;
            giftBoxRT.DOShakeAnchorPos(1f, strength: 10).SetEase(Ease.Linear).OnComplete(OnGiftShakeComplete);
        }
    }

    private void OnGiftShakeComplete() {
        OpenGift();
    }

    private void OpenGift() {
        giftBoxCG.DOFade(0, 0.05f).OnComplete(OnGiftBoxFadeOutComplete);
        questionMarkIconCG.DOFade(1, 0.25f);
        questionMarkIconRT.DOScale(1, 0.25f).SetEase(Ease.OutBack).OnComplete(OnQuestionMarkIconScaleComplete);
        questionMarkVFX.Play();
        SFXManager.Instance.PlaySFX(SFX_TYPE.CurrencyShake, clipVolume:2f);
        SFXManager.Instance.PlaySFX(SFX_TYPE.ClimbingBells, clipVolume: 2f);
        SFXManager.Instance.PlaySFX(SFX_TYPE.FireworksSparkle, clipVolume: 2f);
        int rewardAmount = GetRandomRewardAmount();

        Lazarus.Instance.UpdatePlayerDetails(rewardAmount, PlayerDetail.Currency);
        rewardAmountLabel.SetText(rewardAmount.ToString());
    }

    /// <summary>
    /// If the rewardAmount is set from Open(), then that value is rewarded, else a random value is rewarded
    /// </summary>
    /// <returns></returns>
    private int GetRandomRewardAmount() {
        int rVal = rewardAmount;

        if (rewardAmount == 0) {
            int factorMultiple = Random.Range(5, 11);
            int factor = 100;

            if (factorMultiple < 8) {
                PercentageFloat percentage = new PercentageFloat((float)Lazarus.Instance.GetPlayerDetail(PlayerDetail.AdRewardLowCount) * 10);
                if (percentage.Success) {
                    Lazarus.Instance.SetPlayerDetail(0, PlayerDetail.AdRewardLowCount);
                    factorMultiple = Random.Range(8, 11);
                }
                else {
                    Lazarus.Instance.UpdatePlayerDetails(1, PlayerDetail.AdRewardLowCount);
                }
            }
            else {
                Lazarus.Instance.SetPlayerDetail(0, PlayerDetail.AdRewardLowCount);
            }

            rVal = (factorMultiple * factor);
        }

        return rVal;
    }

    private void OnGiftBoxFadeOutComplete() {
        giftBoxCG.SetAlphaAndBools(false);
    }

    private void OnQuestionMarkIconScaleComplete() {
        rewardAmountLabel.AnimationManager.ResetAnimation();
        rewardAmountLabel.AnimationManager.PlayAnimation();
        okButtonCG.SetAlphaAndBools(true);
    }

    public void OnOkayButtonPressed() {
        SFXManager.Instance.PlaySFX(SFX_TYPE.ButtonClickToony);
        isAnimating = false;
        Close();
    }

    public static void Open() {
        instance.Reset();
        instance.isOpen = true;
        instance.mainCG.SetAlphaAndBools(true);
    }

    public static void Open(string title, int amt) {
        instance.Reset();
        instance.titleLabel.text = title;
        instance.rewardAmount = amt;
        instance.isOpen = true;
        instance.mainCG.SetAlphaAndBools(true);
    }

    private void Close() {
        isOpen = false;
        mainCG.SetAlphaAndBools(false);
        if (OnClosedEvent != null) {
            OnClosedEvent.Invoke();
        }
    }

    // Update is called once per frame
    void Update() {

    }
}
