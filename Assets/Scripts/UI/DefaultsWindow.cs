﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System;

public class DefaultsWindow : MonoBehaviour {

    private List<EnumListItem> subcategoryItems = new List<EnumListItem>();
    private List<EnumListItem> answerCategoryItems = new List<EnumListItem>();

    private QuestionSetDefaults defaultData;

    public Dropdown questionTypeDropdown;
    public Dropdown questionCategoryDropdown;
    public Dropdown answerCategoryDropdown;
    public Dropdown correctLabelColorDropdown;
    public Dropdown correctButtonColorDropdown;

    [Space(10)]
    public GameObject enumListItemPrefab;
    public Transform subcategoryParent;
    public Transform answerCategoryParent;

    void Awake() {
        defaultData = QuestionSetDefaults.Load();
    }

    // Use this for initialization
    void Start() {
        SetValues();
    }

    private void SetValues() {
        for (int i = 0; i < subcategoryItems.Count; i++) {
            Destroy(subcategoryItems[i].gameObject);
        }
        subcategoryItems = new List<EnumListItem>();

        for (int i = 0; i < answerCategoryItems.Count; i++) {
            Destroy(answerCategoryItems[i].gameObject);
        }
        answerCategoryItems = new List<EnumListItem>();

        questionTypeDropdown.value = (int)defaultData.questionType;
        questionTypeDropdown.RefreshShownValue();

        questionCategoryDropdown.value = (int)defaultData.questionCategory;
        questionCategoryDropdown.RefreshShownValue();

        //answerCategoryDropdown.value = (int)defaultData.answerCategories;
        //answerCategoryDropdown.RefreshShownValue();

        correctLabelColorDropdown.value = (int)defaultData.correctLabelColor;
        correctLabelColorDropdown.RefreshShownValue();

        correctButtonColorDropdown.value = (int)defaultData.correctButtonColor;
        correctButtonColorDropdown.RefreshShownValue();

        for (int i = 0; i < defaultData.subCategories.Count; i++) {
            AddSubcategory(defaultData.subCategories[i]);
        }

        for (int i = 0; i < defaultData.answerCategories.Count; i++) {
            AddAnswerCategory(defaultData.answerCategories[i]);
        }
    }

    private void AddSubcategory(QuestionCategory cat) {
        EnumListItem listItem = (Instantiate(enumListItemPrefab, Vector3.zero, Quaternion.identity) as GameObject).GetComponent<EnumListItem>();
        listItem.transform.SetParent(subcategoryParent, false);
        subcategoryItems.Add(listItem);

        listItem.UpdateEnumType<QuestionCategory>(cat);
        listItem.SetListAndIndex(ref subcategoryItems, subcategoryItems.Count - 1);
    }

    public void OnAddSubcategoryButtonPressed() {
        AddSubcategory(QuestionCategory.Uncategorized);
    }

    public void OnAddAnswerCategoryButtonPressed() {
        AddAnswerCategory(AnswerCategory.Uncategorized);
    }

    private void AddAnswerCategory(AnswerCategory cat) {
        EnumListItem listItem = (Instantiate(enumListItemPrefab, Vector3.zero, Quaternion.identity) as GameObject).GetComponent<EnumListItem>();
        listItem.transform.SetParent(answerCategoryParent, false);
        answerCategoryItems.Add(listItem);

        listItem.UpdateEnumType<AnswerCategory>(cat);
        listItem.SetListAndIndex(ref answerCategoryItems, answerCategoryItems.Count - 1);
    }

    public void OnSaveButtonPressed() {
        defaultData.questionType = (QuestionType)questionTypeDropdown.value;
        defaultData.questionCategory = (QuestionCategory)questionCategoryDropdown.value;
        defaultData.correctLabelColor = (ColorAnswerValue)correctLabelColorDropdown.value;
        defaultData.correctButtonColor = (ColorAnswerValue)correctButtonColorDropdown.value;

        defaultData.subCategories.Clear();
        for (int i = 0; i < subcategoryItems.Count; i++) {
            defaultData.subCategories.Add((QuestionCategory)subcategoryItems[i].Value);
        }

        defaultData.answerCategories.Clear();
        for (int i = 0; i < answerCategoryItems.Count; i++) {
            defaultData.answerCategories.Add((AnswerCategory)answerCategoryItems[i].Value);
        }

        defaultData.Save();
        Close();
    }

    public void OnCancelButtonPressed() {
        Close();
    }

    public void Open() {
        this.CG().SetAlphaAndBools(true);
    }

    public void Close() {
        this.CG().SetAlphaAndBools(false);
    }

    // Update is called once per frame
    void Update() {

    }
}

[Serializable]
public class QuestionSetDefaults {

    private static QuestionSetDefaults instance = null;

    public static QuestionSetDefaults Instance { get { return instance; } }

    private string DataFilePath {
        get {
            string path = System.IO.Path.Combine(Application.persistentDataPath, "QuestionSetDefaults.json");
            return path;
        }
    }

    public List<QuestionCategory> subCategories = new List<QuestionCategory>();
    public List<AnswerCategory> answerCategories = new List<AnswerCategory>();

    public QuestionType questionType = QuestionType.Uncategorized;
    public QuestionCategory questionCategory = QuestionCategory.Uncategorized;
    public ColorAnswerValue correctLabelColor = ColorAnswerValue.Null;
    public ColorAnswerValue correctButtonColor = ColorAnswerValue.Null;

    public static QuestionSetDefaults Load() {
        instance = new QuestionSetDefaults();

        string path = instance.DataFilePath;

        if (System.IO.File.Exists(path)) {
            instance = JsonUtility.FromJson<QuestionSetDefaults>(System.IO.File.ReadAllText(path));
        }
        else {
            instance.Save();
        }

        return instance;
    }

    public void Save() {
        string path = instance.DataFilePath;
        Debug.LogFormat(("Saving Question Set Defaults at: {0}").Colored(CustomColor.GetColor(ColorName.YALLOW)), path);
        System.IO.File.WriteAllText(path, JsonUtility.ToJson(instance, true));
    }

}