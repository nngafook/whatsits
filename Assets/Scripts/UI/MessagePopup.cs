﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using UnityEngine.UI;

[RequireComponent(typeof(CanvasGroup))]
public class MessagePopup : MonoBehaviour {

    private static MessagePopup instance;

    private CanvasGroup cg;

    public Action OkayPressedEvent;

    public Text messageLabel;

    void Awake() {
        instance = this;
        cg = this.GetComponent<CanvasGroup>();
    }

    public static void Open(string message, Action callback) {
        instance.messageLabel.text = message;
        instance.OkayPressedEvent = callback;

        instance.cg.SetAlphaAndBools(true);
        instance.RT().SetAsLastSibling();
    }

    public static void Open(string message) {
        instance.messageLabel.text = message;
        instance.OkayPressedEvent = null;

        instance.cg.SetAlphaAndBools(true);
        instance.RT().SetAsLastSibling();
    }

    public void OnOkayPressed() {
        SFXManager.Instance.PlaySFX(SFX_TYPE.ButtonClickToony);
        if (OkayPressedEvent != null) {
            OkayPressedEvent();
        }
        instance.cg.SetAlphaAndBools(false);
    }
}
