﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Opencoding.CommandHandlerSystem;

public class AdWarningIcon : MonoBehaviour {

    private int playsRemaining = 0;

    public Text counterLabel;

    void Awake() {
        CommandHandlers.RegisterCommandHandlers(this);
    }

    public void OnDestroy() {
        CommandHandlers.UnregisterCommandHandlers(this);
    }

	// Use this for initialization
	void Start () {
	
	}

    private void UpdateLabel() {
        counterLabel.text = "IN\n" + playsRemaining.ToString();
    }

    [CommandHandler]
    private void DebugTestPlayCountComplete(int value) {
        Lazarus.Instance.SetPlayerDetail(value, PlayerDetail.AdShowPlayCount);
        UpdatePlaysRemaining();
    }

    public void UpdatePlaysRemaining() {
        playsRemaining = Lazarus.Instance.GetPlayerDetail(PlayerDetail.AdShowPlayCount).Value;

        if (playsRemaining == 0) {
            if (Lazarus.Instance.ShowAd()) {
                Lazarus.Instance.SetPlayerDetail(5, PlayerDetail.AdShowPlayCount);
                playsRemaining = Lazarus.Instance.GetPlayerDetail(PlayerDetail.AdShowPlayCount).Value;
            }
        }

        UpdateLabel();
    }

	// Update is called once per frame
	void Update () {

	}
}
