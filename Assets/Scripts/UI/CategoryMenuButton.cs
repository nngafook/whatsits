﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.Events;
using System;

[Serializable]
public class CategoryMenuButtonEvent : UnityEvent<CategoryMenuButton> { }

public class CategoryMenuButton : MonoBehaviour {

    public QuestionCategory category;

    [Space(10)]
    public Text headerLabel;
    public Text descriptionLabel;

    [Space(10)]
    public CategoryMenuButtonEvent OnButtonPressEvent;


    void Awake() {
        SetTexts(category);
    }

    // Use this for initialization
    void Start() {

    }

    public void OnButtonPressed() {
        if (OnButtonPressEvent != null) {
            OnButtonPressEvent.Invoke(this);
        }
    }

    public void SetTexts(QuestionCategory c) {
        headerLabel.text = TextUtil.QuestionCategoryTitle(c);
        descriptionLabel.text = TextUtil.QuestionCategoryDescription(c);
    }

    // Update is called once per frame
    void Update() {

    }
}
