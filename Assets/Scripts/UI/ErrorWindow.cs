﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class ErrorWindow : MonoBehaviour {

    private QuestionType selectedAnswerType;

    [Header("Answer Type")]
    public Dropdown answerTypeDropdown;

    [Header("Question Set")]
    public Text questionSetLabel;
    public Text questionSetErrorMessageLabel;

    [Header("Question")]
    public Text questionLabel;
    public Text questionErrorMessageLabel;

    [Header("Correct Answer Label")]
    public Text correctAnswerLabel;
    public Text correctAnwerErrorMessageLabel;

    public bool HasErrors {
        get {
            return (questionSetErrorMessageLabel.gameObject.activeSelf || questionErrorMessageLabel.gameObject.activeSelf || correctAnwerErrorMessageLabel.gameObject.activeSelf);
        }
    }

    void Awake() {
        answerTypeDropdown.onValueChanged.AddListener(OnAnswerTypeDropdownChanged);

        selectedAnswerType = (QuestionType)answerTypeDropdown.value;
    }

    // Use this for initialization
    void Start() {

    }

    private void OnAnswerTypeDropdownChanged(int value) {
        selectedAnswerType = (QuestionType)value;
        switch (selectedAnswerType) {
            case QuestionType.Label:
                correctAnwerErrorMessageLabel.gameObject.SetActive(true);
                break;
            case QuestionType.ButtonColor:
            case QuestionType.LabelColor:
            case QuestionType.Uncategorized:
            default:
                correctAnwerErrorMessageLabel.gameObject.SetActive(false);
                break;
        }
    }

    private void CheckQuestionField() {
        if (!questionErrorMessageLabel.gameObject.activeSelf && string.IsNullOrEmpty(questionLabel.text)) {
            questionErrorMessageLabel.gameObject.SetActive(true);
        }
        else if (questionErrorMessageLabel.gameObject.activeSelf && !string.IsNullOrEmpty(questionLabel.text)) {
            questionErrorMessageLabel.gameObject.SetActive(false);
        }
    }

    private void CheckQuestionSetField() {
        if (!questionSetErrorMessageLabel.gameObject.activeSelf && string.IsNullOrEmpty(questionSetLabel.text)) {
            questionSetErrorMessageLabel.gameObject.SetActive(true);
        }
        else if (questionSetErrorMessageLabel.gameObject.activeSelf && !string.IsNullOrEmpty(questionSetLabel.text)) {
            questionSetErrorMessageLabel.gameObject.SetActive(false);
        }
    }

    private void CheckCorrectAnswerField() {
        switch (selectedAnswerType) {
            case QuestionType.Label:
                if (!correctAnwerErrorMessageLabel.gameObject.activeSelf && string.IsNullOrEmpty(correctAnswerLabel.text)) {
                    correctAnwerErrorMessageLabel.gameObject.SetActive(true);
                }
                else if (correctAnwerErrorMessageLabel.gameObject.activeSelf && !string.IsNullOrEmpty(correctAnswerLabel.text)) {
                    correctAnwerErrorMessageLabel.gameObject.SetActive(false);
                }
                break;
            case QuestionType.ButtonColor:
            case QuestionType.LabelColor:
            case QuestionType.Uncategorized:
            default:
                break;
        }
    }

    // Update is called once per frame
    void Update() {
        CheckQuestionField();
        CheckQuestionSetField();
        CheckCorrectAnswerField();
    }

}
