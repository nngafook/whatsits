﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;
using System.Text;

public class TutorialStage : MonoBehaviour {

    private static TutorialStage instance = null;
    public static TutorialStage Instance { get { return instance; } }

    private string welcomeTutorialPath = "TutorialSequences/WelcomeTutorial";
    private string randomWrongPath = "TutorialSequences/RandomWrong";
    private string modularWrongPath = "TutorialSequences/ModularWrong";
    private string wrongIfAnswerPath = "TutorialSequences/WrongIfAnswer";
    private string correctPlusPath = "TutorialSequences/CorrectAnswerPlus";
    private string correctMinusPath = "TutorialSequences/CorrectAnswerMinus";
    private string correctPlusOnePath = "TutorialSequences/CorrectAnswerPlusOne";
    private string correctMinusOnePath = "TutorialSequences/CorrectAnswerMinusOne";

    private int tipIndex = 0;
    private int focusedTargetSiblingIndex = -1;
    private float transitionSpeed = 0.5f;

    // The rule args string for formatting (ex: {0, 1 and 2} )
    private string[] ruleArgs = new string[] { "" };

    private RuleSetData currentRuleSetData;

    private CanvasGroup arrowCG;
    private RectTransform messageBoxRT;

    private RectTransform focusedTarget;
    private Transform movedTargetParent;
    private RectTransform mainRT;
    private CanvasGroup mainCG;
    private CanvasGroup rulesWindowCG;

    public CanvasGroup bgCG;
    public CanvasGroup blackFocusOverlayCG;
    public CanvasGroup messageBoxCG;
    public CanvasGroup answerButtonsCG;
    public CanvasGroup questionCG;
    public CanvasGroup levelTimeCG;
    public CanvasGroup questionTimeCG;

    [Space(10)]
    public Text tutorialMessageLabel;
    public Text questionTextLabel;
    public RectTransform arrowRT;
    public RectTransform proceedClickArea;
    public CanvasGroup transitionOverlay;
    public List<AnswerButton> answerButtons;

    [Space(10)]
    public Text rulesLabel;

    [Space(10)]
    public RectTransform questionLabel;
    public RectTransform levelTimeLabel;
    public RectTransform questionTimeLabel;
    public RectTransform questionNumberLabel;
    public RectTransform rulesWindow;
    public RectTransform buttonA;
    public RectTransform buttonB;
    public RectTransform buttonC;
    public RectTransform buttonD;

    [Space(10)]
    public TutorialSequence loadedTutorialSequence;

    void Awake() {
        instance = this;
        mainRT = this.RT();
        mainCG = this.CG();
        arrowCG = arrowRT.CG();

        messageBoxRT = messageBoxCG.RT();
        rulesWindowCG = rulesWindow.CG();

        arrowRT.localPosition = Vector3.zero;
        arrowCG.alpha = 0;
        arrowRT.DOScale(1.1f, 0.25f).SetEase(Ease.Linear).SetLoops(-1, LoopType.Yoyo);
        arrowRT.DOPause();

        SetTextButtonsAndBGVisible(true);

        mainCG.SetAlphaAndBools(false);
        messageBoxCG.SetAlphaAndBools(false);
        transitionOverlay.SetAlphaAndBools(true);
        blackFocusOverlayCG.SetAlphaAndBools(false);
        rulesWindowCG.SetAlphaAndBools(false);
        blackFocusOverlayCG.RT().SetAsLastSibling();
        messageBoxRT.SetAsLastSibling();
        arrowRT.SetAsLastSibling();
        proceedClickArea.SetAsLastSibling();
        transitionOverlay.RT().SetAsLastSibling();
    }

    // Use this for initialization
    void Start() {

    }

    private void ResetButtonOrder() {
        for (int i = 0; i < answerButtons.Count; i++) {
            answerButtons[i].SetChoiceValue(i);
        }
    }

    private void ShuffleButtonOrder() {
        List<int> shuffledIndices = new List<int>() { 0, 1, 2, 3 };
        Queue<int> indexQueue = new Queue<int>(Utility.ShuffleList<int>(shuffledIndices, UnityEngine.Random.Range(0, 500)));

        for (int i = 0; i < answerButtons.Count; i++) {
            answerButtons[i].SetChoiceValue(indexQueue.Dequeue());
        }
    }

    private void SetCorrectAnswer(MultipleChoice correctChoice) {
        if (correctChoice != loadedTutorialSequence.correctAnswer) {
            string tempLabel = loadedTutorialSequence.answerStrings[(int)loadedTutorialSequence.correctAnswer];
            answerButtons[(int)loadedTutorialSequence.correctAnswer].SetLabel(loadedTutorialSequence.answerStrings[(int)correctChoice]);
            answerButtons[(int)correctChoice].SetLabel(tempLabel);
        }
    }

    public void Open() {
        if (loadedTutorialSequence != null) {
            tipIndex = 0;
            mainRT.SetAsLastSibling();
            mainCG.SetAlphaAndBools(true);
            transitionOverlay.DOFade(0, transitionSpeed).OnComplete(OnFadeInComplete);
        }
        else {
            Debug.Log("loaded tutorial sequence null");
            Close();
        }
    }

    private void OnFadeInComplete() {
        transitionOverlay.SetAlphaAndBools(false);
        Next();
    }

    private void OnFadeOutComplete() {
        mainCG.SetAlphaAndBools(false);
        transitionOverlay.SetAlphaAndBools(true);
        SetTextButtonsAndBGVisible(true);
        tipIndex = 0;
    }

    public void Close() {
        transitionOverlay.DOFade(1, transitionSpeed).OnComplete(OnFadeOutComplete);
    }

    public void OpenWelcomeTutorial() {
        SetTutorialSequence(welcomeTutorialPath);
        Open();
    }

    public void OpenMessage() {
        tipIndex = 0;
        mainRT.SetAsLastSibling();
        mainCG.SetAlphaAndBools(true);
        transitionOverlay.SetAlphaAndBools(false);
        SetTextButtonsAndBGVisible(false);

        TutorialTarget target = loadedTutorialSequence.targets[tipIndex];
        messageBoxCG.SetAlphaAndBools(true);
        tutorialMessageLabel.text = string.Format(target.message, ruleArgs);

        SetMessageAnchor(target);
        FocusTarget(target);

        tipIndex = 1;
    }

    private void SetTextButtonsAndBGVisible(bool value) {
        bgCG.alpha = value.AsInt();
        answerButtonsCG.alpha = value.AsInt();
        questionCG.alpha = value.AsInt();
        levelTimeCG.alpha = value.AsInt();
        //questionTimeCG.alpha = value.AsInt();
    }

    private void FocusTarget(TutorialTarget target) {
        if (!string.IsNullOrEmpty(target.pointTargetName)) {
            string pointTargetName = string.Format(target.pointTargetName, ruleArgs);
            focusedTarget = (RectTransform)this.GetType().GetField(pointTargetName).GetValue(this);
            if (focusedTarget != null) {
                movedTargetParent = focusedTarget.parent;
                focusedTargetSiblingIndex = focusedTarget.GetSiblingIndex();
                focusedTarget.SetParent(blackFocusOverlayCG.transform, true);
                blackFocusOverlayCG.SetAlphaAndBools(true);
            }
        }
    }

    private void Next() {
        SFXManager.Instance.PlaySFX(SFX_TYPE.ButtonClickToony);
        arrowRT.localPosition = Vector3.zero;
        arrowCG.alpha = 0;
        blackFocusOverlayCG.SetAlphaAndBools(false);
        messageBoxCG.SetAlphaAndBools(false);
        HideRulesWindow();
        HideQuestionNumber();

        if (focusedTarget != null) {
            focusedTarget.SetParent(movedTargetParent, true);
            focusedTarget.SetSiblingIndex(focusedTargetSiblingIndex);
            movedTargetParent = null;
            focusedTarget = null;
        }

        if (tipIndex == loadedTutorialSequence.targets.Count) {
            Close();
            if (loadedTutorialSequence.sequenceName == "WelcomeTutorial") {
                PlayerPrefs.SetInt("SeenWelcomeTutorial", 1);
            }
        }
        else {
            TutorialTarget target = loadedTutorialSequence.targets[tipIndex];
            //if (target.pointTarget != Vector2.zero) {
                if (!string.IsNullOrEmpty(target.message)) {
                    messageBoxCG.SetAlphaAndBools(true);
                    //tutorialMessageLabel.text = target.message;
                    tutorialMessageLabel.text = string.Format(target.message, ruleArgs);
                    tutorialMessageLabel.text = tutorialMessageLabel.text.Replace("\\n", "\n");
                    SetMessageAnchor(target);
                }

                FocusTarget(target);
                
                SetArrowRotation(target.arrowDirection);
                arrowRT.anchoredPosition = target.pointTarget;
                if (target.arrowDirection == TargetPointDirection.Left || target.arrowDirection == TargetPointDirection.Right) {
                    arrowRT.localPosition = arrowRT.localPosition.AddX(target.arrowOffset * ((target.arrowDirection == TargetPointDirection.Left) ? 1 : -1));
                }
                else {
                    arrowRT.localPosition = arrowRT.localPosition.AddY(target.arrowOffset * ((target.arrowDirection == TargetPointDirection.Down) ? 1 : -1));
                }
            //}

            if (!string.IsNullOrEmpty(target.specialMethodCall)) {
                Invoke(target.specialMethodCall, 0);
            }

            tipIndex++;
        }
    }

    private void SetTutorialSequence(string path) {
        loadedTutorialSequence = Resources.Load<TutorialSequence>(path);

        questionTextLabel.text = loadedTutorialSequence.question;
        
        ResetButtonOrder();
        for (int i = 0; i < answerButtons.Count; i++) {
            answerButtons[i].SetLabel(loadedTutorialSequence.answerStrings[i]);
        }

        CheckAnswerOrder();
    }

    private void SetMessageAnchor(TutorialTarget target) {
        if (target.messageAnchor != MessageAnchorPosition.Custom) {
            float x = 0.5f;
            float y = 0.5f;
            float pivotX = 0.5f;
            float pivotY = 0.5f;

            switch (target.messageAnchor) {
                case MessageAnchorPosition.TopLeft:
                case MessageAnchorPosition.Left:
                case MessageAnchorPosition.BottomLeft:
                    x = 0;
                    pivotX = 0;
                    break;
                case MessageAnchorPosition.Top:
                case MessageAnchorPosition.Center:
                case MessageAnchorPosition.Bottom:
                    x = 0.5f;
                    pivotX = 0.5f;
                    break;
                case MessageAnchorPosition.TopRight:
                case MessageAnchorPosition.Right:
                case MessageAnchorPosition.BottomRight:
                    x = 1;
                    pivotX = 1;
                    break;
            }
            switch (target.messageAnchor) {
                case MessageAnchorPosition.TopLeft:
                case MessageAnchorPosition.Top:
                case MessageAnchorPosition.TopRight:
                    y = 1;
                    pivotY = 1;
                    break;
                case MessageAnchorPosition.Left:
                case MessageAnchorPosition.Center:
                case MessageAnchorPosition.Right:
                    y = 0.5f;
                    pivotY = 0.5f;
                    break;
                case MessageAnchorPosition.BottomLeft:
                case MessageAnchorPosition.Bottom:
                case MessageAnchorPosition.BottomRight:
                    y = 0;
                    pivotY = 0;
                    break;
            }
            Vector2 pos = new Vector2(x, y);
            messageBoxRT.anchorMin = pos;
            messageBoxRT.anchorMax = pos;
            messageBoxRT.pivot = new Vector2(pivotX, pivotY);
            messageBoxRT.anchoredPosition = Vector2.zero;
        }
        else{
            Vector2 pos = new Vector2(0.5f, 0.5f);
            messageBoxRT.anchorMin = pos;
            messageBoxRT.anchorMax = pos;
            messageBoxRT.pivot = new Vector2(0.5f, 0.5f);
            messageBoxRT.anchoredPosition = target.messageCustomPos;
        }
    }

    private void SetArrowRotation(TargetPointDirection targetPointDirection) {
        arrowCG.alpha = 1;
        arrowRT.DOPlay();
        switch (targetPointDirection) {
            case TargetPointDirection.Right:
                arrowRT.rotation = Quaternion.Euler(0, 0, 0);
                break;
            case TargetPointDirection.Down:
                arrowRT.rotation = Quaternion.Euler(0, 0, 270);
                break;
            case TargetPointDirection.Left:
                arrowRT.rotation = Quaternion.Euler(0, 0, 180);
                break;
            case TargetPointDirection.Up:
                arrowRT.rotation = Quaternion.Euler(0, 0, 90);
                break;
            case TargetPointDirection.None:
                arrowCG.alpha = 0;
                arrowRT.DOPause();
                break;
            default:
                break;
        }
    }

    #region RULES_WINDOW

    private void CheckAnswerOrder() {
        MultipleChoice correctAnswer = loadedTutorialSequence.correctAnswer;

        // This is null when it's the welcome tutorial
        if (currentRuleSetData != null) {
            switch (currentRuleSetData.gameRule) {
                //case GameRule.RandomWrong:

                //    break;
                //case GameRule.ModularWrong:

                //    break;
                case GameRule.WrongIfAnswer:
                    if (currentRuleSetData.wrongIfAnswerIs != correctAnswer) {
                        correctAnswer = currentRuleSetData.wrongIfAnswerIs;
                    }
                    SetCorrectAnswer(correctAnswer);
                    break;
                //case GameRule.CorrectAnswerPlusOne:
                //    break;
                //case GameRule.CorrectAnswerMinusOne:
                //    break;
                //case GameRule.CorrectAnswerPlus:
                //    break;
                //case GameRule.CorrectAnswerMinus:
                //    break;
                default:

                    break;
            }
        }
    }

    public void SetGameRule(RuleSetData ruleSetData, string ruleString) {
        currentRuleSetData = ruleSetData;
        rulesLabel.text = ruleString;

        CreateRuleArgs();

        switch (ruleSetData.gameRule) {
            case GameRule.RandomWrong:
                SetTutorialSequence(randomWrongPath);
                break;
            case GameRule.ModularWrong:
                SetTutorialSequence(modularWrongPath);
                break;
            case GameRule.WrongIfAnswer:
                SetTutorialSequence(wrongIfAnswerPath);
                break;
            case GameRule.CorrectAnswerPlusOne:
                SetTutorialSequence(correctPlusOnePath);
                break;
            case GameRule.CorrectAnswerMinusOne:
                SetTutorialSequence(correctMinusOnePath);
                break;
            case GameRule.CorrectAnswerPlus:
                SetTutorialSequence(correctPlusPath);
                break;
            case GameRule.CorrectAnswerMinus:
                SetTutorialSequence(correctMinusPath);
                break;
            default:
                SetTutorialSequence(randomWrongPath);
                break;
        }
    }

    private void ShowRulesWindow() {
        rulesWindowCG.SetAlphaAndBools(true);
    }

    private void HideRulesWindow() {
        rulesWindowCG.SetAlphaAndBools(false);
    }

    private void ShowQuestionNumber() {
        questionNumberLabel.CG().alpha = 1;
    }

    private void HideQuestionNumber() {
        questionNumberLabel.CG().alpha = 0;
    }

    #endregion RULES_WINDOW

    #region STRING_FORMATTING

    private void CreateRuleArgs() {
        StringBuilder sb = new StringBuilder();
        ruleArgs = new string[] { "" };

        switch (currentRuleSetData.gameRule) {
            case GameRule.RandomWrong:
            case GameRule.ModularWrong:
                int totalIndices = currentRuleSetData.affectedQuestionIndices.Count;
                if (totalIndices > 0) {
                    for (int i = 0; i < totalIndices; i++) {
                        if (i == 0) {
                            sb.Append("{" + i.ToString() + "}");
                        }
                        else if (i == (currentRuleSetData.affectedQuestionIndices.Count - 1)) {
                            sb.Append(" and {" + i.ToString() + "}");
                        }
                        else {
                            sb.Append(", {" + i.ToString() + "}");
                        }
                    }

                    string[] args = new string[totalIndices];
                    for (int i = 0; i < totalIndices; i++) {
                        args[i] = (currentRuleSetData.affectedQuestionIndices[i] + 1).ToString();
                    }

                    ruleArgs = new string[] { string.Format(sb.ToString(), args) };
                }
                break;
            case GameRule.WrongIfAnswer:
                ruleArgs = new string[] { currentRuleSetData.wrongIfAnswerIs.ToString() };
                break;
            case GameRule.CorrectAnswerPlusOne:
                break;
            case GameRule.CorrectAnswerMinusOne:
                break;
            case GameRule.CorrectAnswerPlus:
                break;
            case GameRule.CorrectAnswerMinus:
                break;
            default:
                break;
        }

    }

    #endregion STRING_FORMATTING

    // Update is called once per frame
    void Update() {
        
    }

}

[Serializable]
public class TutorialTarget {

    public string message = "";
    public float arrowOffset = 0;
    public string pointTargetName = "";
    public Vector2 pointTarget = Vector2.zero;
    public TargetPointDirection arrowDirection = TargetPointDirection.None;

    public MessageAnchorPosition messageAnchor = MessageAnchorPosition.Top;
    public Vector2 messageCustomPos = Vector2.zero;

    public string specialMethodCall = "";
}

/// <summary>
/// Direction of the arrow
/// </summary>
public enum TargetPointDirection {
    Right,
    Down,
    Left,
    Up,
    None
}

/// <summary>
/// Anchor positions for the message label box
/// </summary>
public enum MessageAnchorPosition {
    TopLeft,
    Top,
    TopRight,
    Left,
    Center,
    Right,
    BottomLeft,
    Bottom,
    BottomRight, 
    Custom
}