﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class OptionsPopup : Popup {

    private bool isOpeningPlaytestOptions = false;

    //public bool isPauseMenu = false;
    public Text sfxVolumeLabel;
    public Text musicVolumeLabel;
    public Slider sfxVolumeSlider;
    public Slider musicVolumeSlider;
    public Toggle sfxEnabledToggle;
    public Toggle musicEnabledToggle;

    [Header("Playtest")]
    public GameObject playtestOptionsButton;
    public PlaytestOptionsWindow playtestOptionsWindow;

    public override void Awake() {
        base.Awake();
    }

	// Use this for initialization
	public override void Start () {
        base.Start();

        //if (!isPauseMenu) {
            playtestOptionsButton.SetActive(Lazarus.Instance.playtestMode);
        //}
	}

    public void OpenPlaytestOptions() {
        SFXManager.Instance.PlaySFX(SFX_TYPE.ButtonClickToony);
        isOpeningPlaytestOptions = true;
        base.Close();
    }

    protected override void OnCloseComplete() {
        base.OnCloseComplete();
        if (isOpeningPlaytestOptions) {
            isOpeningPlaytestOptions = false;
            playtestOptionsWindow.Open();
        }
    }

    public override void Open() {
        SFXManager.Instance.PlaySFX(SFX_TYPE.ButtonClickToony);
        UpdateValues();

        AddListeners();

        base.Open();
    }

    public override void Close() {
        RemoveListeners();
        AudioManager.Instance.SavePlayerPrefs();

        base.Close();
    }

    private void UpdateValues() {
        sfxVolumeSlider.value = AudioManager.Instance.SFXVolumePercent * 100;
        musicVolumeSlider.value = AudioManager.Instance.MusicVolumePercent * 100;
        sfxEnabledToggle.isOn = AudioManager.Instance.SFXEnabled;
        musicEnabledToggle.isOn = AudioManager.Instance.MusicEnabled;
        musicVolumeLabel.text = musicVolumeSlider.value.ToString() + "%";
        sfxVolumeLabel.text = sfxVolumeSlider.value.ToString() + "%";
    }

    private void AddListeners() {
        sfxVolumeSlider.onValueChanged.AddListener(OnSFXSliderChange);
        musicVolumeSlider.onValueChanged.AddListener(OnMusicSliderChange);
        sfxEnabledToggle.onValueChanged.AddListener(OnSFXEnableToggled);
        musicEnabledToggle.onValueChanged.AddListener(OnMusicEnableToggled);
    }

    private void RemoveListeners() {
        sfxVolumeSlider.onValueChanged.RemoveListener(OnSFXSliderChange);
        musicVolumeSlider.onValueChanged.RemoveListener(OnMusicSliderChange);
        sfxEnabledToggle.onValueChanged.RemoveListener(OnSFXEnableToggled);
        musicEnabledToggle.onValueChanged.RemoveListener(OnMusicEnableToggled);
    }

    private void OnMusicEnableToggled(bool value) {
        AudioManager.Instance.SetMusicEnabled(value);
    }

    private void OnSFXEnableToggled(bool value) {
        AudioManager.Instance.SetSFXEnabled(value);
    }

    private void OnMusicSliderChange(float value) {
        musicVolumeLabel.text = value.ToString() + "%";
        AudioManager.Instance.SetMusicVolumePercent(value / 100f);
    }

    private void OnSFXSliderChange(float value) {
        sfxVolumeLabel.text = value.ToString() + "%";
        AudioManager.Instance.SetSFXVolumePercent(value / 100f);
    }

    public void OnSFXSliderReleased() {
        SFXManager.Instance.PlaySFX(SFX_TYPE.AnswerCorrect);
    }

	// Update is called once per frame
	void Update () {
	
	}
}
