﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.Events;

public class QuestionSetSelectButton : MonoBehaviour {

    private Color unlockedColor = Color.white;
    private Color lockedColor = new Color(0.3176471f, 0.3176471f, 0.3176471f, 0.6901961f);

    private QuestionSetData questionSetDataReference;
    private QuestionSetSelectList selectList;

    private bool isLocked = false;
    private bool canBeUnlocked = false;
    private bool needsReset = false;
    private int setProgressionIndex = -1;

    public Text numberLabel;
    public Text progressionLabel;
    public Image buttonImage;
    public GameObject resetIcon;
    public GameObject lockIcon;
    public GameObject currencyCostIcon;

    public int ProgressionIndex { get { return setProgressionIndex; } }
    public bool IsLocked { get { return isLocked; } }

    // Use this for initialization
    void Start() {

    }

    private void UpdateLockedInformation() {
        int seen = QuestionSetProgression.Instance.QuestionsSeenInID(questionSetDataReference.id);

        if (questionSetDataReference.cost == 0 || QuestionSetProgression.Instance.OwnsSet(questionSetDataReference.id)) {
            isLocked = false;
            needsReset = false;
            buttonImage.color = unlockedColor;
            currencyCostIcon.SetActive(false);
            lockIcon.SetActive(false);
            resetIcon.SetActive(false);
            progressionLabel.text = seen.ToString() + "/" + questionSetDataReference.questions.Count.ToString();
            numberLabel.text = setProgressionIndex.ToString();
        }
        else {
            int totalSetsInResetIndex = Lazarus.Instance.GetCurrentResetData().levelDatas.Count;

            isLocked = true;
            buttonImage.color = lockedColor;
            numberLabel.text = "";

            if (setProgressionIndex > totalSetsInResetIndex && !Lazarus.Instance.IsLastGenerationLoaded()) {
                progressionLabel.text = "";
                needsReset = true;
                resetIcon.SetActive(true);
                lockIcon.SetActive(false);
                currencyCostIcon.SetActive(false);
            }
            else {
                needsReset = false;
                currencyCostIcon.SetActive(canBeUnlocked);
                progressionLabel.text = (canBeUnlocked) ? questionSetDataReference.cost.ToString() : "";
                resetIcon.SetActive(false);
                lockIcon.SetActive(true);
            }
        }
    }

    public void SetData(ref QuestionSetData data, QuestionSetSelectList theList) {
        questionSetDataReference = data;
        selectList = theList;

        UpdateLockedInformation();
    }

    public void SetCanBeUnlocked(bool value) {
        canBeUnlocked = value;
        UpdateLockedInformation();
    }

    public void SetLabels(int listNumber) {
        setProgressionIndex = listNumber;
        numberLabel.text = listNumber.ToString();
    }

    public void OnButtonPress() {
        if (needsReset) {
            selectList.OpenResetConfirmPopup();
            //ConfirmWindow.Open("Do you want to reset " + questionSetDataReference.cost.ToString() + "?", OnUnlockConfirmed);
        }
        else if (isLocked && canBeUnlocked) {
            SFXManager.Instance.PlaySFX(SFX_TYPE.ButtonClickToony);
            //if (needsReset) {
            //    selectList.OpenResetConfirmPopup();
            //    //ConfirmWindow.Open("Do you want to reset " + questionSetDataReference.cost.ToString() + "?", OnUnlockConfirmed);
            //}
            /*else*/ if (Lazarus.Instance.GetPlayerDetail(PlayerDetail.Currency) >= questionSetDataReference.cost) {
                ConfirmWindow.Open("DO YOU WANT TO UNLOCK THIS QUESTION SET FOR " + questionSetDataReference.cost.ToString() + "?", OnUnlockConfirmed);
            }
            else {
                MessagePopup.Open("YOU CANNOT AFFORD TO UNLOCK THIS SET RIGHT NOW");
            }
        }
        else if (!isLocked) {
            SFXManager.Instance.PlaySFX(SFX_TYPE.ButtonClickToony, clipVolume: 2f);
            selectList.QuestionSetSelected(setProgressionIndex);
        }
    }

    private void OnUnlockConfirmed(bool value) {
        if (value) {
            Lazarus.Instance.UpdatePlayerDetails(-questionSetDataReference.cost, PlayerDetail.Currency);
            QuestionSetProgression.Instance.AddSetToOwned(questionSetDataReference.id);
            QuestionSetProgression.Instance.Save();
            UpdateLockedInformation();
            selectList.UnlockQuestionSetPressed();
        }
    }

    // Update is called once per frame
    void Update() {

    }
}
