﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System;

public class RewardValuesWindow : Popup {

    private string uniqueQuestionRewardPrefsString = "uniqueQuestionRewardValue";
    private string seenQuestionRewardPrefsString = "seenQuestionRewardValue";
    private string questionSetCompleteRewardPrefsString = "questionSetCompleteRewardValue";

    public InputField uniqueQuestionRewardInput;
    public InputField seenQuestionRewardInput;
    public InputField questionSetCompleteRewardInput;

    public override void Awake() {
        base.Awake();
    }

    public override void Start() {
        base.Start();
    }

    public override void Open() {
        SFXManager.Instance.PlaySFX(SFX_TYPE.ButtonClickToony);
        uniqueQuestionRewardInput.text = Lazarus.Instance.uniqueQuestionReward.ToString();
        seenQuestionRewardInput.text = Lazarus.Instance.seenQuestionReward.ToString();
        questionSetCompleteRewardInput.text = Lazarus.Instance.questionSetCompleteReward.ToString();
        base.Open();
    }

    public override void OnClosePressed() {
        SFXManager.Instance.PlaySFX(SFX_TYPE.ButtonClickToony);
        base.OnClosePressed();
    }

    public override void Close() {
        int value = 1000;
        if (!string.IsNullOrEmpty(uniqueQuestionRewardInput.text)) {
            Int32.TryParse(uniqueQuestionRewardInput.text, out value);
        }
        Lazarus.Instance.uniqueQuestionReward = value;
        PlayerPrefs.SetInt(uniqueQuestionRewardPrefsString, value);

        if (!string.IsNullOrEmpty(seenQuestionRewardInput.text)) {
            Int32.TryParse(seenQuestionRewardInput.text, out value);
        }
        Lazarus.Instance.seenQuestionReward = value;
        PlayerPrefs.SetInt(seenQuestionRewardPrefsString, value);

        if (!string.IsNullOrEmpty(questionSetCompleteRewardInput.text)) {
            Int32.TryParse(questionSetCompleteRewardInput.text, out value);
        }
        Lazarus.Instance.questionSetCompleteReward = value;
        PlayerPrefs.SetInt(questionSetCompleteRewardPrefsString, value);


        base.Close();
    }

    public void OnResetToDefaults() {
        uniqueQuestionRewardInput.text = "10";
        seenQuestionRewardInput.text = "5";
        questionSetCompleteRewardInput.text = "200";
    }

}
