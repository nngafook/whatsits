﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class EnumListItem : MonoBehaviour {

    private List<EnumListItem> itemList;
    private int listIndex = -1;

    public EnumDropdown enumDropdown;
    public Utility.EnumType enumType;

    public int Value { get { return enumDropdown.Value; } }

    // Use this for initialization
    void Start() {

    }

    public void UpdateEnumType<T>(Enum e) {
        enumDropdown.UpdateDropdownOptions<T>(e);
    }

    public void SetListAndIndex(ref List<EnumListItem> list, int index) {
        itemList = list;
        listIndex = index;
    }

    public void UpdateIndex(int index) {
        listIndex = index;
    }

    public void OnRemoveButtonPressed() {
        ConfirmWindow.Open("Confirm Remove Item?", OnRemoveConfirmed);
    }

    private void OnRemoveConfirmed(bool value) {
        if (value) {
            itemList.RemoveAt(listIndex);

            for (int i = listIndex; i < itemList.Count; i++) {
                itemList[i].UpdateIndex(i);
            }

            Destroy(this.gameObject);
        }
    }

    // Update is called once per frame
    void Update() {

    }
}
