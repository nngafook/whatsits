﻿using UnityEngine;
using DG.Tweening;
using System.Collections;

[RequireComponent(typeof(CanvasGroup))]
public class Popup : MonoBehaviour {

    private bool isTweening = false;
    private float scaleSpeed = 0.25f;

    private Vector3 bigScale = new Vector3(1.25f, 1.25f, 1.25f);

    protected CanvasGroup mainCG;
    protected RectTransform mainRT;
    protected RectTransform contentRT;

    public bool startOpen = false;

    public CanvasGroup contentCG;

    public virtual void Awake() {
        mainCG = this.CG();
        mainRT = this.RT();
        contentRT = contentCG.RT();

        if (!startOpen) {
            mainCG.SetAlphaAndBools(false);
            contentRT.localScale = bigScale;
        }

    }

	// Use this for initialization
	public virtual void Start () {
        
	}

    protected virtual void OnOpenComplete() {
        isTweening = false;
        mainCG.SetAlphaAndBools(true);
    }

    protected virtual void OnCloseComplete() {
        isTweening = false;
        mainCG.SetAlphaAndBools(false);
        contentRT.localScale = bigScale;
    }

    public virtual void OnClosePressed() {
        SFXManager.Instance.PlaySFX(SFX_TYPE.ButtonClickToony);
        Close();
    }

    public virtual void Open() {
        if (!isTweening) {
            isTweening = true;
            mainCG.alpha = 1;

            contentRT.DOScale(1, scaleSpeed);
            contentCG.DOFade(1, scaleSpeed).OnComplete(OnOpenComplete);
        }
    }

    public virtual void Close() {
        if (!isTweening) {
            isTweening = true;
            contentRT.DOScale(0, scaleSpeed).SetEase(Ease.InBack);
            contentCG.DOFade(0, scaleSpeed).OnComplete(OnCloseComplete);
        }
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
