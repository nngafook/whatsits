﻿using UnityEngine;
using System.Collections;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class SceneTransitionBlackOverlay : MonoBehaviour {

    private float fadeSpeed = 0.25f;
    private string sceneToLoad;
    private CanvasGroup blackOverlayCG;

	// Use this for initialization
	void Awake () {
        blackOverlayCG = this.GetComponent<CanvasGroup>();
	}

    public void FadeToScene(string sceneName) {
        sceneToLoad = sceneName;
        this.RT().SetAsLastSibling();
        blackOverlayCG.blocksRaycasts = true;
        blackOverlayCG.interactable = true;
        blackOverlayCG.DOFade(1, fadeSpeed).SetEase(Ease.Linear).OnComplete(BlackFadeInComplete);
    }

    private void BlackFadeInComplete() {
        SceneManager.LoadScene(sceneToLoad);
    }

	// Update is called once per frame
	void Update () {
	
	}
}
