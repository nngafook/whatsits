﻿using UnityEngine;
using DG.Tweening;
using System.Collections;
using UnityEngine.UI;


public class SplashScreen : MonoBehaviour {

    private float logoShowDuration = 3;
    private bool complete = false;
    public bool Complete { get { return complete; } }

    public CanvasGroup mainCG;
    public CanvasGroup mwuLogoCG;
    public CanvasGroup nnfLogoCG;

    void Awake() {
        mainCG.SetAlphaAndBools(true);
    }

	// Use this for initialization
	void Start () {
        nnfLogoCG.DOFade(1, 1).SetEase(Ease.Linear);
        nnfLogoCG.DOFade(0, 1).SetEase(Ease.Linear).SetDelay(logoShowDuration - 1);
        nnfLogoCG.RT().DOScale(1.1f, logoShowDuration).SetEase(Ease.Linear).OnComplete(OnNNFLogoComplete);
	}

    private void OnNNFLogoComplete() {
        mwuLogoCG.DOFade(1, 1).SetEase(Ease.Linear);
        mwuLogoCG.DOFade(0, 1).SetEase(Ease.Linear).SetDelay(logoShowDuration - 1);
        mwuLogoCG.RT().DOScale(1.1f, logoShowDuration).SetEase(Ease.Linear).OnComplete(OnMWULogoComplete);
    }

    private void OnMWULogoComplete() {
        mainCG.DOFade(0, 1f).SetEase(Ease.Linear).OnComplete(OnFadeOutComplete);
    }

    private void OnFadeOutComplete() {
        complete = true;
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
