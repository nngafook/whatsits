﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.Events;

public class MultipleChoiceEvent : UnityEvent<MultipleChoice> { }

public class AnswerButton : MonoBehaviour {

    [HideInInspector]
    public MultipleChoiceEvent OnPressedEvent = new MultipleChoiceEvent();

    [HideInInspector]
    public UnityEvent OnShowComplete = new UnityEvent();
    [HideInInspector]
    public UnityEvent OnHideComplete = new UnityEvent();

    private float scaleSpeed = 0.15f;

    private Color baseColor;

    private MultipleChoice choiceValue;
    private CanvasGroup mainCG;
    private CanvasGroup labelCG;
    private CanvasGroup buttonCG;
    private CanvasGroup buttonBaseCG;
    private RectTransform labelRT;
    private RectTransform buttonRT;
    private RectTransform buttonBaseRT;

    public Text label;
    public Text choiceLabel;
    public Image buttonBase;
    public Button button;

    public string Text { get { return label.text; } }

    public MultipleChoice ChoiceValue { get { return choiceValue; } }
    public Color CurrentColor { get { return button.image.color; } }
    public Transform ButtonTransform { get { return button.transform; } }

    void Awake() {
        mainCG = this.GetComponent<CanvasGroup>();
        labelCG = label.CG();
        labelRT = label.RT();
        buttonCG = button.CG();
        buttonRT = button.RT();
        buttonBaseCG = buttonBase.CG();
        buttonBaseRT = buttonBase.RT();
        baseColor = button.image.color;
    }

    // Use this for initialization
    void Start() {
        button.onClick.AddListener(OnClicked);
    }

    private void OnClicked() {
        if (OnPressedEvent != null) {
            OnPressedEvent.Invoke(choiceValue);
        }
    }

    private void KillTweens() {
        labelCG.DOKill();
        labelRT.DOKill();
        buttonCG.DOKill();
        buttonRT.DOKill();
        buttonBaseCG.DOKill();
        buttonBaseRT.DOKill();
    }

    public void AnimateShow() {
        labelCG.alpha = 0;
        buttonCG.SetAlphaAndBools(false);
        labelRT.localScale = Vector3.zero;
        buttonRT.localScale = Vector3.zero;
        mainCG.SetAlphaAndBools(true);

        KillTweens();

        labelCG.DOFade(1, scaleSpeed);
        labelRT.DOScale(1, scaleSpeed).SetEase(Ease.OutBack);
        buttonBaseCG.DOFade(1, scaleSpeed);
        buttonBaseRT.DOScale(1, scaleSpeed).SetEase(Ease.OutBack);
        buttonCG.DOFade(1, scaleSpeed).OnComplete(OnButtonScaleUpComplete);
        buttonRT.DOScale(1, scaleSpeed).SetEase(Ease.OutBack);
    }

    private void OnButtonScaleUpComplete() {
        buttonCG.SetAlphaAndBools(true);
        if (OnShowComplete != null) {
            OnShowComplete.Invoke();
        }
    }

    public void AnimateHide() {
        KillTweens();

        buttonCG.SetAlphaAndBools(false);
        labelCG.DOFade(0, scaleSpeed);
        labelRT.DOScale(0, scaleSpeed).SetEase(Ease.InBack);
        buttonBaseCG.DOFade(0, scaleSpeed);
        buttonBaseRT.DOScale(0, scaleSpeed).SetEase(Ease.InBack);
        buttonCG.DOFade(0, scaleSpeed).OnComplete(OnButtonScaleDownComplete);
        buttonRT.DOScale(0, scaleSpeed).SetEase(Ease.InBack);
    }

    private void OnButtonScaleDownComplete() {
        mainCG.SetAlphaAndBools(false);
        ResetColor();
        if (OnHideComplete != null) {
            OnHideComplete.Invoke();
        }
    }

    public void SetShow() {
        KillTweens();

        mainCG.SetAlphaAndBools(true);
        labelCG.alpha = 1;
        buttonCG.SetAlphaAndBools(true);
        labelRT.localScale = Vector3.one;
        buttonRT.localScale = Vector3.one;
        buttonBaseRT.localScale = Vector3.one;
    }

    public void SetHide() {
        KillTweens();

        mainCG.SetAlphaAndBools(false);
        labelCG.alpha = 0;
        buttonCG.SetAlphaAndBools(false);
        labelRT.localScale = Vector3.zero;
        buttonRT.localScale = Vector3.zero;
        buttonBaseRT.localScale = Vector3.zero;
        ResetColor();
    }

    /// <summary>
    /// Sets the choice value, which is stored and used when the buttons are shuffled
    /// Also sets the choice label
    /// </summary>
    /// <param name="index"></param>
    public void SetChoiceValue(int index) {
        choiceValue = (MultipleChoice)index;
        choiceLabel.text = choiceValue.ToString();
    }

    public void SetLabel(string text) {
        label.text = text.ToUpper();
    }

    public void ResetColor() {
        SetColor(baseColor);
    }

    public void SetColor(Color c) {
        button.image.color = c;
    }

    public void SetRandomColor() {
        SetColor(ColorAnswer.GetRandomColor());
    }

    /// <summary>
    /// Sets a random color that is NOT the parameter
    /// </summary>
    /// <param name="exception"></param>
    public void SetRandomColor(ColorAnswerValue exception) {
        SetColor(ColorAnswer.GetRandomColor(exception));
    }

    // Update is called once per frame
    void Update() {

    }
}
