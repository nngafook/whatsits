﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Text;

public class AnswerSheetItem : MonoBehaviour {

    public Text label;

    private string questionTextColor = "#FF7F3AFF";
    private string answerTextColor = "#74C33FFF";
    private string darkGrayColor = "#686767FF";

	// Use this for initialization
	void Start () {
	
	}

    public void SetInfo(AnswerSheetEntry entry) {
        StringBuilder sb = new StringBuilder();
        sb.Append("<color=" + darkGrayColor + ">Q: </color>" + "<color=" + questionTextColor + ">" + entry.question + "</color>");
        sb.Append("\n");
        sb.Append("<color=" + darkGrayColor + ">A: </color>" + "<color=" + answerTextColor + ">" + entry.answer + "</color>");
        label.text = sb.ToString();

    }

	// Update is called once per frame
	void Update () {
	
	}
}
