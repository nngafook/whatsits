﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.Events;
using System;

public class ResetConfirmPopup : Popup {

    [HideInInspector]
    public Action<bool> OnResetCompleteEvent;

    private int nextGenerationCost = 0;

    public Text costLabel;

	// Use this for initialization
	public override void Start () {
	
	}

    public override void Open() {
        if (Lazarus.Instance.IsLastGenerationLoaded()) {
            Debug.LogError("Reset Confirm Popup Opened when there's no more generations");
        }
        else {
            nextGenerationCost = Lazarus.Instance.NextResetData().costToUnlock;
        }

        costLabel.text = nextGenerationCost.ToString();
        base.Open();
    }

    public void OnConfirmButtonPressed() {
        if (Lazarus.Instance.GetPlayerDetail(PlayerDetail.Currency).Value >= nextGenerationCost) {
            Lazarus.Instance.ResetProgress();
            if (OnResetCompleteEvent != null) {
                OnResetCompleteEvent(true);
            }
        }
        else {
            MessagePopup.Open("YOU CANNOT AFFORD TO RESET RIGHT NOW");
        }
        Close();
    }

    public void OnCancelButtonPressed() {
        Close();
    }

    public override void Close() {
        SFXManager.Instance.PlaySFX(SFX_TYPE.ButtonClickToony);
        base.Close();
    }
	
	// Update is called once per frame
	void Update () {
	
	}
}
