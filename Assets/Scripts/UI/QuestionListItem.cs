﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class QuestionListItem : MonoBehaviour {

    private GameDesigner gameDesigner;
    private Question question;
    private int listIndex = -1;

    public Text questionLabel;

    // Use this for initialization
    void Start() {

    }

    public void Init(ref GameDesigner gd, string questionText, int index) {
        gameDesigner = gd;
        listIndex = index;
        questionLabel.text = questionText;
    }

    public void OnViewButtonPressed() {
        gameDesigner.ViewQuestion(listIndex);
    }

    // Update is called once per frame
    void Update() {

    }
}
