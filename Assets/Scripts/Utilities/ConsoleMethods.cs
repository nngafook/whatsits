﻿using UnityEngine;
using System.Collections;
using Opencoding.CommandHandlerSystem;
using System.IO;

public class ConsoleMethods : MonoBehaviour {

	// Use this for initialization
	void Awake () {
        CommandHandlers.RegisterCommandHandlers(this);
        Debug.LogWarning("This should not exist when the game goes live".Bold().Sized(14).Colored(CustomColor.GetColor(ColorName.SKIER_BLUE)));
	}

    public void OnDestroy() {
        CommandHandlers.UnregisterCommandHandlers(this);
    }

    [CommandHandler]
    private void SetSeenWelcomeTutorial(bool value) {
        if (value) {
            PlayerPrefs.SetInt("SeenWelcomeTutorial", 1);
        }
        else {
            PlayerPrefs.SetInt("SeenWelcomeTutorial", 0);
        }
    }

    [CommandHandler]
    private void SetAnswerSheetOpened(bool value) {
        if (value) {
            PlayerPrefs.SetInt("OpenedAnswerSheet", 1);
        }
        else {
            PlayerPrefs.SetInt("OpenedAnswerSheet", 0);
        }
    }

    [CommandHandler]
    private void SetUsedRecapCycleButtons(bool value) {
        if (value) {
            PlayerPrefs.SetInt("UsedRecapCycleButtons", 1);
        }
        else {
            PlayerPrefs.SetInt("UsedRecapCycleButtons", 0);
        }
    }

    [CommandHandler]
    private void SetUsedRecap(bool value) {
        if (value) {
            PlayerPrefs.SetInt("UsedRecap", 1);
        }
        else {
            PlayerPrefs.SetInt("UsedRecap", 0);
        }
    }

    [CommandHandler]
    private void GetGeneratedQuestionsCount() {
        string path = Path.Combine(Application.persistentDataPath, "GeneratedSets/v1");
        if (Directory.Exists(path)) {
            string[] paths = Directory.GetFiles(path, "*.json");
            Debug.Log("Generated Question Sets: " + paths.Length);

            QuestionSet qs = null;
            QuestionSetData setData = null;
            int totalQuestions = 0;
            for (int i = 0; i < paths.Length; i++) {
                setData = JsonUtility.FromJson<QuestionSetData>(System.IO.File.ReadAllText(paths[i]));
                qs = ScriptableObject.CreateInstance<QuestionSet>();
                qs.data = setData;
                totalQuestions += setData.questions.Count;
            }
            Debug.Log("Total Generated Question: " + totalQuestions);
        }
    }
    
	// Update is called once per frame
	void Update () {
	
	}
}
