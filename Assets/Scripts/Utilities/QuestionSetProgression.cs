﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;

public class QuestionSetProgression  {

    private static QuestionSetProgression instance = null;

    public static QuestionSetProgression Instance {
        get {
            if (instance == null) {
                instance = new QuestionSetProgression();
                instance.Load();
            }
            return instance;
        }
    }

    public List<string> ownedQuestionSets = new List<string>();
    public List<string> questionSetsComplete = new List<string>();
    public List<SetProgression> setProgressionList = new List<SetProgression>();
    
    // Stores a list of questions seen, and the corresponding answers. 
    // (This list ID volatile (gets purged with reset))
    public List<AnswerSheetEntrySet> answerSheetEntrySets = new List<AnswerSheetEntrySet>();

    public void ClearAll() {
        ownedQuestionSets.Clear();
        questionSetsComplete.Clear();
        setProgressionList.Clear();
        answerSheetEntrySets.Clear();
    }

    private void Copy(QuestionSetProgression from) {
        instance.setProgressionList = new List<SetProgression>(from.setProgressionList);
        instance.ownedQuestionSets = new List<string>(from.ownedQuestionSets);
        instance.questionSetsComplete = new List<string>(from.questionSetsComplete);
        instance.answerSheetEntrySets = new List<AnswerSheetEntrySet>(from.answerSheetEntrySets);
    }

    private bool Contains(SetProgression set) {
        bool rVal = false;

        for (int i = 0; i < setProgressionList.Count; i++) {
            if (setProgressionList[i].questionSetID == set.questionSetID) {
                rVal = true;
                break;
            }
        }

        return rVal;
    }

    private bool Contains(string id) {
        bool rVal = false;

        for (int i = 0; i < setProgressionList.Count; i++) {
            if (setProgressionList[i].questionSetID == id) {
                rVal = true;
                break;
            }
        }

        return rVal;
    }

    private void Load() {
        string folderPath = System.IO.Path.Combine(Application.persistentDataPath, "SetProgression");
        string filePath = System.IO.Path.Combine(folderPath, "SetProgression.json");
        if (System.IO.File.Exists(filePath)) {
            QuestionSetProgression loadedSetProgression = JsonUtility.FromJson<QuestionSetProgression>(System.IO.File.ReadAllText(filePath));
            this.Copy(loadedSetProgression);
        }
    }

    public bool HasSeenQuestion(string setProgressionID, string questionID) {
        bool rVal = false;

        if (Contains(setProgressionID)) {
            SetProgression setProg = SetProgressionByID(setProgressionID);
            if (setProg.questionIDsSeen.Contains(questionID)) {
                rVal = true;
            }
        }

        return rVal;
    }

    public int QuestionsSeenInID(string id) {
        int rVal = 0;

        if (Contains(id)) {
            rVal = SetProgressionByID(id).questionIDsSeen.Count;
        }

        return rVal;
    }

    public void AddSetProgression(SetProgression set) {
        if (this.Contains(set)) {
            SetProgression existingSet = SetProgressionByID(set.questionSetID);
            for (int i = 0; i < set.questionIDsSeen.Count; i++) {
                if (!existingSet.questionIDsSeen.Contains(set.questionIDsSeen[i])) {
                    existingSet.questionIDsSeen.Add(set.questionIDsSeen[i]);
                }
            }
        }
        else {
            setProgressionList.Add(set);
        }
    }

    public void AddIDSeen(string setID, string questionID) {
        SetProgression setProgression = SetProgressionByID(setID);
        if (!setProgression.questionIDsSeen.Contains(questionID)) {
            setProgression.questionIDsSeen.Add(questionID);
        }
    }

    public SetProgression SetProgressionByID(string id) {
        SetProgression data = null;
        for (int i = 0; i < setProgressionList.Count; i++) {
            if (setProgressionList[i].questionSetID == id) {
                data = setProgressionList[i];
                break;
            }
        }
        return data;
    }

    public bool OwnsSet(string id) {
        bool rVal = false;

        for (int i = 0; i < ownedQuestionSets.Count; i++) {
            if (ownedQuestionSets[i] == id) {
                rVal = true;
                break;
            }
        }

        return rVal;
    }

    public void AddSetToOwned(string id) {
        if (!OwnsSet(id)) {
            ownedQuestionSets.Add(id);
        }
    }

    public bool CompletedSet(string id) {
        bool rVal = false;
        for (int i = 0; i < questionSetsComplete.Count; i++) {
            if (questionSetsComplete[i] == id) {
                rVal = true;
                break;
            }
        }
        return rVal;
    }

    public void AddSetToCompleted(string id) {
        if (!CompletedSet(id)) {
            questionSetsComplete.Add(id);
        }
    }

    public void AddAnswerSheetEntries(int progressionIndex, List<AnswerSheetEntry> source) {
        AnswerSheetEntrySet entrySet = new AnswerSheetEntrySet();
        entrySet.progressionIndex = progressionIndex;

        bool exists = false;
        for (int i = 0; i < answerSheetEntrySets.Count; i++) {
            if (answerSheetEntrySets[i].progressionIndex == progressionIndex) {
                entrySet = answerSheetEntrySets[i];
                exists = true;
                break;
            }
        }
        if (!exists) {
            answerSheetEntrySets.Add(entrySet);
        }

        exists = false;
        for (int i = 0; i < source.Count; i++) {
            exists = false;
            for (int j = 0; j < entrySet.entries.Count; j++) {
                if (entrySet.entries[j].uid == source[i].uid) {
                    exists = true;
                    break;
                }
            }

            if (!exists) {
                entrySet.entries.Add(source[i]);
            }
        }
    }

    public void Save() {
        string folderPath = System.IO.Path.Combine(Application.persistentDataPath, "SetProgression");
        System.IO.Directory.CreateDirectory(folderPath);

        string filePath = System.IO.Path.Combine(folderPath, "SetProgression.json");

        Debug.LogFormat(("Saving Set Progression at: {0}").Colored(Color.magenta), filePath);
        System.IO.File.WriteAllText(filePath, JsonUtility.ToJson(this, true));
    }

}

[Serializable]
public class SetProgression {

    public string questionSetID = "_id_";
    public List<string> questionIDsSeen = new List<string>();

}

[Serializable]
public class AnswerSheetEntry {

    public string uid = "";
    public string question = "";
    public string answer = "";

    public AnswerSheetEntry(string id, string q, string a) {
        uid = id;
        question = q;
        answer = a;
    }

}

// A question set equivalent of answer sheet entries
[Serializable]
public class AnswerSheetEntrySet {

    // Question set ID that the entries belong to
    //public string id = "";

    // Game Progression Rank for setting the label in the list
    public int progressionIndex = -1;
    public List<AnswerSheetEntry> entries = new List<AnswerSheetEntry>();

}