﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class TextUtil {

    public static string QuestionCategoryTitle(QuestionCategory category) {
        string rVal = "-QuestionCategory Not Found-";

        switch (category) {
            case QuestionCategory.General:
                rVal = "General";
                break;
            case QuestionCategory.VideoGames:
                rVal = "Video Games";
                break;
            case QuestionCategory.Geography:
                rVal = "Geography";
                break;
            case QuestionCategory.TV:
                rVal = "T.V.";
                break;
            case QuestionCategory.Cinema:
                rVal = "Cinema";
                break;
            case QuestionCategory.Music:
                rVal = "Music";
                break;
            default:
                break;
        }

        return rVal.ToUpper();
    }

    public static string QuestionCategoryDescription(QuestionCategory category) {
        string rVal = "-Description Not Found-";

        switch (category) {
            case QuestionCategory.General:
                rVal = "Random Questions from any Category";
                break;
            case QuestionCategory.VideoGames:
                rVal = "All Your Answers Are Belong To Us";
                break;
            case QuestionCategory.Geography:
                rVal = "Test Your Geographical Knowledge";
                break;
            case QuestionCategory.TV:
                rVal = "Test Your TV Knowledge";
                break;
            case QuestionCategory.Cinema:
                rVal = "You Can't Handle These Questions!";
                break;
            case QuestionCategory.Music:
                rVal = "Free Bird!!";
                break;
            default:
                break;
        }

        return rVal.ToUpper();
    }

}
