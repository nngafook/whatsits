﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using DG.Tweening;

public class TitleScreen : MonoBehaviour {

    private Color highlightBlue = new Color(0.3647059f, 0.6196079f, 0.8313726f, 1f);

    public RectTransform tapToBeginLabel;

    public SplashScreen splashScreen;
    public SceneTransitionBlackOverlay sceneTransitionBlackOverlay;

    // Use this for initialization
    void Awake() {
        MusicManager.Instance.PlayLobbyMusic();

        tapToBeginLabel.DOScale(1.1f, 1).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.Linear);
        tapToBeginLabel.CG().DOFade(0.5f, 1f).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.Linear);
        tapToBeginLabel.GetComponent<Text>().DOColor(highlightBlue, 1f).SetLoops(-1, LoopType.Yoyo).SetEase(Ease.Linear);
    }

    public void OnPlayPressed() {
        sceneTransitionBlackOverlay.FadeToScene("Lobby");
    }

    public void OnDesignerPressed() {
        SceneManager.LoadScene("GameDesigner");
    }

    // Update is called once per frame
    void Update() {
        if (Input.GetMouseButtonUp(0) && splashScreen.Complete) {
            OnPlayPressed();
        }
    }
}
