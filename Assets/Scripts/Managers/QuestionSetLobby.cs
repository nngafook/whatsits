﻿using UnityEngine;
using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
#if UNITY_IOS || UNITY_ANDROID
using UnityEngine.Advertisements;
using UnityEngine.Analytics;
#endif

public class QuestionSetLobby : MonoBehaviour {

    public AdWarningIcon adWarningIcon;
    public SceneTransitionBlackOverlay sceneTransitionBlackOverlay;
    public GameObject reviewQAPrompt;
    public GameObject resetProgressButton;

    [Space(10)]
    public QuestionSetSelectList questionSetSelectList;
    public ResetConfirmPopup resetConfirmPopup;
    public Popup playtestWelcomeWindow;

    [Space(10)]
    public CanvasGroup questionSetListCG;

    [Space(10)]
    public CanvasGroup adBlocker;

    [Header("Currency")]
    //public CurrencyCheatWindow currencyCheatWindow;
    public Text currentCurrencyLabel;

    // Use this for initialization
    void Start() {
        MusicManager.Instance.PlayLobbyMusic();

        adWarningIcon.UpdatePlaysRemaining();

        Lazarus.Instance.OnQuestionSetsResetEvent.AddListener(OnQuestionSetsReset);

        questionSetListCG.SetAlphaAndBools(true);
        UpdateCurrencyLabel();
        questionSetSelectList.LoadItems();

        CheckQuestionSetInPlayCompletion();
        CheckQuestionSetResetProgress();

        // Only do this if we're in playtest mode and if question set is null, which means this is the first time the lobby is shown, and not shown after a level
        if (Lazarus.Instance.playtestMode && Lazarus.Instance.QuestionSetInPlay == null) {
            playtestWelcomeWindow.Open();
        }

        if (PlayerPrefs.GetInt("OpenedAnswerSheet", 0) == 0 && QuestionSetProgression.Instance.answerSheetEntrySets.Count > 0) {
            SetReviewQAVisible(true);
        }
        else {
            SetReviewQAVisible(false);
        }
    }


    public void OnDestroy() {
        Lazarus.Instance.OnQuestionSetsResetEvent.RemoveListener(OnQuestionSetsReset);
    }

    private void CheckQuestionSetResetProgress() {
        bool lastSetLoaded = Lazarus.Instance.IsLastGenerationLoaded();
        resetProgressButton.SetActive(!lastSetLoaded);
    }

    public void CheckQuestionSetInPlayCompletion() {
        if (Lazarus.Instance.QuestionSetInPlay != null) {
            // Means we came back to the lobby from a round
            int seen = QuestionSetProgression.Instance.QuestionsSeenInID(Lazarus.Instance.QuestionSetInPlay.data.id);
            if (!QuestionSetProgression.Instance.CompletedSet(Lazarus.Instance.QuestionSetInPlay.data.id) && Lazarus.Instance.QuestionSetInPlay.data.questions.Count == seen) {
                //int setCompleteBonusReward = (Lazarus.Instance.playtestMode) ? Lazarus.Instance.questionSetCompleteReward : Lazarus.Instance.QuestionSetInPlay.data.setCompleteBonus;
                int setCompleteBonusReward = Lazarus.Instance.QuestionSetInPlay.data.setCompleteBonus;
                RewardPopup.Open("SET COMPLETE BONUS", setCompleteBonusReward);
                RewardPopup.OnClosedEvent.AddListener(OnSetCompleteRewardClosed);
            }
        }
        else {
            Debug.Log("QuestionSetLobby - Lazarus.Instance.QuestionSetInPlay was NULL");
        }
    }

    private void OnSetCompleteRewardClosed() {
        RewardPopup.OnClosedEvent.RemoveListener(OnSetCompleteRewardClosed);
        QuestionSetProgression.Instance.AddSetToCompleted(Lazarus.Instance.QuestionSetInPlay.data.id);
        QuestionSetProgression.Instance.Save();
        UpdateCurrencyLabel();
        Lazarus.Instance.QuestionSetInPlay = null;
    }

    private void OnQuestionSetsReset() {
        questionSetSelectList.LoadItems();
        CheckQuestionSetResetProgress();
    }

    public void UpdateCurrencyLabel() {
        currentCurrencyLabel.text = Lazarus.Instance.GetPlayerDetail(PlayerDetail.Currency).ToString();
    }
    public void OnReturnButtonPressed() {
        SceneManager.LoadScene("Title");
    }

    public void StartGame() {
        sceneTransitionBlackOverlay.FadeToScene("QuestionTime");
    }

    public void ShowAd() {
        #if UNITY_IOS || UNITY_ANDROID
        if (Lazarus.Instance.ShowRewardedAd(HandleAdCallback)) {
            
            Lazarus.Instance.UpdatePlayerDetails(1, PlayerDetail.VoluntaryAdCount);
            Analytics.CustomEvent("ProgressReset", new Dictionary<string, object> {
                { "VoluntaryAdCount", Lazarus.Instance.GetPlayerDetail(PlayerDetail.VoluntaryAdCount).Value }
            });

            adBlocker.blocksRaycasts = true;
            adBlocker.interactable = true;
        }
        #endif
    }

    public void OpenResetConfirmPopup() {
        SFXManager.Instance.PlaySFX(SFX_TYPE.ButtonClickToony);
        resetConfirmPopup.Open();
        resetConfirmPopup.OnResetCompleteEvent += OnResetConfirmed;
    }

    private void OnResetConfirmed(bool value) {
        resetConfirmPopup.OnResetCompleteEvent -= OnResetConfirmed;
        if (value) {
            UpdateCurrencyLabel();
        }
    }

    #if UNITY_IOS || UNITY_ANDROID
    private void HandleAdCallback(ShowResult result) {
        adBlocker.blocksRaycasts = false;
        adBlocker.interactable = false;
        if (RewardPopup.IsOpen) {
            RewardPopup.OnClosedEvent.AddListener(OnRewardPopupClosed);
        }
    }
    #endif

    private void OnRewardPopupClosed() {
        RewardPopup.OnClosedEvent.RemoveListener(OnRewardPopupClosed);
        UpdateCurrencyLabel();
    }

    public void OnAnswerSheetOpened() {
        if (QuestionSetProgression.Instance.answerSheetEntrySets.Count > 0) {
            PlayerPrefs.SetInt("OpenedAnswerSheet", 1);
            SetReviewQAVisible(false);
        }
    }

    private void SetReviewQAVisible(bool value) {
        reviewQAPrompt.SetActive(value);
    }

    // Update is called once per frame
    void Update() {
        //if (Input.GetKey(KeyCode.LeftControl)) {
        //    if (Input.GetKeyUp(KeyCode.Space)) {
        //        currencyCheatWindow.Toggle();
        //    }
        //}
    }

}
