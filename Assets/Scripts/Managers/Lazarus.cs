﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using UnityEngine.Events;
using Opencoding.CommandHandlerSystem;
using UnityEngine.SceneManagement;
using UnityEngine.Analytics;
#if UNITY_IOS || UNITY_ANDROID
using UnityEngine.Advertisements;
#endif

public class Lazarus : MonoBehaviour {

    #region SINGLETON
    private static Lazarus instance = null;
    private static bool isShuttingDown;
    #endregion SINGLETON

    public static Lazarus Instance {
        get {
            // If there is no _Instance for DialogManager yet and we're not shutting down at the moment
            if (instance == null && !isShuttingDown) {
                //Try finding and _Instance in the scene
                instance = GameObject.FindObjectOfType<Lazarus>();
                //If no _Instance was found, let's create one
                if (!instance) {
                    GameObject singleton = (GameObject)Instantiate(Resources.Load("Prefabs/Lazarus"));
                    singleton.name = "Lazarus";
                    instance = singleton.GetComponent<Lazarus>();
                }
                //Set the _Instance to persist between levels.
                DontDestroyOnLoad(instance.gameObject);
            }
            //Return an _Instance, either that we found or that we created.
            return instance;
        }
    }

    [HideInInspector]
    public UnityEvent OnQuestionSetsResetEvent = new UnityEvent();

    //private QuestionSet questionSetInPlay;
    public QuestionSet QuestionSetInPlay { get; set; }

    void OnApplicationQuit() {
        isShuttingDown = true;
    }

    #if UNITY_IOS || UNITY_ANDROID
    private Action<ShowResult> adResultCallback;
    #endif

    private string uniqueQuestionRewardPrefsString = "uniqueQuestionRewardValue";
    private string seenQuestionRewardPrefsString = "seenQuestionRewardValue";
    private string questionSetCompleteRewardPrefsString = "questionSetCompleteRewardValue";

    private static bool hasBeenAwaked = false;
    private QuestionSetGenerationRules generationRules;
    private string setGenerationRulesPath = "DataObjects/GenerationRules";
    private string questionsV1Path = "Questions/ALLQUESTIONSv1";
    private int questionsPerSet = 20;

    public bool playtestMode = false;

    public int uniqueQuestionReward = 10;
    public int seenQuestionReward = 2;
    public int questionSetCompleteReward = 200;
    //public int uniqueQuestionReward;

    public QuestionSetGenerationRules GetQuestionSetGenerationRules { get { return generationRules; } }

    void Awake() {
        //If there is no _Instance of this currently in the scene
        if (instance == null) {
            //Set ourselves as the _Instance and mark us to persist between scenes
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else {
            //If there is already an _Instance of this and It's not me, then destroy me as there should only be one.
            if (this != instance)
                Destroy(this.gameObject);
        }

        SingletonAwake();
    }

    private void SingletonAwake() {
        if (!hasBeenAwaked) {
            hasBeenAwaked = true;
            generationRules = Resources.Load<QuestionSetGenerationRules>(setGenerationRulesPath);

            // Temp
            CreateQuestionSets();

            #if UNITY_EDITOR
            if (playtestMode) {
                Debug.LogWarning("Playtest mode is true. Turn off before going live".Bold().Sized(14).Colored(CustomColor.GetColor(ColorName.PURPLE_PLUM)));
            }
            #endif

            if (playtestMode) {
                uniqueQuestionReward = PlayerPrefs.GetInt(uniqueQuestionRewardPrefsString, 10);
                seenQuestionReward = PlayerPrefs.GetInt(seenQuestionRewardPrefsString, 2);
                questionSetCompleteReward = PlayerPrefs.GetInt(questionSetCompleteRewardPrefsString, 200);
            }

            CommandHandlers.RegisterCommandHandlers(this);
        }
    }

    public void OnDestroy() {
        CommandHandlers.UnregisterCommandHandlers(this);
    }

    // Use this for initialization
    void Start() {

    }

    private void CreateQuestionSets() {
        int? resetCount = GetPlayerDetail(PlayerDetail.ResetCount);
        QuestionSetHashInfo hash = GetQuestionSetHashInfo();

        string generatedFolderPath = Path.Combine(Application.persistentDataPath, "GeneratedSets");
        generatedFolderPath = Path.Combine(generatedFolderPath, "v1");

        bool generatedFolderExists = Directory.Exists(generatedFolderPath);
        bool generatedFolderIsEmpty = (generatedFolderExists) ? !(Directory.GetFiles(generatedFolderPath).Length > 0) : true;

        Queue<Question> questionsQueue;
        QuestionSetData allQuestionsSetData;
        string filename = "";
        int setsMade = 0;

        // resetCount cannot be null here, cause the player detail file is validated if it doesn't exist
        if ((!generatedFolderExists || generatedFolderIsEmpty) || (hash == null || resetCount != hash.resetVersion)) {
            Debug.Log("Generating New QuestionSets");

            allQuestionsSetData = JsonUtility.FromJson<QuestionSetData>(Resources.Load<TextAsset>(questionsV1Path).text);

            questionsQueue = new Queue<Question>(Utility.ShuffleList(allQuestionsSetData.questions, UnityEngine.Random.Range(10021, 1091827)));
            
            Debug.Log("Total Questions: " + questionsQueue.Count);
            
            filename = "";
            setsMade = 0;
            int mathQuestionsAdded = 0;
            int totalMathQuestionsToAdd = 5;

            int buttonColorQuestionsAdded = 0;
            int totalButtonColorQuestionsToAdd = 5;
            List<ColorAnswerValue> usedColors = new List<ColorAnswerValue>();

            while(questionsQueue.Count > 0) {
                mathQuestionsAdded = 0;
                buttonColorQuestionsAdded = 0;
                usedColors = new List<ColorAnswerValue>();
                usedColors.Add(ColorAnswerValue.Null);

                filename = "Set_" + setsMade.ToString();
                QuestionSetData qsData = new QuestionSetData();
                QuestionSetLevelData levelData = generationRules.GetResetData(resetCount.Value).GetLevelData(setsMade);
                qsData.name = filename;
                qsData.GenerateID();
                qsData.cost = levelData.cost;
                qsData.shuffleButtons = levelData.shuffleButtons;
                qsData.gameProgressionRank = setsMade + 1;
                qsData.timePerQuestion = levelData.timePerQuestion;
                qsData.levelTime = levelData.levelTime;
                qsData.setCompleteBonus = levelData.setCompleteBonus;
                qsData.possibleGameRules = new List<GameRule>(levelData.possibleGameRules);
                qsData.questions = new List<Question>();
                while (questionsQueue.Count > 0 && qsData.questions.Count < questionsPerSet) {
                    if (mathQuestionsAdded < totalMathQuestionsToAdd) {
                        Question q = new Question();
                        q.GenerateMathQuestion(ref levelData);
                        qsData.questions.Add(q);
                        mathQuestionsAdded++;
                    }
                    else if (buttonColorQuestionsAdded < totalButtonColorQuestionsToAdd) {
                        Question q = new Question();
                        q.GenerateButtonColorQuestion(ref usedColors);
                        qsData.questions.Add(q);
                        buttonColorQuestionsAdded++;
                    }
                    else {
                        Question q = questionsQueue.Dequeue();
                        qsData.questions.Add(q);
                    }
                }
                Utility.QuestionSetToJson(qsData, filename, generatedFolderPath);
                setsMade++;
            }

            //Debug.Log("Questions Left: " + questionsQueue.Count);
            
            // Reset Hash Info
            hash = new QuestionSetHashInfo();
            hash.resetVersion = resetCount.Value;
            Utility.HashToJson(hash);
            
            #if UNITY_EDITOR
            Debug.Log("New Hash Info Generated. New Reset Count: " + hash.resetVersion);
            #endif
        }
        else if (generatedFolderExists) {
            Debug.Log("Generated Folder Exists. Going to check for questions that haven't been added to the sets");
            List<string> existingQuestionIDs = new List<string>();

            // Get the generated question sets
            List<QuestionSetData> datas = new List<QuestionSetData>();
            string[] paths = Directory.GetFiles(generatedFolderPath);
            if (paths.Length > 0) {
                for (int i = 0; i < paths.Length; i++) {
                    datas.Add(JsonUtility.FromJson<QuestionSetData>(System.IO.File.ReadAllText(paths[i])));
                }
            }

            // Iterate through the existing sets and add them to a list to be compared against
            QuestionSetData qsd = null;
            for (int i = 0; i < datas.Count; i++) {
                qsd = datas[i];
                for (int j = 0; j < qsd.questions.Count; j++) {
                    existingQuestionIDs.Add(qsd.questions[j].uid);
                }
            }

            setsMade = datas.Count;
            allQuestionsSetData = JsonUtility.FromJson<QuestionSetData>(Resources.Load<TextAsset>(questionsV1Path).text);
            questionsQueue = new Queue<Question>(Utility.ShuffleList(allQuestionsSetData.questions, UnityEngine.Random.Range(10021, 1091827)));
            Queue<Question> newQueue = new Queue<Question>();
            bool found = false;
            Question q = null;

            while (questionsQueue.Count > 0) {
                q = questionsQueue.Dequeue();
                found = false;
                for (int i = 0; i < existingQuestionIDs.Count; i++) {
                    if (q.uid == existingQuestionIDs[i]) {
                        found = true;
                        break;
                    }
                }
                if (!found) {
                    newQueue.Enqueue(q);
                }
            }
            

            while (newQueue.Count > 0) {
                QuestionSetData qsData = new QuestionSetData();
                // If the last question set is not full of questions
                if (datas[datas.Count - 1].questions.Count < questionsPerSet) {
                    Debug.Log("Adding new questions to SET_" + (datas.Count - 1));
                    qsData = datas[datas.Count - 1];
                    filename = "Set_" + (datas.Count - 1).ToString();
                }
                else {
                    Debug.Log("Created a new set to facilitate new questions");
                    // Create new set to add the new questions too.
                    filename = "Set_" + setsMade.ToString();
                    QuestionSetLevelData levelData = generationRules.GetResetData(resetCount.Value).GetLevelData(setsMade);
                    qsData.name = filename;
                    qsData.GenerateID();
                    qsData.cost = levelData.cost;
                    qsData.gameProgressionRank = setsMade + 1;
                    qsData.timePerQuestion = levelData.timePerQuestion;
                    qsData.levelTime = levelData.levelTime;
                    qsData.setCompleteBonus = levelData.setCompleteBonus;
                    qsData.questions = new List<Question>();

                    // Add math and button color questions to set
                    int mathQuestionsAdded = 0;
                    int totalMathQuestionsToAdd = 5;

                    int buttonColorQuestionsAdded = 0;
                    int totalButtonColorQuestionsToAdd = 5;
                    List<ColorAnswerValue> usedColors = new List<ColorAnswerValue>();
                    usedColors.Add(ColorAnswerValue.Null);
                    while (mathQuestionsAdded < totalMathQuestionsToAdd) {
                        q = new Question();
                        q.GenerateMathQuestion(ref levelData);
                        qsData.questions.Add(q);
                        mathQuestionsAdded++;
                    }

                    while (buttonColorQuestionsAdded < totalButtonColorQuestionsToAdd) {
                        q = new Question();
                        q.GenerateButtonColorQuestion(ref usedColors);
                        qsData.questions.Add(q);
                        buttonColorQuestionsAdded++;
                    }

                    setsMade++;
                }

                while (newQueue.Count > 0 && qsData.questions.Count < questionsPerSet) {
                    q = newQueue.Dequeue();
                    qsData.questions.Add(q);
                }
                Utility.QuestionSetToJson(qsData, filename, generatedFolderPath);
                Debug.Log("Added new set: " + filename);
            }
        }
        else {
            Debug.Log("Question Sets Not Generated - " + ((hash == null) ? "Hash was null".Bold() : "Player Detail Reset Count == Hash Reset Count".Bold()));
        }
    }

    private QuestionSetHashInfo GetQuestionSetHashInfo() {
        QuestionSetHashInfo rVal = null;
        if (File.Exists(System.IO.Path.Combine(Application.persistentDataPath, "QuestionSetHashInfo.json"))) {
            rVal = JsonUtility.FromJson<QuestionSetHashInfo>(System.IO.File.ReadAllText(System.IO.Path.Combine(Application.persistentDataPath, "QuestionSetHashInfo.json")));
        }
        else {
            // Generate Default Hash. Set count to -1 so that questions get set
            rVal = new QuestionSetHashInfo();
            rVal.resetVersion = -1;
            Utility.HashToJson(rVal);
        }
        return rVal;
    }

    public QuestionSetResetData GetCurrentResetData() {
        return generationRules.GetResetData(GetPlayerDetail(PlayerDetail.ResetCount).Value);
    }

    public bool IsLastGenerationLoaded() {
        return GetPlayerDetail(PlayerDetail.ResetCount) + 1 >= generationRules.resetDataList.Count;
    }

    public QuestionSetResetData NextResetData() {
        return generationRules.GetResetData(GetPlayerDetail(PlayerDetail.ResetCount).Value + 1);
    }

    private void CreateDefaultPlayerDetailsFile() {
        AddDefaultPlayerDetail(PlayerDetail.Currency);
        AddDefaultPlayerDetail(PlayerDetail.ResetCount);
    }

    public void SetPlayerDetail(int value, PlayerDetail detail) {
        string filename = Utility.PlayerDetailTagValue(PlayerDetail.FileName);
        string tag = Utility.PlayerDetailTagValue(detail);

        ValidatePlayerDetailsExist();

        ES2.Save(value, filename + "?tag=" + tag + "&encrypt=true&password=sOnIsAUrUs");
    }

    public void UpdatePlayerDetails(int value, PlayerDetail detail) {
        string filename = Utility.PlayerDetailTagValue(PlayerDetail.FileName);
        string tag = Utility.PlayerDetailTagValue(detail);
        int currentValue = value;

        ValidatePlayerDetailsExist();

        if (ES2.Exists(filename + "?tag=" + tag)) {
            currentValue = ES2.Load<int>(filename + "?tag=" + tag + "&encrypt=true&password=sOnIsAUrUs");
            currentValue += value;
        }

        ES2.Save(currentValue, filename + "?tag=" + tag + "&encrypt=true&password=sOnIsAUrUs");
    }

    public int? GetPlayerDetail(PlayerDetail detail) {
        int? rVal = null;
        string filename = Utility.PlayerDetailTagValue(PlayerDetail.FileName);
        string tag = Utility.PlayerDetailTagValue(detail);

        ValidatePlayerDetailsExist();

        if (ES2.Exists(filename + "?tag=" + tag)) {
            rVal = ES2.Load<int>(filename + "?tag=" + tag + "&encrypt=true&password=sOnIsAUrUs");
        }
        else {
            Debug.LogWarning((filename + "?tag=" + tag + " does not exist").Bold());
        }
        return rVal;
    }

    private void AddDefaultPlayerDetail(PlayerDetail detail) {
        string filename = Utility.PlayerDetailTagValue(PlayerDetail.FileName);
        string tag = Utility.PlayerDetailTagValue(detail);
        int value = 0;
        switch (detail) {
            case PlayerDetail.FileName:
                Debug.Log("Why is you trying to add a default player detail value for PlayerDetail.FileName?".Bold());
                break;
            case PlayerDetail.Currency:
                value = 0;
                break;
            case PlayerDetail.ResetCount:
                value = 0;
                break;
            case PlayerDetail.AdShowPlayCount:
                value = 5;
                break;
            case PlayerDetail.AdRewardLowCount:
                value = 0;
                break;
            case PlayerDetail.VoluntaryAdCount:
                value = 0;
                break;
            default:
                Debug.Log(("Default Case for AddDefaultPlayerDetail() - Detail: " + detail.ToString()).Bold());
                break;
        }
        ES2.Save(value, filename + "?tag=" + tag + "&encrypt=true&password=sOnIsAUrUs");
        Debug.Log(("Added default player detail value for: " + detail.ToString()).Colored(Color.blue).Bold());
    }

    private void ValidatePlayerDetailsExist() {
        string filename = Utility.PlayerDetailTagValue(PlayerDetail.FileName);
        if (!ES2.Exists(filename)) {
            Debug.LogWarning((filename + " does not exist. Creating default").Bold());
            CreateDefaultPlayerDetailsFile();
        }
        else {
            int totalPlayerDetails = Enum.GetValues(typeof(PlayerDetail)).Length;
            string tag = "";
            for (int i = 0; i < totalPlayerDetails; i++) {
                PlayerDetail detail = (PlayerDetail)i;
                // Skip the filename detail
                if (detail == PlayerDetail.FileName) {
                    continue;
                }

                tag = Utility.PlayerDetailTagValue(detail);
                // If the detail does not exist, add a default
                if (!ES2.Exists(filename + "?tag=" + tag)) {
                    AddDefaultPlayerDetail(detail);
                }
            }
        }
    }

    #if UNITY_IOS || UNITY_ANDROID
    public bool ShowRewardedAd(Action<ShowResult> callback = null) {
        bool rVal = false;
        if (Advertisement.IsReady("rewardedVideo")) {
            var options = new ShowOptions { resultCallback = HandleShowResult };
            Advertisement.Show("rewardedVideo", options);
            MusicManager.Instance.StopLobbyMusic(false);
            rVal = true;
        }
        else {
            MessagePopup.Open("No Ads Available");
        }

        adResultCallback = callback;

        return rVal;
    }
    #endif

    public bool ShowAd() {
        bool rVal = false;
        #if UNITY_IOS || UNITY_ANDROID
        if (Advertisement.IsReady()) {
            var options = new ShowOptions { resultCallback = NonRewardAdShown };
            Advertisement.Show("", options);
            MusicManager.Instance.StopLobbyMusic(false);
            rVal = true;
        }
        else {
            Debug.Log("Ad was not ready");
        }
        #endif
        return rVal;
    }

    #if UNITY_IOS || UNITY_ANDROID
    private void NonRewardAdShown(ShowResult result) {
        MusicManager.Instance.PlayLobbyMusic();
    }
    #endif

    #if UNITY_IOS || UNITY_ANDROID
    private void HandleShowResult(ShowResult result) {
        switch (result) {
            case ShowResult.Finished:
                Debug.Log("The ad was successfully shown.");
                RewardPopup.Open();
                break;
            case ShowResult.Skipped:
                Debug.Log("The ad was skipped before reaching the end.");
                break;
            case ShowResult.Failed:
                Debug.LogError("The ad failed to be shown.");
                break;
        }

        MusicManager.Instance.PlayLobbyMusic();

        if (adResultCallback != null) {
            adResultCallback(result);
            adResultCallback = null;
        }
    }
    #endif

    #region RESET

    public void ResetProgress() {

        Analytics.CustomEvent("ProgressReset", new Dictionary<string, object> {
            { "CurrencyPreReset", GetPlayerDetail(PlayerDetail.Currency).Value },
            { "ResetCost", NextResetData().costToUnlock },
            { "ResetFromLevel", GetPlayerDetail(PlayerDetail.ResetCount).Value },
            { "TotalSetsComplete", QuestionSetProgression.Instance.questionSetsComplete.Count },
            { "TotalSetsUnlocked", QuestionSetProgression.Instance.ownedQuestionSets.Count }
        });

        // Spend currency
        int costToReset = NextResetData().costToUnlock;
        UpdatePlayerDetails(-costToReset, PlayerDetail.Currency);

        // Player Detail reset
        int playerDetailsReset = GetPlayerDetail(PlayerDetail.ResetCount).Value;
        // Make sure that the player details reset value does not get higher than generation rules allow.
        if (playerDetailsReset + 1 < generationRules.resetDataList.Count) {
            UpdatePlayerDetails(1, PlayerDetail.ResetCount);
        }
        
        // Hash reset
        QuestionSetHashInfo hash = new QuestionSetHashInfo();
        hash.resetVersion = -1;
        Utility.HashToJson(hash);

        QuestionSetProgression.Instance.ClearAll();
        QuestionSetProgression.Instance.Save();

        CreateQuestionSets();

        if (OnQuestionSetsResetEvent != null) {
            OnQuestionSetsResetEvent.Invoke();
        }
    }

    [CommandHandler]
    public void DebugResetAllProgress() {
        // Player Detail reset
        SetPlayerDetail(0, PlayerDetail.ResetCount);

        // Hash reset
        QuestionSetHashInfo hash = new QuestionSetHashInfo();
        // This gets set to -1, cause when the sets are generated, this hash value gets updated in there. So it cannot be equal to the player detail
        hash.resetVersion = -1;
        Utility.HashToJson(hash);

        QuestionSetProgression.Instance.ClearAll();
        QuestionSetProgression.Instance.Save();

        CreateQuestionSets();

        if (OnQuestionSetsResetEvent != null) {
            OnQuestionSetsResetEvent.Invoke();
        }
    }

    #endregion RESET

    // Update is called once per frame
    void Update() {
        if (Input.GetKeyUp(KeyCode.F11)) {
            Application.CaptureScreenshot("Screenshot" + UnityEngine.Random.Range(0, 1000).ToString() +".png", 3);
        }
    }

}
