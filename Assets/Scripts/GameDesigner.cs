﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using System.IO;
using UnityEngine.SceneManagement;
using System.Net;
using System;
using System.Text;
using System.Net.Mail;
using System.Security.Cryptography.X509Certificates;
using System.Net.Security;

#pragma warning disable 0414
public class GameDesigner : MonoBehaviour {

    private string projectJsonFolderPath = "/QuestionSetJSONFiles/";
    //private string projectBackupJsonFolderPath = "/QuestionSetJSONFiles/Backups/";
    private bool usingProjectQuestionSets = false;

    private QuestionSetData loadedQuestionSetData;

    public CanvasGroup blackOverlayCG;

    [Header("Containers")]
    //public CanvasGroup newQuestionWindowCG;
    public ScrollRect questionScrollRect;
    public BottomMenu bottomMenu;

    [Header("Windows")]
    public NewQuestionWindow newQuestionWindow;
    public ViewQuestionWindow viewQuestionWindow;
    public ViewQuestionSetWindow viewQuestionSetWindow;

    [Header("Question")]
    public InputField questionInputField;
    public Dropdown answerTypeDropdown;
    public Dropdown questionCategoryDropdown;

    public bool UsingProjectQuestionSets { get { return usingProjectQuestionSets; } set { usingProjectQuestionSets = value; } }

    void Awake() {
        #if UNITY_EDITOR
            usingProjectQuestionSets = true;
        #endif

        blackOverlayCG.SetAlphaAndBools(true);
        bottomMenu.OnQuestionSetLoaded(false);
    }

    // Use this for initialization
    void Start() {
        SetNewQuestionWindowVisible(false);
    }

    private void SetNewQuestionWindowVisible(bool value) {
        if (value) {
            newQuestionWindow.Open();
        }
        else {
            newQuestionWindow.Close();
        }
    }

    private void ClearFields() {
        questionInputField.text = "";
        //correctAnswerLabel.text = "";
        //labelAnswerContent.ClearFields();
        questionScrollRect.normalizedPosition = Vector2.one;
    }

    private void CloseViewQuestionSetWindow() {
        viewQuestionSetWindow.Close();
    }
    
    private void CloseViewQuestionWindow() {
        viewQuestionWindow.Close();
    }

    public void SetBlackOverlay(bool value) {
        blackOverlayCG.SetAlphaAndBools(value);
    }

    public void LoadQuestionSet(QuestionSetData data) {
        loadedQuestionSetData = data;
        bottomMenu.OnQuestionSetLoaded(true);

        ViewQuestionSet();
        //SetNewQuestionWindowVisible(true);
    }

    public void CreateQuestionSet(QuestionSetData data) {
        loadedQuestionSetData = data;
        bottomMenu.OnQuestionSetLoaded(true);
        Utility.QuestionSetToJson(loadedQuestionSetData, loadedQuestionSetData.name);
        
        ViewQuestionSet();
        //SetNewQuestionWindowVisible(true);
    }

    public void EmailLoadedSet() {
        if (loadedQuestionSetData != null) {
            //For File Attachment, more files can also be attached
            string path = (usingProjectQuestionSets) ? System.IO.Path.Combine(System.IO.Path.Combine(Application.dataPath, "QuestionSetJSONFiles"), loadedQuestionSetData.name + ".json") : System.IO.Path.Combine(Utility.SavedQuestionSetsPath, loadedQuestionSetData.name + ".json");
            Attachment att = new Attachment(path);
            //tested only for files on local machine


            //Hardcoded recipient email and subject and body of the mail
            string myEmail = "nicholasngfook@gmail.com";
            string recipient = "nicholasngfook@gmail.com";
            string subject = "Question Set - " + loadedQuestionSetData.name;
            string message = "What's Its Question Set";
            string pw = "uxoikzlpbtubupta";

            //SmtpClient client = new SmtpClient("smtp-mail.outlook.com");
            SmtpClient client = new SmtpClient("smtp.gmail.com");
            //SMTP server can be changed for gmail, yahoomail, etc., just google it up

            client.Port = 587;
            client.DeliveryMethod = SmtpDeliveryMethod.Network;
            client.UseDefaultCredentials = false;
            System.Net.NetworkCredential credentials = new System.Net.NetworkCredential(myEmail, pw);
            client.EnableSsl = true;
            client.Credentials = (System.Net.ICredentialsByHost)credentials;

            try {
                var mail = new MailMessage(myEmail, recipient);
                mail.Subject = subject;
                mail.Body = message;
                mail.Attachments.Add(att);
                ServicePointManager.ServerCertificateValidationCallback = delegate(object s, X509Certificate certificate, X509Chain chain, SslPolicyErrors sslPolicyErrors) {
                    return true;
                };

                client.Send(mail);
                ScreenMessage.PushMessage("Email Sent!", CustomColor.GetColor(ColorName.SUBMIT_GREEN), 1f);
            }

            catch (Exception ex) {
                ScreenMessage.PushMessage("There was an error sending the email!", CustomColor.GetColor(ColorName.ERROR_RED), 1f);
                throw ex;
            }
        }
    }

    /// <summary>
    /// Called from the NewQuestionWindow
    /// </summary>
    public void OnNewQuestionSavePressed() {
            /*Question q = new Question();
            q.GenerateUID();
            q.questionText = questionInputField.text;
            q.questionType = (QuestionType)answerTypeDropdown.value;
            q.correctLabelAnswer = correctAnswerLabel.text;
            q.answerCategory = (AnswerCategory)answerCategoryDropdown.value;
            q.questionCategory = (QuestionCategory)questionCategoryDropdown.value;
            q.correctButtonColorAnswer = (ColorAnswerValue)correctButtonColorDropdown.value;
            q.correctLabelColorAnswer = (ColorAnswerValue)correctLabelColorDropdown.value;

            q.buttonLabelOptions = new List<string>(labelAnswerContent.LabelOptions);

            loadedQuestionSetData.questions.Add(q);
            Utility.QuestionSetToJson(loadedQuestionSetData, loadedQuestionSetData.name);

            // ==== Check for errors
            ScreenMessage.PushMessage("Question Saved!", CustomColor.GetColor(ColorName.SUBMIT_GREEN), 1f);

            ClearFields();
            SetNewQuestionWindowVisible(false);
            ViewQuestionSet();
        */
    }

    /// <summary>
    /// Called from the NewQuestionWindow
    /// </summary>
    public void OnNewQuestionCancelPressed() {
        SetNewQuestionWindowVisible(false);
        ViewQuestionSet();
        ClearFields();
    }

    public void BackToTitle() {
        SceneManager.LoadScene("Title");
    }

    public void OnEmailSetsPressed() {
        if (loadedQuestionSetData != null) {
            StartCoroutine(UploadLevel());
        }
        else {
            ScreenMessage.PushMessage("Question Set in GameDesigner is NULL", CustomColor.GetColor(ColorName.RACY_RED), 2f);
        }
    }

    public void CopySetsToProject() {
        #if UNITY_EDITOR

        string projectPath = Application.dataPath + projectJsonFolderPath;

        // To copy a folder's contents to a new location:
        // Create a new target folder, if necessary.
        if (!System.IO.Directory.Exists(projectPath)) {
            System.IO.Directory.CreateDirectory(projectPath);
        }

        // To copy a file to another location and 
        // overwrite the destination file if it already exists.
                        //System.IO.File.Copy(Utility.SavedQuestionSetsPath + "/CAT.json", projectPath + "/CAT.json", true);

        
        // To copy all the files in one directory to another directory.
        // Get the files in the source folder. (To recursively iterate through
        // all subfolders under the current directory, see
        // "How to: Iterate Through a Directory Tree.")
        // Note: Check for target path was performed previously
        //       in this code example.
        if (System.IO.Directory.Exists(Utility.SavedQuestionSetsPath)) {
            string[] files = System.IO.Directory.GetFiles(Utility.SavedQuestionSetsPath);
            string fileName = "";
            string destFile = "";
            //bool backupFolderMade = false;

            // Copy the files and overwrite destination files if they already exist.
            foreach (string s in files) {
                // Use static Path methods to extract only the file name from the path.
                fileName = System.IO.Path.GetFileName(s);
                destFile = System.IO.Path.Combine(projectPath, fileName);

                if (System.IO.File.Exists(destFile)) {
                    //backupFolderMade = true;
                    Debug.Log("Exists");
                }

                System.IO.File.Copy(s, destFile, true);
            }
        }
        else {
            Console.WriteLine("Source path does not exist!");
        }








        /*
        string[] paths = Directory.GetFiles(Utility.SavedQuestionSetsPath);
        if (paths.Length > 0) {
            for (int i = 0; i < paths.Length; i++) {
                questionSetPaths.Add(paths[i]);
                loadSetDropdown.options.Add(new UnityEngine.UI.Dropdown.OptionData(Path.GetFileNameWithoutExtension(paths[i])));
            }

            //newSetInputField.text = Path.GetFileNameWithoutExtension(paths[0]);
        }
        else {
            loadSetDropdown.options.Add(new UnityEngine.UI.Dropdown.OptionData("<Empty>"));
        }
        */

        //UnityEditor.FileUtil.CopyFileOrDirectory(Utility.SavedQuestionSetsPath, Application.dataPath + projectBackupJsonFolderPath);
        #endif
    }

    public void OnCloseQuestionWindowButtonPressed() {
        CloseViewQuestionWindow();
        viewQuestionSetWindow.Open();
    }

    public void OnCloseQuestionSetWindowButtonPressed() {
        CloseViewQuestionSetWindow();
        loadedQuestionSetData = null;
        bottomMenu.OnQuestionSetLoaded(false);
    }

    /// <summary>
    /// Button exists in the View Question Set window
    /// </summary>
    public void OnNewQuestionButtonPressed() {
        //SetNewQuestionWindowVisible(true);
        CloseViewQuestionSetWindow();

        Question q = new Question();
        q.GenerateUID();
        q.GenerateDefaults();

        loadedQuestionSetData.questions.Add(q);
        ViewQuestion(loadedQuestionSetData.questions.Count - 1);

        //q.questionText = questionInputField.text;
        //q.questionType = (QuestionType)answerTypeDropdown.value;
        //q.correctLabelAnswer = correctAnswerLabel.text;
        //q.answerCategory = (AnswerCategory)answerCategoryDropdown.value;
        //q.questionCategory = (QuestionCategory)questionCategoryDropdown.value;
        //q.correctButtonColorAnswer = (ColorAnswerValue)correctButtonColorDropdown.value;
        //q.correctLabelColorAnswer = (ColorAnswerValue)correctLabelColorDropdown.value;

        //q.buttonLabelOptions = new List<string>(labelAnswerContent.LabelOptions);

    }

    IEnumerator UploadLevel() {
        string filename = loadedQuestionSetData.name + ".json";
        string filepath = Path.Combine(Utility.SavedQuestionSetsPath, filename);

        if (System.IO.File.Exists(filepath)) {
            string jsonData = System.IO.File.ReadAllText(filepath);

            WWW localFile = new WWW(jsonData);
            yield return localFile;

            if (!(localFile == null)) {
                Debug.Log("Good");
            }
            else {
                ScreenMessage.PushMessage("JSON file is NULL", CustomColor.GetColor(ColorName.RACY_RED), 2f);
                Debug.Log("NULL");
            }

            ////converting the xml to bytes to be ready for upload
            byte[] setData = Encoding.UTF8.GetBytes(jsonData);

            WWWForm postForm = new WWWForm();
            postForm.AddField("action", "questionSetUpload");
            postForm.AddField("file", "file");
            postForm.AddBinaryData("file", setData, filename, "text/json");

            WWW upload = new WWW("http://www.nngafook.com/QuestionSetUpload.php", postForm);
            yield return upload;
            if (upload.error == null) {
                Debug.Log(upload.text);
                Debug.Log("upload error null");
                ScreenMessage.PushMessage(filename + " Uploaded Successfully", CustomColor.GetColor(ColorName.SUBMIT_GREEN), 2f);
            }
            else {
                Debug.Log("Error: " + upload.error);
                Debug.Log("isdone: " + upload.isDone);
                Debug.Log("bytesdownloaded " + upload.bytesDownloaded);
                Debug.Log("progress " + upload.progress);
                Debug.Log("url " + upload.url);
                Debug.Log("responseheaders " + upload.responseHeaders);
                Debug.Log("size " + upload.size);
                Debug.Log("text " + upload.text);
                Debug.Log("uploadprogress " + upload.uploadProgress);

                ScreenMessage.PushMessage("Upload Failed. Maybe no Internet?", CustomColor.GetColor(ColorName.RACY_RED), 2f);
            }
        }
        else {
            ScreenMessage.PushMessage("JSON file at: " + filepath + " is NULL", CustomColor.GetColor(ColorName.RACY_RED), 2f);
        }
    }

    public void DeleteQuestionSet() {
        if (loadedQuestionSetData != null) {
            File.Delete(Path.Combine(Utility.SavedQuestionSetsPath, loadedQuestionSetData.name + ".json"));


            SetNewQuestionWindowVisible(false);
            CloseViewQuestionSetWindow();
            CloseViewQuestionWindow();


            loadedQuestionSetData = null;
            bottomMenu.OnQuestionSetLoaded(false);
            ScreenMessage.PushMessage("Question Set Deleted!", CustomColor.GetColor(ColorName.RACY_RED), 2f);
        }
    }

    private void ViewQuestionSet() {
        if (loadedQuestionSetData != null) {
            //SetNewQuestionWindowVisible(false);
            viewQuestionSetWindow.SetData(loadedQuestionSetData);
            viewQuestionSetWindow.Open();
        }
        else {
            Debug.LogError("WHY WAS THE questionSetData NULL IN THE GAME DESIGNER?");
        }
        
    }

    public void ViewQuestion(int index) {
        if (loadedQuestionSetData != null) {
            CloseViewQuestionSetWindow();
            viewQuestionWindow.SetData(ref loadedQuestionSetData, index);
            viewQuestionWindow.Open();
        }
        else {
            Debug.LogError("WHY WAS THE questionSetData NULL IN THE GAME DESIGNER?");
        }
    }

    //private void ViewQuestion(Question q) {
    //    if (loadedQuestionSetData != null) {
    //        CloseViewQuestionSetWindow();
    //        viewQuestionWindow.SetDataForNewQuestion(ref loadedQuestionSetData, q);
    //        viewQuestionWindow.Open();
    //    }
    //    else {
    //        Debug.LogError("WHY WAS THE questionSetData NULL IN THE GAME DESIGNER?");
    //    }
    //}

    public void QuestionDeleted() {
        viewQuestionWindow.Close();
        ViewQuestionSet();
    }

    public void QuestionSaved() {
        viewQuestionWindow.Close();
        ViewQuestionSet();
    }

    // Update is called once per frame
    void Update() {
        
    }
}
