﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.Events;
using Opencoding.CommandHandlerSystem;

public class TimeManager : MonoBehaviour {

    [HideInInspector]
    public UnityEvent OnLevelTimeUpEvent = new UnityEvent();
    [HideInInspector]
    public UnityEvent OnQuestionTimeUpEvent = new UnityEvent();

    private RectTransform levelTimeLabelRT;
    private CanvasGroup levelTimeLabelCG;

    private RectTransform questionTimeLabelRT;
    private CanvasGroup questionTimeLabelCG;

    private float questionTimerScaleSpeed = 0.5f;
    private float questionTimeLabelStartY = 0;
    private float questionTimeCounter = 0;

    public float totalLevelTime = 60;
    public float totalTimePerQuestion = 5;

    public Text levelTimeLabel;
    public Text questionTimeLabel;

    void Awake() {
        questionTimeCounter = totalTimePerQuestion;

        levelTimeLabel.text = totalLevelTime.ToString();
        questionTimeLabel.text = questionTimeCounter.ToString();

        levelTimeLabelRT = levelTimeLabel.RT();
        levelTimeLabelCG = levelTimeLabel.CG();

        questionTimeLabelRT = questionTimeLabel.RT();
        questionTimeLabelCG = questionTimeLabel.CG();

        questionTimeLabelStartY = questionTimeLabelRT.anchoredPosition.y;

        HideQuestionTimer();

        CommandHandlers.RegisterCommandHandlers(this);
    }

    public void OnDestroy() {
        CommandHandlers.UnregisterCommandHandlers(this);
    }

    // Use this for initialization
    void Start() {
        
    }

    public void SetTimes(ref QuestionSet questionSetInPlay) {
        totalTimePerQuestion = questionSetInPlay.data.timePerQuestion;
        questionTimeCounter = totalTimePerQuestion;
        totalLevelTime = questionSetInPlay.data.levelTime;

        levelTimeLabel.text = totalLevelTime.ToString();
        questionTimeLabel.text = questionTimeCounter.ToString();
    }

    public void PauseGame() {
        levelTimeLabelRT.DOPause();
        levelTimeLabelCG.DOPause();

        questionTimeLabelRT.DOPause();
        questionTimeLabelCG.DOPause();
    }

    public void UnpauseGame() {
        levelTimeLabelRT.DOPlay();
        levelTimeLabelCG.DOPlay();

        questionTimeLabelRT.DOPlay();
        questionTimeLabelCG.DOPlay();
    }

    #region LEVEL_TIMER

    private void TickLevelTimer() {
        levelTimeLabelRT.localScale = Vector3.one;
        levelTimeLabelCG.alpha = 1;
        if (totalLevelTime > 0) {
            levelTimeLabel.color = (totalLevelTime > 5) ? Color.white : CustomColor.GetColor(ColorName.RACY_RED);

            levelTimeLabel.text = totalLevelTime.ToString();
            levelTimeLabelRT.DOScale(1.2f, 0.75f).SetEase(Ease.Linear).OnComplete(OnScaleUpComplete);
            totalLevelTime--;
        }
        else {
            levelTimeLabel.text = "Time's Up!";
            if (OnLevelTimeUpEvent != null) {
                OnLevelTimeUpEvent.Invoke();
            }
        }
    }

    private void OnScaleUpComplete() {
        if (totalLevelTime <= 5) {
            SFXManager.Instance.PlaySFX(SFX_TYPE.TimerLowBeep, clipVolume:0.5f);
        }
        levelTimeLabelRT.DOScale(0, 0.25f).SetEase(Ease.Linear).OnComplete(OnScaleDownComplete);
        levelTimeLabelCG.DOFade(0, 0.2f).SetEase(Ease.Linear);
    }

    private void OnScaleDownComplete() {
        TickLevelTimer();
    }

    public void BeginLevel() {
        TickLevelTimer();
    }

    public void StopTimer() {
        levelTimeLabelRT.DOKill();
        levelTimeLabelCG.DOKill();
        levelTimeLabelRT.localScale = Vector3.one;
        levelTimeLabelCG.alpha = 1;
    }

    #endregion LEVEL_TIMER

    #region QUESTION_TIMER

    public void StartQuestionTimer() {
        TickQuestionTimer();
    }

    public void StopQuestionTimer() {
        questionTimeLabelCG.DOKill();
        questionTimeLabelRT.DOKill();
        HideQuestionTimer();
        questionTimeCounter = totalTimePerQuestion;
    }

    private void TickQuestionTimer() {
        HideQuestionTimer();
        
        if (questionTimeCounter < 0) {
            questionTimeCounter = totalTimePerQuestion;
            if (OnQuestionTimeUpEvent != null) {
                OnQuestionTimeUpEvent.Invoke();
            }
        }
        else {
            questionTimeLabel.text = questionTimeCounter.ToString();

            questionTimeCounter--;

            questionTimeLabelCG.DOFade(1, questionTimerScaleSpeed);
            questionTimeLabelRT.DOScale(1, questionTimerScaleSpeed).SetEase(Ease.OutBounce).OnComplete(OnQuestionTimerScaleUpComplete);
        }
    }

    private void OnQuestionTimerScaleUpComplete() {
        float to = questionTimeLabelRT.anchoredPosition.y + 50;
        questionTimeLabelCG.DOFade(0, questionTimerScaleSpeed);
        questionTimeLabelRT.DOAnchorPosY(to, questionTimerScaleSpeed).SetEase(Ease.Linear).OnComplete(OnQuestionTimerMoveUpComplete);
    }

    private void OnQuestionTimerMoveUpComplete() {
        TickQuestionTimer();
    }

    private void HideQuestionTimer() {
        questionTimeLabelRT.localScale = Vector3.zero;
        questionTimeLabelCG.alpha = 0;
        questionTimeLabelRT.anchoredPosition = questionTimeLabelRT.anchoredPosition.SetY(questionTimeLabelStartY);
    }

    public int QuestionTimeRemaining {
        get {
            // Adds one cause the actual numbers gets decremented at the same time the label is shown, 
            // so the second hasn't actually ticked until the next tick
            return (int)questionTimeCounter + 1;
        }
    }

    #endregion QUESTION_TIMER

    [CommandHandler]
    private void DebugEndLevel() {
        totalLevelTime = 0;
    }

    // Update is called once per frame
    void Update() {
        #if UNITY_EDITOR
        if (Input.GetKeyUp(KeyCode.Keypad0)) {
            DebugEndLevel();
        }
        #endif
    }

}
