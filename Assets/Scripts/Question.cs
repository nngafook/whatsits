﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Text;

[Serializable]
public class Question {
    
    public string questionText = "";
    public string instruction = "";
    public string uid = "";
    public int versionSet = -1;

    public QuestionCategory questionCategory = QuestionCategory.Uncategorized;
    public QuestionType questionType = QuestionType.Uncategorized;

    [Space(10)]
    public List<QuestionCategory> subCategories = new List<QuestionCategory>();

    [Header("Answer Holding Variables")]
    public string correctLabelAnswer = "";
    public ColorAnswerValue correctButtonColorAnswer = ColorAnswerValue.Null;
    public ColorAnswerValue correctLabelColorAnswer = ColorAnswerValue.Null;

    [Space(10)]
    public List<AnswerCategory> answerCategories = new List<AnswerCategory>();
    
    [Space(10)]
    public List<string> buttonLabelOptions = new List<string>();


    public string QuestionText {
        get {
            string rVal = "";
            if (questionType == QuestionType.NounSpot) {
                rVal = "Spot The Noun\n".Colored(CustomColor.GetColor(ColorName.PURPLE_PLUM)) + questionText;
            }
            else {
                rVal = questionText;
            }
            return rVal;
        }
    }

    // Different types of questions:
    // Multiple choice (uses label answer)
    // Button color (uses button color)
    // Label color (uses label color)
    // Label color text (uses label answer)
    // Spot (Spot the Noun/Adjective/etc)

    public Question() {

    }

    /// <summary>
    /// Used in the GameDesigner to generate a question based on the defaults saved in the QuestionSetDefaults
    /// </summary>
    public void GenerateDefaults() {
        questionCategory = QuestionSetDefaults.Instance.questionCategory;
        questionType = QuestionSetDefaults.Instance.questionType;
        correctButtonColorAnswer = QuestionSetDefaults.Instance.correctButtonColor;
        correctLabelColorAnswer = QuestionSetDefaults.Instance.correctLabelColor;
        answerCategories = new List<AnswerCategory>(QuestionSetDefaults.Instance.answerCategories);
        subCategories = new List<QuestionCategory>(QuestionSetDefaults.Instance.subCategories);
    }

    public void GenerateUID() {
        uid = Utility.GenerateUniqueStringID();
    }

    public void GenerateMathQuestion(ref QuestionSetLevelData levelData) {
        GenerateUID();
        versionSet = 0;
        questionType = QuestionType.RandomMath;
        questionCategory = QuestionCategory.Math;
        answerCategories.Add(AnswerCategory.Numbers);

        int answer = -1;
        int value = levelData.mathValueRange.RandomInt;
        int secondValue = levelData.mathValueRange.RandomInt;
        string qText = "";
        MathSign sign = MathSign.Plus;
        if (levelData.mathSigns.Count > 0) {
            sign = levelData.mathSigns.RandomElement<MathSign>();
        }
        
        switch (sign) {
            case MathSign.Plus:
                qText = value + " + " + secondValue + " = ?";
                answer = value + secondValue;
                break;
            case MathSign.Minus:
                qText = ((value >= secondValue) ? value : secondValue) + " - " + ((secondValue < value) ? secondValue : value) + " = ?";
                answer = Mathf.Abs(value - secondValue);
                break;
            case MathSign.Multiply:
                secondValue = levelData.mathMultiplyRange.RandomInt;
                qText = value + " x " + secondValue + " = ?";
                answer = value * secondValue;
                break;
            case MathSign.Divide:
                secondValue = levelData.mathDivideRange.RandomInt;
                value = secondValue * value;
                qText = value + " / " + secondValue + " = ?";
                answer = value / secondValue;
                break;
        }

        questionText = qText;
        correctLabelAnswer = answer.ToString();

        buttonLabelOptions.Add((answer + 1).ToString());
        buttonLabelOptions.Add((answer - 1).ToString());
        buttonLabelOptions.Add(UnityEngine.Random.Range(answer + UnityEngine.Random.Range(2, 6), answer * 2).ToString());
        buttonLabelOptions.Add(UnityEngine.Random.Range(0, Mathf.Abs((answer - UnityEngine.Random.Range(2, 6)) * 2)).ToString());

        bool allUnique = false;
        while (!allUnique) {
            // Make sure no duplicate answers
            for (int i = 0; i < buttonLabelOptions.Count; i++) {
                for (int j = i + 1; j < buttonLabelOptions.Count; j++) {
                    if (buttonLabelOptions[i] == buttonLabelOptions[j]) {
                        buttonLabelOptions.RemoveAt(i);
                        #if UNITY_EDITOR
                        Debug.Log("Duplicate Math Answer Found");
                        #endif
                        i = -1;
                        break;
                    }
                }
            }

            if (buttonLabelOptions.Count >= 4) {
                allUnique = true;
            }
            else {
                while (buttonLabelOptions.Count < 4) {
                    buttonLabelOptions.Add(UnityEngine.Random.Range(answer + UnityEngine.Random.Range(2, 6), answer * 2).ToString());
                }
            }
        }

    }

    public void GenerateButtonColorQuestion(ref List<ColorAnswerValue> usedColors) {
        GenerateUID();
        versionSet = 0;
        questionType = QuestionType.ButtonColor;
        questionCategory = QuestionCategory.General;
        answerCategories.Add(AnswerCategory.ButtonColors);

        // Get a list of random colors that have not been used in the set as yet.
        List<ColorAnswerValue> colorsAvailable = new List<ColorAnswerValue>();
        bool found = false;
        for (int i = 0; i < Enum.GetNames(typeof(ColorAnswerValue)).Length; i++) {
            found = false;
            for (int j = 0; j < usedColors.Count; j++) {
                if (usedColors[j] == (ColorAnswerValue)i) {
                    found = true;
                    break;
                }
            }
            if (!found) {
                colorsAvailable.Add((ColorAnswerValue)i);
            }
        }
        // Get a random color from the available colors list, assign that as the answer
        ColorAnswerValue correctColor = (colorsAvailable.Count > 0) ? colorsAvailable[UnityEngine.Random.Range(0, colorsAvailable.Count)] : (ColorAnswerValue)UnityEngine.Random.Range(0, Enum.GetNames(typeof(ColorAnswerValue)).Length);

        string cHex = ColorUtility.ToHtmlStringRGBA(ColorAnswer.GetColor(correctColor));
        questionText = "Press the <color=#" + cHex + ">" + correctColor.ToString() + "</color> Button";
        correctLabelAnswer = correctColor.ToString();
        correctButtonColorAnswer = correctColor;

        usedColors.Add(correctColor);
    }

    public bool Validate() {
        bool rVal = true;

        if (rVal && string.IsNullOrEmpty(questionText)) {
            rVal = false;
            Debug.LogError("Validate failed in Question. questionText was either null or empty");
        }

        if (rVal && string.IsNullOrEmpty(uid)) {
            rVal = false;
            Debug.LogError("Validate failed in Question. uid was either null or empty");
        }

        if (rVal && versionSet == -1) {
            rVal = false;
            Debug.LogError("Validate failed in Question. Version Set was -1");
        }

        //QuestionCategory questionCategory;
        //QuestionType questionType;

        if (rVal && subCategories == null) {
            rVal = false;
            Debug.LogError("Validate failed in Question. subCategories was either null or empty");
        }

        //string correctLabelAnswer;
        //ColorAnswerValue correctButtonColorAnswer;
        //ColorAnswerValue correctLabelColorAnswer;

        //AnswerCategory answerCategory;
        if (rVal && buttonLabelOptions == null) {       // Or if the questionType REQUIRES label options
            rVal = false;
            Debug.LogError("Validate failed in Question. buttonLabelOptions was either null or empty");

        }


        return rVal;
    }

}

public class QuestionRecapData {
    public int questionNumber = -1;
    public Question question = null;
    public List<MultipleChoice> buttonChoiceOrder = new List<MultipleChoice>();
    public List<Color> buttonColorOrder = new List<Color>();
    public List<string> buttonLabels = new List<string>();
    // The correct answer based on question.
    public MultipleChoice correctMultipleChoice = MultipleChoice.A;
    // The choice the player made
    public MultipleChoice choiceMade = MultipleChoice.A;
    public bool supposedToAnswerWrong = false;
    public bool answeredRight = false;
}