﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using DG.Tweening;

public class ScreenMessage : MonoBehaviour {

    private static ScreenMessage instance = null;
    public static ScreenMessage Instance { get { return instance;}}

    private float scaleSpeed = 0.25f;
    private float baseShowDuration = 0.5f;
    private float showDuration = 0.5f;
    private Text label;
    private RectTransform labelRT;

    void Awake() {
        instance = this;
        label = this.GetComponent<Text>();
        labelRT = this.RT();
    }

    // Use this for initialization
    void Start() {

    }

    private void SetMessage(string message) {
        labelRT.DOKill();
        label.text = message;
        labelRT.DOScale(1, scaleSpeed).SetEase(Ease.OutBack).OnComplete(OnScaleUpComplete);
    }

    private void OnScaleUpComplete() {
        labelRT.DOScale(0, scaleSpeed).SetEase(Ease.InBack).SetDelay(showDuration);
        showDuration = baseShowDuration;
    }

    public static void PushMessage(string message) {
        instance.labelRT.localScale = Vector3.zero;
        instance.label.color = Color.white;
        instance.SetMessage(message);
    }

    public static void PushMessage(string message, Color c) {
        instance.labelRT.localScale = Vector3.zero;
        instance.label.color = c;
        instance.SetMessage(message);
    }

    public static void PushMessage(string message, Color c, float duration) {
        instance.labelRT.localScale = Vector3.zero;
        instance.label.color = c;
        instance.showDuration = duration;
        instance.SetMessage(message);
    }

    // Update is called once per frame
    void Update() {

    }
}
