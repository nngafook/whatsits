﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

public class MusicManager : MonoBehaviour {

    private static MusicManager instance = null;
    public static MusicManager Instance {
        get {
            if (instance == null) {
                AudioManager.Instance.Init();
            }
            return instance;
        }
    }

    [Header("Clips")]
    public AudioClip lobbyMusic;

    private static AudioClip activeMusicClip;

    void Awake() {
        instance = this;
    }

    // Use this for initialization
    void Start() {

    }

    private void PlayMusic(AudioClip clip, float duration = 1, bool loop = false) {
        if (AudioManager.Instance.MusicEnabled && activeMusicClip != clip) {
            activeMusicClip = clip;
            AudioManager.Instance.PlayMusic(activeMusicClip, duration, loop);
        }
    }

    public void PlayLobbyMusic(bool fade = false) {
        float fadeDuration = (fade) ? 1f : 0f;
        PlayMusic(lobbyMusic, fadeDuration, true);
        //AudioManager.Instance.OnMusicComplete += OnThemeMusicComplete;
    }

    public void StopLobbyMusic(bool fade = true) {
        float fadeDuration = (fade) ? 1f : 0f;
        AudioManager.Instance.FadeMusicOut(fadeDuration);
        activeMusicClip = null;
    }

    //public void CrossFadeToDesignerRealtor() {
    //    PlayMusic(_DesignerRealtorMusic, 1f, true);
    //}

    #region EVENT_CALLBACKS
    private void OnThemeMusicComplete() {
        //AudioManager.Instance.OnMusicComplete -= OnThemeMusicComplete;
        //PlayGameMusic();
    }

    #endregion EVENT_CALLBACKS

}
