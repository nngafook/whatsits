﻿using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;
using System.Collections.Generic;

public class AudioManager : MonoBehaviour {
    private const int MAX_SFX_SOURCES = 5;

    #region SINGLETON
    private static AudioManager instance = null;
    private static bool isShuttingDown;
    #endregion SINGLETON

    #region EVENTS_AND_DELEGATES
    public delegate void MusicCompleteEvent();
    public event MusicCompleteEvent OnMusicComplete;
    public delegate void SFXCompleteEvent();
    public event SFXCompleteEvent OnSFXComplete;
    public delegate void FadeMusicOutEvent();
    public event FadeMusicOutEvent OnMusicFadeOutComplete;
    #endregion EVENTS_AND_DELEGATES

    #region ENABLED_BOOLS
    private bool musicEnabled = true;
    private bool sfxEnabled = true;
    public bool MusicEnabled { get { return musicEnabled; } }
    public bool SFXEnabled { get { return sfxEnabled; } }
    #endregion ENABLED_BOOLS

    private string musicEnabledPlayerPrefString = "MusicEnabled";
    private string sfxEnabledPlayerPrefsString  = "SFXEnabled";
    private string musicVolumePlayerPrefsString = "MusicVolumePercent";
    private string sfxVolumePlayerPrefsString = "SFXVolumePercent";

    private float masterVolumePercent = 0.5f;
    private float sfxVolumePercent = 1;
    private float musicVolumePercent = 1;

    private int activeMusicSourceIndex;

    private static bool hasAwoken = false;
    private bool gameBeganMuted = false;

    private AudioSource[] musicSources;

    private AudioSource sfxAudioSource;
    private Stack<AudioSource> sfxAudioSourcePool;
    // maybe we don't need another list with a reference to current sources in use? but maybe we do, so I'll just leave this here
    private List<AudioSource> sfxAudioSourcesInUse;

    public float SFXVolumePercent { get { return sfxVolumePercent; } }
    public float MusicVolumePercent { get { return musicVolumePercent; } }

    public static AudioManager Instance {
        get {
            // If there is no _Instance for DialogManager yet and we're not shutting down at the moment
            if (instance == null && !isShuttingDown) {
                //Try finding and _Instance in the scene
                instance = GameObject.FindObjectOfType<AudioManager>();
                //If no _Instance was found, let's create one
                if (!instance) {
                    GameObject singleton = (GameObject)Instantiate(Resources.Load("Prefabs/AudioManager"));
                    singleton.name = "AudioManager";
                    instance = singleton.GetComponent<AudioManager>();
                }
                //Set the _Instance to persist between levels.
                DontDestroyOnLoad(instance.gameObject);
            }
            //Return an _Instance, either that we found or that we created.
            return instance;
        }
    }

    public void Init() {

    }

    void OnApplicationQuit() {
        isShuttingDown = true;
    }

    #region GETTERS_SETTERS
    public AudioSource ActiveMusicSource {
        get {
            return musicSources[activeMusicSourceIndex];
        }
    }
    #endregion GETTERS_SETTERS

    // Use this for initialization
    void Awake() {
        //If there is no _Instance of this currently in the scene
        if (instance == null) {
            //Set ourselves as the _Instance and mark us to persist between scenes
            instance = this;
            DontDestroyOnLoad(this);
        }
        else {
            //If there is already an _Instance of this and It's not me, then destroy me as there should only be one.
            if (this != instance)
                Destroy(this.gameObject);
        }

        if (!hasAwoken) {
            hasAwoken = true;
            //Debug.Log("AudioManager AWAKE");

            musicEnabled = PlayerPrefs.GetInt(musicEnabledPlayerPrefString, 1).AsBool();
            musicVolumePercent = PlayerPrefs.GetFloat(musicVolumePlayerPrefsString, 0.5f);
            sfxEnabled = PlayerPrefs.GetInt(sfxEnabledPlayerPrefsString, 1).AsBool();
            sfxVolumePercent = PlayerPrefs.GetFloat(sfxVolumePlayerPrefsString, 0.5f);

            gameBeganMuted = !musicEnabled;

            musicSources = new AudioSource[2];
            for (int i = 0; i < 2; i++) {
                GameObject newMusicSource = new GameObject("Music Source " + (i + 1));
                musicSources[i] = newMusicSource.AddComponent<AudioSource>();
                newMusicSource.transform.SetParent(this.transform);
            }

            GameObject newSFXSource = new GameObject("SFX Audio Source");
            sfxAudioSource = newSFXSource.AddComponent<AudioSource>();
            newSFXSource.transform.SetParent(this.transform);

            sfxAudioSourcePool = new Stack<AudioSource>(MAX_SFX_SOURCES);
            GameObject sfxSourceObj;
            AudioSource sfxSource;
            for (int i = 0; i < MAX_SFX_SOURCES; i++) {
                sfxSourceObj = new GameObject("SFX Audio Source #" + (i + 1));
                sfxSource = sfxSourceObj.AddComponent<AudioSource>();
                sfxSource.transform.SetParent(this.transform);
                sfxAudioSourcePool.Push(sfxSource);
            }
            sfxAudioSourcesInUse = new List<AudioSource>();
        }
    }

    //void Start() {
    //    musicEnabled = PlayerPrefs.GetInt(musicEnabledPlayerPrefString, 1).AsBool();
    //    musicVolumePercent = PlayerPrefs.GetFloat(musicVolumePlayerPrefsString, 0.5f);
    //    sfxEnabled = PlayerPrefs.GetInt(sfxEnabledPlayerPrefsString, 1).AsBool();
    //    sfxVolumePercent = PlayerPrefs.GetFloat(sfxVolumePlayerPrefsString, 0.5f);
    //}

    #region MUSIC_CONTROL_METHODS
    public void PlayMusic(AudioClip clip, float fadeDuration = 1, bool loop = false) {
        if (musicEnabled) {
            StopCoroutine("WaitForEndOfActiveMusic");
            activeMusicSourceIndex = 1 - activeMusicSourceIndex;
            musicSources[activeMusicSourceIndex].clip = clip;
            musicSources[activeMusicSourceIndex].loop = loop;
            musicSources[activeMusicSourceIndex].Play();
            StartCoroutine(AnimateMusicCrossFade(fadeDuration));
            StartCoroutine("WaitForEndOfActiveMusic");
        }
    }

    public void FadeMusicOut(float duration) {
        StopCoroutine("WaitForEndOfActiveMusic");
        StartCoroutine(AnimateFadeActiveMusicOut(duration));
    }

    public void QueueMusic(AudioClip clip, float fadeDuration = 1, bool loop = false) {
        StartCoroutine(WaitToPlayQueuedClip(clip, fadeDuration, loop));
    }
    #endregion MUSIC_CONTROL_METHODS


    #region SFX_CONTROL_METHODS
    public void PlaySound(AudioClip clip, bool loop = false, float clipVolume = 1) {
        //        StopCoroutine("WaitForEndOfActiveSFX");
        //        _SFXAudioSource.Stop();
        //        _SFXAudioSource.clip = clip;
        //        _SFXAudioSource.loop = loop;
        //        _SFXAudioSource.volume = (_SFXVolumePercent * _MasterVolumePercent); 
        //        _SFXAudioSource.Play();
        //        StartCoroutine("WaitForEndOfActiveSFX");

        if (sfxEnabled && sfxAudioSourcePool.Count > 0) {
            AudioSource nextSource = sfxAudioSourcePool.Pop();
            nextSource.clip = clip;
            nextSource.loop = loop;
            nextSource.volume = ((clipVolume * sfxVolumePercent) * masterVolumePercent);
            nextSource.Play();
            sfxAudioSourcesInUse.Add(nextSource);
            StartCoroutine(WaitForEndOfSpecificSFX(nextSource));
        }
    }

    public void StopSFX() {
        StopCoroutine("WaitForEndOfActiveSFX");
        sfxAudioSource.Stop();
    }

    public void StopSFXClip(AudioClip clip) {
        for (int i = 0; i < sfxAudioSourcesInUse.Count; i++) {
            if (sfxAudioSourcesInUse[i].clip == clip) {
                sfxAudioSourcesInUse[i].Stop();
            }
        }
    }
    #endregion SFX_CONTROL_METHODS

    public void SetMusicEnabled(bool value) {
        musicEnabled = value;
        //musicVolumePercent *= (musicEnabled) ? 1 : 0;

        if (gameBeganMuted && value) {
            gameBeganMuted = false;
            MusicManager.Instance.PlayLobbyMusic();
        }

        AudioSource activeSource = musicSources[activeMusicSourceIndex];
        activeSource.volume = (musicEnabled) ? (musicVolumePercent * masterVolumePercent) : 0;
        SavePlayerPrefs();
    }

    public void SetSFXEnabled(bool value) {
        sfxEnabled = value;
        //sfxVolumePercent *= (sfxEnabled) ? 1 : 0;
        sfxAudioSource.volume = (sfxEnabled) ? (sfxVolumePercent * masterVolumePercent) : 0;
        SavePlayerPrefs();
    }

    public void SetMusicVolumePercent(float value) {
        musicVolumePercent = value;
        AudioSource activeSource = musicSources[activeMusicSourceIndex];
        activeSource.volume = (musicVolumePercent * masterVolumePercent);
        SavePlayerPrefs();
    }

    public void SetSFXVolumePercent(float value) {
        sfxVolumePercent = value;
        sfxAudioSource.volume = (sfxVolumePercent * masterVolumePercent);
        SavePlayerPrefs();
    }

    public void SavePlayerPrefs() {
        int value = (musicEnabled) ? 1 : 0;
        PlayerPrefs.SetInt(musicEnabledPlayerPrefString, value);
        value = (sfxEnabled) ? 1 : 0;
        PlayerPrefs.SetInt(sfxEnabledPlayerPrefsString, value);
        
        PlayerPrefs.SetFloat(musicVolumePlayerPrefsString, musicVolumePercent);
        PlayerPrefs.SetFloat(sfxVolumePlayerPrefsString, sfxVolumePercent);
        
        PlayerPrefs.Save();
    }

    #region COROUTINES
    private IEnumerator WaitForEndOfActiveMusic() {
        AudioSource activeSource = musicSources[activeMusicSourceIndex];
        while (activeSource.isPlaying) {
            yield return null;
        }
        if (OnMusicComplete != null) {
            OnMusicComplete();
        }
    }

    private IEnumerator WaitForEndOfActiveSFX() {
        while (sfxAudioSource.isPlaying) {
            yield return null;
        }
        if (OnSFXComplete != null) {
            OnSFXComplete();
        }
    }

    private IEnumerator WaitForEndOfSpecificSFX(AudioSource source) {
        while (source.isPlaying) {
            yield return null;
        }
        sfxAudioSourcesInUse.Remove(source);
        sfxAudioSourcePool.Push(source);
        if (OnSFXComplete != null) {
            OnSFXComplete();
        }
    }

    private IEnumerator WaitToPlayQueuedClip(AudioClip clip, float fadeDuration = 1, bool loop = false) {
        AudioSource activeSource = musicSources[activeMusicSourceIndex];
        float timeRemaining = activeSource.clip.length - activeSource.time;
        while (timeRemaining > fadeDuration) {
            timeRemaining = activeSource.clip.length - activeSource.time;
            yield return null;
        }
        PlayMusic(clip, fadeDuration, loop);
    }

    private IEnumerator AnimateMusicCrossFade(float duration) {
        float percent = 0;

        while (percent < 1) {
            if (duration == 0) {
                percent = 1;
            }
            else {
                percent += Time.deltaTime * (1 / duration);
            }
            musicSources[activeMusicSourceIndex].volume = Mathf.Lerp(0, musicVolumePercent * masterVolumePercent, percent);
            musicSources[1 - activeMusicSourceIndex].volume = Mathf.Lerp(musicVolumePercent * masterVolumePercent, 0, percent);
            yield return null;
        }
    }

    private IEnumerator AnimateFadeActiveMusicOut(float duration) {
        float percent = 0;
        while (percent < 1) {
            percent += Time.deltaTime * (1 / duration);
            musicSources[activeMusicSourceIndex].volume = Mathf.Lerp(musicVolumePercent * masterVolumePercent, 0, percent);
            yield return null;
        }
        if (OnMusicFadeOutComplete != null) {
            OnMusicFadeOutComplete();
        }
    }
    #endregion COROUTINES

    #region SCENE_METHODS

    #endregion SCENE_METHODS

    void Update() {
        if (Input.GetKey(KeyCode.LeftControl)) {
            if (Input.GetKeyUp(KeyCode.M)) {
                SetMusicEnabled(!musicEnabled);
            }

            if (Input.GetKeyUp(KeyCode.S)) {
                SetSFXEnabled(!sfxEnabled);
            }
        }
    }

}
