﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class SFXManager : MonoBehaviour {

    private static SFXManager instance = null;
    public static SFXManager Instance {
        get {
            if (instance == null) {
                AudioManager.Instance.Init();
            }
            return instance;
        }
    }

    private AudioClip activeClip;

    public List<SFX> sfxList;
    public List<SFX> SFXList { get { return sfxList; } }

    public void PlaySFX(SFX_TYPE sfx, bool loop = false, float clipVolume = 1) {
        activeClip = GetSFXClip(sfx);
        AudioManager.Instance.PlaySound(activeClip, loop, clipVolume);
    }

    public void StopSFX() {
        AudioManager.Instance.StopSFX();
    }

    public void StopSFXClip(SFX_TYPE clip) {
        AudioManager.Instance.StopSFXClip(GetSFXClip(clip));
    }

    public float GetLengthOfClip(SFX_TYPE sfx) {
        return GetSFXClip(sfx).length;
    }

    private AudioClip GetSFXClip(SFX_TYPE type) {
        for (int i = 0; i < sfxList.Count; i++) {
            if (sfxList[i].SFXType == type) {
                return sfxList[i].clip;
            }
        }
        return null;
    }

    // Use this for initialization
    void Awake() {
        instance = this;
    }

    // Update is called once per frame
    void Update() {

    }
}

[Serializable]
public class SFX {
    public SFX_TYPE SFXType;
    public AudioClip clip;
}

public enum SFX_TYPE {
    QuestionButtonPress,
    QuestionAppear,
    QuestionDisappear,
    AnswerCorrect,
    AnswerWrong,
    TimerLowBeep,
    GiftBoxOpen,
    GiftShake,
    ClimbingBells,
    CurrencyShake,
    FireworksSparkle,
    ButtonClickToony,
    ButtonClickDoor,
    Clapping
}